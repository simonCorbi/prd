///////////////////////////////////////////////////////////////////////////////
// 
// PRD 2016-2017
// 
// author : Simon Corbill�
//

#ifndef CSEGMENTATPLUGIN_H
#define CSEGMENTATPLUGIN_H

#include <QRect>
#include <QObject>
#include <QStringList>
#include <QPainterPath>
#include <QImage>
#include <QAction>

#include <qtplugin/PluginInterface.h>
#include <controls/ccolorcombobox.h>
#include <data/CRegionData.h>

class CSegmentationPluginPanel;

//! Segmentation Plugin has to inherit QObject and PluginInterface
class CSegmentationPlugin : public QObject,
                    public PluginInterface,
                    public data::CObjectObserver< data::CRegionData >
{
    Q_OBJECT
#if QT_VERSION >= 0x050000    
    Q_PLUGIN_METADATA(IID "com.3dim-laboratory.Qt.SegmentationPlugin")
#endif
    Q_INTERFACES(PluginInterface)
public:
    //! Constructor
    CSegmentationPlugin();
    //! Destructor
    ~CSegmentationPlugin();
protected:
    //! Plugin menu
    QMenu*      m_pMenu;
    //! Plugin toolbar
    QToolBar*   m_pToolBar;
    //! Plugin panel
    CSegmentationPluginPanel* m_pPanel;
    //! Plugin action
    QAction*    m_actionPickValue;
    //! Helper method for action creation
    void        createActions();
    //! Uncheck actions
    void        updateActions(QAction *letBe);

	
private slots:
    //! action handler
    void        onActionPickValue();

protected:
    //! Signal connection for mouse mode change monitoring
    vpl::mod::tSignalConnection m_ConnectionModeChanged;
public:
    //! Methods from PluginInterface
    QMenu*      getOrCreateMenu();
    QToolBar*   getOrCreateToolBar();
    QWidget*    getOrCreatePanel();
    QAction*    getAction(const QString &actionName);
    QString     pluginName();
    QString     pluginID();
    void        connectPlugin();
    void        disconnectPlugin();
protected:
    //! Signal handlers
    void        sigModeChanged( scene::CAppMode::tMode mode );
    //! Region data observer method
    void        objectChanged(data::CRegionData *pData);
};

#endif
