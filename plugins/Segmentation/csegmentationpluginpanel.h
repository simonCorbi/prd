///////////////////////////////////////////////////////////////////////////////
// 
// Copyright 2008-2015 3Dim Laboratory s.r.o.
//

#ifndef CSEGPLUGINPANEL_H
#define CSEGPLUGINPANEL_H

#include <QWidget>

#include <qtplugin/PluginInterface.h>

#include <controls/ccolorcombobox.h>

#include "itkImage.h"
#include "itkImportImageFilter.h"
#include <itkImageRegionIterator.h>
#include "itkWatershedImageFilter.h"
#include "itkScalarToRGBPixelFunctor.h"
#include "itkScalarToRGBColormapImageFilter.h"

#include "itkConfidenceConnectedImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkCurvatureFlowImageFilter.h"

#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

typedef float InputPixelType;
typedef unsigned char OutputPixelType;
const unsigned int Dimension = 3;

typedef itk::Image< InputPixelType, Dimension > InputImageType;
typedef itk::Image< OutputPixelType, Dimension > OutputImageType;

typedef itk::ImportImageFilter< InputPixelType, Dimension >   ImportFilterType;

typedef itk::CastImageFilter< InputImageType, OutputImageType >CastingFilterType;

typedef  itk::ImageFileWriter< OutputImageType > WriterType;

namespace Ui {
class CSegmentationPluginPanel;
}

class CSegmentationPlugin;

class CSegmentationPluginPanel : public QWidget, public CAppBindings
{
    Q_OBJECT
    
public:
    explicit CSegmentationPluginPanel(CAppBindings* pBindings, QWidget *parent = 0);
    ~CSegmentationPluginPanel();

	double toZeroOne(int x);
	double toZeroOne(int x, double min, double max);
	int normalisetoHounsfield(double x);
	short normaliseToGray(double x);
	int grayToHounsfield(short x);

protected:
    //! Point selected signal handler
    void OnPointPicked( float x, float y, float z, int EventType );

    //! Connect drawing signals
    void connectSignals();

    //! Current drawisq ng mode
    data::CDrawingOptions::EDrawingMode m_drawingMode;

    //! Drawing mode changed signal
    typedef vpl::mod::CSignal< void, int > tSigDrawingModeChanged;

    //! Drawing mode changed signal
    tSigDrawingModeChanged m_sigDrawingModeChanged;

    //! Drawing callback connection
    vpl::mod::tSignalConnection m_conDrawingDone, m_conDrawingModeChanged;

    //! Get current drawing mode
    data::CDrawingOptions::EDrawingMode getMode() { return m_drawingMode; }

    //! Returns the current region id
    int getCurrentRegion();

    //! uncheck buttons
    void updateButtons(QWidget *letBe);

    //! Have I focus?
    bool m_haveFocus;
    
	double m_lowValueVoxel;
	double m_highValueVoxel;

	int xSeed;
	int ySeed;
	int zSeed;

	double coeffMulti;
	int nbIteration;
	int radius;

private slots:
    void on_pushButtonPickValue_clicked();

	void on_pushButtonSegmente_clicked();

	void testColorisation();
	void calculBorne();

	void majCoeffMulti(double value);
	void majNbIteration(int value);
	void majRadius(int value);

private:
    Ui::CSegmentationPluginPanel *ui;

	InputImageType::Pointer convertToITK(int nx, int ny, int nz, InputPixelType * buffer, double dx, double dy, double dz, float ox, float oy, float oz);

    friend class CSegmentationPlugin;
};

#endif // CSEGMENTATIONPANEL_H
