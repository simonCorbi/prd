///////////////////////////////////////////////////////////////////////////////
// 
// Copyright 2008-2015 3Dim Laboratory s.r.o.
//

#include <QtGui>
#include <QMenu>
#include <QToolBar>
#include "csegmentationplugin.h"
#include "csegmentationpluginpanel.h"

CSegmentationPlugin::CSegmentationPlugin() : QObject(), PluginInterface()
{
    m_actionPickValue = NULL;

    m_pMenu = NULL;
    m_pToolBar = NULL;
    m_pPanel = NULL;

	setProperty("Icon",":/icons/icons/segmentation.png");

}

CSegmentationPlugin::~CSegmentationPlugin()
{

}

void  CSegmentationPlugin::createActions()
{
    if (!m_actionPickValue)
    {
        m_actionPickValue = new QAction(QIcon(":/icons/icons/1.png"),tr("Pick Value"),NULL);
		m_actionPickValue->setObjectName("actionPickValue");
        m_actionPickValue->setStatusTip(tr("Click the button and then select any point the volume data test."));
        QObject::connect(m_actionPickValue, SIGNAL(triggered()), this, SLOT(onActionPickValue()) );
    }
}

QMenu* CSegmentationPlugin::getOrCreateMenu()
{
    if (m_pMenu) return m_pMenu;
    createActions();
    m_pMenu = new QMenu(tr("Segmentation Plugin test"));
    if (m_pMenu)
    {
        m_pMenu->addAction(m_actionPickValue);
        m_pMenu->addSeparator();
    }
    return m_pMenu;
}

QToolBar* CSegmentationPlugin::getOrCreateToolBar()
{
    if (m_pToolBar) return m_pToolBar;
    createActions();
    QToolBar* pToolBar = new QToolBar(tr("Segmentation Plugin ToolBar"));
    if (pToolBar)
    {
        pToolBar->setObjectName("Segmentation Plugin ToolBar"); 
        pToolBar->addAction(m_actionPickValue);

        pToolBar->hide();
        m_pToolBar = pToolBar;
    }
    return pToolBar;
}

QWidget*  CSegmentationPlugin::getOrCreatePanel()
{
    createActions();
    // NOTE: seems like there's a bug in QT https://bugreports.qt.nokia.com/browse/QTBUG-13237
    //       uncheck acceptdrops in lineEdits to avoid it
    if (!m_pPanel)
        m_pPanel = new CSegmentationPluginPanel(this);
    return m_pPanel;
}

QString  CSegmentationPlugin::pluginName()
{
    return tr("Segmentation Plugin");
}

QString  CSegmentationPlugin::pluginID()
{
    return ("SegmentationPlugin");
}

void CSegmentationPlugin::connectPlugin()
{
    
}

void CSegmentationPlugin::disconnectPlugin()
{
    getAppMode()->getModeChangedSignal().disconnect( m_ConnectionModeChanged );
    PLUGIN_APP_STORAGE.disconnect(data::Storage::RegionData::Id, this);
}

QAction* CSegmentationPlugin::getAction(const QString &actionName)
{
    createActions();
    if (actionName=="PickValue")
        return m_actionPickValue;

    return NULL;
}

void CSegmentationPlugin::sigModeChanged( scene::CAppMode::tMode mode )
{
    data::CObjectPtr< data::CDrawingOptions > spOptions( PLUGIN_APP_STORAGE.getEntry(data::Storage::DrawingOptions::Id) );
    data::CDrawingOptions::EDrawingMode drMode=spOptions->getDrawingMode();
    spOptions.release();

}

void CSegmentationPlugin::objectChanged(data::CRegionData *pData)
{

}

void CSegmentationPlugin::updateActions(QAction *letBe)
{

}

void CSegmentationPlugin::onActionPickValue()
{
    CSegmentationPluginPanel * pPanel = dynamic_cast<CSegmentationPluginPanel*>( getOrCreatePanel() );
    if( pPanel )
        pPanel->on_pushButtonPickValue_clicked();
}

#if QT_VERSION < 0x050000  
    Q_EXPORT_PLUGIN2(pnp_Segmentationplugin, CSegmentationPlugin)
#endif