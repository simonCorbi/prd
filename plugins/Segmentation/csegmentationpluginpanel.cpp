///////////////////////////////////////////////////////////////////////////////
// 
// Copyright 2008-2015 3Dim Laboratory s.r.o.
//

#include "csegmentationpluginpanel.h"
#include "ui_csegmentationpluginpanel.h"

#include <app/Signals.h>
#include <data/CRegionData.h>
#include <data/CUndoManager.h>

#include <drawing/CISEventHandler.h>
#include <drawing/CLineOptimizer.h>

#include <typeinfo>

CSegmentationPluginPanel::CSegmentationPluginPanel(CAppBindings *pBindings, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CSegmentationPluginPanel)
{
    ui->setupUi(this);
    Q_ASSERT(pBindings);
    setAppMode(pBindings->getAppMode());
    setDataStorage(pBindings->getDataStorage());

	QObject::connect(ui->segmenteButton, SIGNAL(clicked()), this, SLOT(on_pushButtonSegmente_clicked()));

    m_haveFocus = false;
	//Initialise the seed
	xSeed = 0;
	ySeed = 0;
	zSeed = 0;

	//Initialisation des parametres de segmentation
	coeffMulti = 1.25;
	nbIteration = 20;
	radius = 2;

	QObject::connect(ui->spinBoxCoeffMulti, SIGNAL(valueChanged(double)), this, SLOT(majCoeffMulti(double)));
	QObject::connect(ui->spinBoxNbItera, SIGNAL(valueChanged(int)), this, SLOT(majNbIteration(int)));
	QObject::connect(ui->spinBoxRadius, SIGNAL(valueChanged(int)), this, SLOT(majRadius(int)));
    


}

CSegmentationPluginPanel::~CSegmentationPluginPanel()
{
	//PLUGIN_APP_STORAGE.disconnect(data::Storage::RegionColoring::Id, &m_colorComboBox);
	//PLUGIN_APP_MODE.disconnectAllDrawingHandlers();
    delete ui;
}

void CSegmentationPluginPanel::testColorisation()
{
	//setMessageDebug(tr("test colorisation debut"));

	data::CObjectPtr< data::CActiveDataSet > ptrDataset(PLUGIN_APP_STORAGE.getEntry(data::Storage::ActiveDataSet::Id));
	data::CObjectPtr< data::CDensityData > pVolume(PLUGIN_APP_STORAGE.getEntry(ptrDataset->getId()));

	data::CColor4b MyColor(0, 255, 0, 128);

	//VPL_SIGNAL(SigSetColoring).invoke(new data::CConstColoring(0, 500, MyColor));
	//VPL_SIGNAL(SigSetColoring).connect();
	PLUGIN_APP_STORAGE.invalidate(pVolume.getEntryPtr());

	//setMessageDebug(tr("test colorisation fin"));
}
///////////////////////////////////////////////////////////////////////////////
// Callback for point clicked

void CSegmentationPluginPanel::OnPointPicked( float x, float y, float z, int EventType )
{
    // Get pointer to the volume data
    data::CObjectPtr< data::CActiveDataSet > ptrDataset( PLUGIN_APP_STORAGE.getEntry(data::Storage::ActiveDataSet::Id) );
    data::CObjectPtr< data::CDensityData > pVolume( PLUGIN_APP_STORAGE.getEntry(ptrDataset->getId()) );

    // Get the density value
    int ix = vpl::math::round2Int(x);
    int iy = vpl::math::round2Int(y);
    int iz = vpl::math::round2Int(z);
    vpl::img::tDensityPixel Value = pVolume->at(ix, iy, iz);

    // Show values
    ui->editDensity->setText(QString::number(Value));
    ui->editCoords->setText(QString("%1 %2 %3").arg(ix).arg(iy).arg(iz));

	//Set this point as seed
	xSeed = ix;
	ySeed = iy;
	zSeed = iz;
}

void CSegmentationPluginPanel::on_pushButtonPickValue_clicked()
{
    // Register the scene hit signal handler
    PLUGIN_APP_MODE.getSceneHitSignal().connect(this, &CSegmentationPluginPanel::OnPointPicked);
    // Change the mouse mode
    PLUGIN_APP_MODE.storeAndSet(scene::CAppMode::COMMAND_SCENE_HIT);
}

/**********************************************************************************************//**
 * 
 * \brief	Permet de segmenter une image
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void CSegmentationPluginPanel::on_pushButtonSegmente_clicked()
{
	calculBorne();
	
	//Pointer to data
	data::CObjectPtr< data::CActiveDataSet > ptrDataset(PLUGIN_APP_STORAGE.getEntry(data::Storage::ActiveDataSet::Id));
	data::CObjectPtr< data::CDensityData > pVolume(PLUGIN_APP_STORAGE.getEntry(ptrDataset->getId()));

	//Save original before working on it
	data::CObjectPtr< data::CUndoManager > undoManager(PLUGIN_APP_STORAGE.getEntry(data::Storage::UndoManager::Id));
	undoManager->insert(pVolume->getVolumeSnapshot());	

	//teste fonction de convertion (� refaire propre)
	int value = pVolume->at(255, 244, 20);
	double valueNormalise = toZeroOne(value);
	double valueNormalise2 = toZeroOne(value, -1500, 7000);
	int normaliseHounsfield = normalisetoHounsfield(valueNormalise);
	short ntG = normaliseToGray(valueNormalise);
	int gtH = grayToHounsfield(ntG);

	if (pVolume->hasData())
	{
		//Size
		int xSize = pVolume->getSize().getX();
		int ySize = pVolume->getSize().getY();
		int zSize = pVolume->getSize().getZ();

		//Spacing
		double dx = pVolume->getDX(); //float
		double dy = pVolume->getDY();
		double dz = pVolume->getDZ();

		//Origine
		float ox = 0;
		float oy = 0;
		float oz = 0;

		//Data
		InputPixelType * buffer = new InputPixelType[xSize*ySize*zSize];

		int index = 0;
			
		//#pragma omp parallel for
		for (int x = 0; x < xSize; ++x)
		{
			for (int y = 0; y < ySize; ++y)
			{
				for (int z = 0; z < zSize; ++z)
				{
					buffer[index] = normaliseToGray(toZeroOne(pVolume->at(x, y, z))); //hounsfield to gray 0-255
					index = z + y*zSize + x*(ySize*zSize);
				}
			}
		}

		InputImageType::Pointer imageITKInput = convertToITK(xSize, ySize, zSize, buffer, dx, dy, dz, ox, oy, oz);
 
		CastingFilterType::Pointer caster = CastingFilterType::New();
		WriterType::Pointer writer = WriterType::New();
		
		typedef itk::CurvatureFlowImageFilter< InputImageType, InputImageType >
			CurvatureFlowImageFilterType;
		CurvatureFlowImageFilterType::Pointer smoothing =
			CurvatureFlowImageFilterType::New();
		typedef itk::ConfidenceConnectedImageFilter<InputImageType, InputImageType>
			ConnectedFilterType;
		ConnectedFilterType::Pointer confidenceConnected = ConnectedFilterType::New();
		
		smoothing->SetInput(imageITKInput);
		smoothing->SetNumberOfIterations(3);
		smoothing->SetTimeStep(0.05);

		confidenceConnected->SetInput(smoothing->GetOutput());
		confidenceConnected->SetMultiplier(coeffMulti); //2.5
		confidenceConnected->SetNumberOfIterations(nbIteration); //20
		confidenceConnected->SetInitialNeighborhoodRadius(radius); //2
		confidenceConnected->SetReplaceValue(255); //blanc

		InputImageType::IndexType index1;
		index1[0] = xSeed;
		index1[1] = ySeed;
		index1[2] = zSeed;
		confidenceConnected->AddSeed(index1);

		caster->SetInput(confidenceConnected->GetOutput());

		writer->SetInput(caster->GetOutput());
		writer->SetFileName("testSegmentation2.nii");

		try
		{
			writer->Update();
			const OutputPixelType * bufferOutput = writer->GetInput()->GetBufferPointer();
			index = 0;

			for (int x = 0; x < xSize; ++x)
			{
				for (int y = 0; y < ySize; ++y)
				{
					for (int z = 0; z < zSize; ++z)
					{
						if (grayToHounsfield(bufferOutput[index]) == m_highValueVoxel)
						{
							pVolume->at(x, y, z) = m_highValueVoxel; // Pour l'instant on colore la r�gion en blanc
						}
						index = x + y*xSize + z*(ySize*xSize); //index++;
					}
				}
			}
			PLUGIN_APP_STORAGE.invalidate(pVolume.getEntryPtr());
			//delete(bufferOutput);
		}
		catch (itk::ExceptionObject & excep)
		{
			std::cerr << "Exception caught !" << std::endl;
			std::cerr << excep << std::endl;

		}

		//Penser � lib�rer la m�moire inutiliser : buffer ...
		//delete(buffer);

	}
	else
	{
		//setMessageDebug(tr("Pas de donnees a segmenter"));
	}
	
}
/**********************************************************************************************//**
 * 
 * \brief	Permet de convertir une image de 3DimViewer en image ITK
 * \author	Simon
 * \date	05/04/2017
 **************************************************************************************************/
InputImageType::Pointer CSegmentationPluginPanel::convertToITK(int nx, int ny, int nz, InputPixelType * buffer, double dx, double dy, double dz, float ox, float oy, float oz)
{
	InputImageType::Pointer image = InputImageType::New();

	InputImageType::SizeType size;
	size[0] = nx;
	size[1] = ny;
	size[2] = nz;

	InputImageType::IndexType start;
	start[0] = 0;
	start[1] = 0;
	start[2] = 0;

	InputImageType::RegionType region;
	region.SetSize(size);
	region.SetIndex(start);
	
	image->SetRegions(region);
	image->Allocate();

	double spacing[3];
	spacing[0] = dx;
	spacing[1] = dy;
	spacing[2] = dz;

	image->SetSpacing(spacing);

	double origin[3];
	origin[0] = ox;
	origin[1] = oy;
	origin[2] = oz;

	image->SetOrigin(origin);

	typedef itk::ImageRegionIterator< InputImageType > IteratorType;

	IteratorType it(image, region);

	it.GoToBegin();

	InputPixelType * data = buffer; 

	int indexeData = 0;
	//#pragma omp parallel for
	for (int x = 0; x < nx; ++x)
	{
		for (int y = 0; y < ny; ++y)
		{
			for (int z = 0; z < nz; ++z)
			{
				InputImageType::IndexType pixelIndex;
				pixelIndex[0] = x; // x position
				pixelIndex[1] = y; // y position
				pixelIndex[2] = z; // z position

				indexeData = z + y*nz + x*(ny*nz);
				InputImageType::PixelType pixelValue = data[indexeData];
				image->SetPixel(pixelIndex, pixelValue);
			}
		}
	}
	//delete(data);

	return image;
}


void CSegmentationPluginPanel::connectSignals()
{

}

void CSegmentationPluginPanel::updateButtons(QWidget *letBe)
{
   
}

int CSegmentationPluginPanel::getCurrentRegion()
{
    // Get currently active region from the storage
    data::CObjectPtr< data::CRegionColoring > ptrColoring( PLUGIN_APP_STORAGE.getEntry( data::Storage::RegionColoring::Id ) );
    return ptrColoring->getActiveRegion();
}

void CSegmentationPluginPanel::calculBorne()
{
	data::CObjectPtr< data::CActiveDataSet > ptrDataset(PLUGIN_APP_STORAGE.getEntry(data::Storage::ActiveDataSet::Id));
	data::CObjectPtr< data::CDensityData > pVolumeSrc(PLUGIN_APP_STORAGE.getEntry(ptrDataset->getId()));

	int xSize = pVolumeSrc->getSize().getX();
	int ySize = pVolumeSrc->getSize().getY();
	int zSize = pVolumeSrc->getSize().getZ();

	if (pVolumeSrc->hasData())
	{
		m_lowValueVoxel = vpl::img::CPixelTraits<vpl::img::tDensityPixel>::getPixelMin();
		m_highValueVoxel = vpl::img::CPixelTraits<vpl::img::tDensityPixel>::getPixelMax();
	}
	else
	{
		
	}
}
//min-max-> 0-1
double CSegmentationPluginPanel::toZeroOne(int x){
	if (x == m_lowValueVoxel){
		return 0;
	}
	else if (x == m_highValueVoxel)
	{
		return 1;
	}
	else
	{
		return (x - m_lowValueVoxel) / (m_highValueVoxel - m_lowValueVoxel);
	}	
}

//x-y -> 0-1
double CSegmentationPluginPanel::toZeroOne(int x, double min, double max){
	if (x == min){
		return 0;
	}
	else if (x == max)
	{
		return 1;
	}
	else
	{
		return (x - min) / (max - min);
	}
}
//0-1 -> -1500 7000
int CSegmentationPluginPanel::normalisetoHounsfield(double x){
	return x * (m_highValueVoxel - m_lowValueVoxel) + m_lowValueVoxel;
}
// 0-1 to 0-255
short CSegmentationPluginPanel::normaliseToGray(double x)
{
	return round(x * 255);
}
//0-255 -> -1500 7000
int CSegmentationPluginPanel::grayToHounsfield(short x)
{
	return normalisetoHounsfield(toZeroOne(x, 0, 255));
}

void CSegmentationPluginPanel::majCoeffMulti(double value)
{
	coeffMulti = value;
}
void CSegmentationPluginPanel::majNbIteration(int value)
{
	nbIteration = value;
}
void CSegmentationPluginPanel::majRadius(int value)
{
	radius = value;
}