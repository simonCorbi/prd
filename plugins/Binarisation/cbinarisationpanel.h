#ifndef CBINARISATIONPANEL_H
#define CBINARISATIONPANEL_H

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QPushButton>

#include <qtplugin/PluginInterface.h>

/**********************************************************************************************//**
 * \namespace	Ui
 *
 * \brief	Ui of Qt
 **************************************************************************************************/

namespace Ui {

/**********************************************************************************************//**
 * \class	CBinarisationPanel
 *
 * \brief	Panel du plugin de binarisation
 *
 * \author	Simon
 * \date	01/03/2017
 **************************************************************************************************/

class CBinarisationPanel;
}

class CBinarisationPanel : public QWidget, 
	public CAppBindings
{
    Q_OBJECT
    
public:
    explicit CBinarisationPanel(CAppBindings* pBindings, QWidget *parent = 0);
    ~CBinarisationPanel();

	int getSeuil();
	void setSeuil(int valeur);

	void setBorneSeuil(int min, int max);

	void setMessageDebug(QString message);

protected:
	//La valeur du seuil de binarisation de l'image : par d�faut seuil = 0
	// -1500 < seuil < 7000 pour image issue d'un scanner X
	int seuil;

private slots:
	void majSeuil(int value);
	void binarise();

private:
    Ui::CBinarisationPanel *ui;

	int borneMinSeuil;
	int borneMaxSeuil;
	
	friend class BinarisationPlugin;
};

#endif // CBINARISATIONPANEL_H
