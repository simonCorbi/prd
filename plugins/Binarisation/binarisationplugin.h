#ifndef BINARISATIONPLUGIN_H
#define BINARISATIONPLUGIN_H

#include <QRect>
#include <QObject>
#include <QStringList>
#include <QPainterPath>
#include <QImage>
#include <QAction>

#include <qtplugin/PluginInterface.h>
#include <data/CRegionData.h>

class CBinarisationPanel;

/**********************************************************************************************//**
 * \class	BinarisationPlugin
 *
 * \brief	A binarisation plugin.
 *
 * \author	Simon
 * \date	01/03/2017
 **************************************************************************************************/

class BinarisationPlugin : public QObject,
                           public PluginInterface
{
    Q_OBJECT
#if QT_VERSION >= 0x050000    
    Q_PLUGIN_METADATA(IID "com.3dim-laboratory.Qt.BinarisationPlugin")
#endif
    Q_INTERFACES(PluginInterface)
public:
    BinarisationPlugin();
	~BinarisationPlugin();
protected:
    QMenu*      m_pMenu;
    QToolBar*   m_pToolBar;
    CBinarisationPanel* m_pPanel;

public:
    //! Methods from PluginInterface
    QMenu*      getOrCreateMenu();
    QToolBar*   getOrCreateToolBar();
    QWidget*    getOrCreatePanel();
	QAction*    getAction(const QString &actionName);
    QString     pluginName();
    QString     pluginID();
    void        connectPlugin();
    void        disconnectPlugin();
};

#endif
