/**********************************************************************************************//**
*
* \brief	Implements the binarisatioplugin class.
* 			Cette classe permet de cr�er le plugin
* 			Une fois cr�er 3DimViewer peut l'int�grer
* \author	Simon
* \date	02/03/2017
**************************************************************************************************/

#include <QtGui>
#include <QMenu>
#include <QToolBar>

#include "binarisationplugin.h"
#include <data/CSceneManipulatorDummy.h>
#include "cbinarisationpanel.h"

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::BinarisationPlugin()
 *
 * \brief Constructeur du plugin de binarisation pour 3DimViewer
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
BinarisationPlugin::BinarisationPlugin() : QObject(), PluginInterface()
{
    m_pMenu = NULL;
    m_pToolBar = NULL;
    m_pPanel = NULL;
	setProperty("Icon",":/icons/binarisation.png");
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::~BinarisationPlugin()
 *
 * \brief Destructeur du plugin
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
BinarisationPlugin::~BinarisationPlugin()
{

}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::getOrCreateMenu()
 *
 * \brief Cr�ation du menu du plugin
 * 		  On ne d�finit pas d'action, elles seront accessibles via le panel
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QMenu* BinarisationPlugin::getOrCreateMenu()
{
    if (m_pMenu) return m_pMenu;

    m_pMenu = new QMenu(tr("Binarisation Plugin"));
    if (m_pMenu)
    {
    }
    return m_pMenu;
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::getOrCreateToolBar()
 *
 * \brief On cr�e un toolbar vide : les actions seront disponibles via le panel
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QToolBar* BinarisationPlugin::getOrCreateToolBar()
{
    if (m_pToolBar) return m_pToolBar;

    QToolBar* pToolBar = new QToolBar(tr("Binarisation Plugin ToolBar"));
    if (pToolBar)
    {
        pToolBar->setObjectName("Binarisation Plugin ToolBar"); 
        pToolBar->hide();
        m_pToolBar = pToolBar;
    }
    return pToolBar;
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::getOrCreatePanel()
 *
 * \brief Permet de cr�er ou de r�cup�rer le panel
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QWidget* BinarisationPlugin::getOrCreatePanel()
{
    if (!m_pPanel)
        m_pPanel = new CBinarisationPanel(this);
    return m_pPanel;
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::pluginName()
 *
 * \brief Permet de r�cup�rer le nom du plugin
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QString BinarisationPlugin::pluginName()
{
    return tr("Binarisation Plugin");
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::pluginID()
 *
 * \brief Permet de r�cup�rer le nom du plugin
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QString BinarisationPlugin::pluginID()
{
    return ("Binarisation");
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::getAction(const QString &actionName)
 *
 * \brief M�thode � impl�menter pour l'interface, mais dans notre cas elle ne fait rien
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
QAction* BinarisationPlugin::getAction(const QString &actionName)
{
	return NULL;
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::connectPlugin()
 *
 * \brief M�thode � impl�menter pour l'interface, mais dans notre cas elle ne fait rien
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void BinarisationPlugin::connectPlugin()
{
}

/**********************************************************************************************//**
 * \fn	void BinarisationPlugin::disconnectPlugin()
 *
 * \brief M�thode � impl�menter pour l'interface, mais dans notre cas elle ne fait rien
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void BinarisationPlugin::disconnectPlugin()
{
}

#if QT_VERSION < 0x050000    
    Q_EXPORT_PLUGIN2(pnp_binarisationplugin, BinarisationPlugin)
#endif