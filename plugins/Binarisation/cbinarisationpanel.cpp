/**********************************************************************************************//**
 * 
 * \brief	Implements the cbinarisationpanel class.
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/

#include <data/CDensityData.h>
#include <data/CActiveDataSet.h>
#include <data/CRegionData.h>
#include <data/CUndoManager.h>
#include <data/CSceneManipulatorDummy.h>

#include "cbinarisationpanel.h"
#include "ui_cbinarisationpanel.h"

/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::CBinarisationPanel(CAppBindings *pBindings, QWidget *parent)
 *
 * \brief Constructeur du panel du plugin
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
CBinarisationPanel::CBinarisationPanel(CAppBindings *pBindings, QWidget *parent) :
    QWidget(parent), CAppBindings(pBindings),
    ui(new Ui::CBinarisationPanel)
{	
    ui->setupUi(this);

	seuil = 0; // Valeur par d�faut du seuil de binarisation = valeur de l'eau sur l'�chelle de Hounsfield

	//D�finit les bornes
	borneMinSeuil = vpl::img::CPixelTraits<vpl::img::tDensityPixel>::getPixelMin();
	borneMaxSeuil = vpl::img::CPixelTraits<vpl::img::tDensityPixel>::getPixelMax();

	// Connecte le slider avec la spinBox
    QObject::connect(ui->spinBoxSeuil, SIGNAL(valueChanged(int)), ui->seuilSlider,  SLOT(setValue(int)));
    QObject::connect(ui->seuilSlider,  SIGNAL(valueChanged(int)), ui->spinBoxSeuil, SLOT(setValue(int)));

	//Maj du seuil
	QObject::connect(ui->spinBoxSeuil, SIGNAL(valueChanged(int)), this, SLOT(majSeuil(int)));
	QObject::connect(ui->seuilSlider, SIGNAL(valueChanged(int)), this, SLOT(majSeuil(int)));

	// Connecte les boutons avec leur action respective
	connect(ui->binariserButton, SIGNAL(clicked()), this, SLOT(binarise()));
}
/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::~CBinarisationPanel()
 *
 * \brief Destructeur du panel
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
CBinarisationPanel::~CBinarisationPanel()
{
    delete ui;
}
/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::majSeuil(int value)
 *
 * \brief Permet de mettre � jour le seuil de binarisation
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void CBinarisationPanel::majSeuil(int value)
{
	seuil = value;
}
/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::setBorneSeuil(int min, int max)
 *
 * \brief Permet d'affecter une valeur aux bornes des voxels de l'image
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void CBinarisationPanel::setBorneSeuil(int min, int max)
{
	borneMinSeuil = min;
	borneMaxSeuil = max;
}
/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::setMessageDebug(QString message)
 *
 * \brief Permet d'afficher un message dans le panel du plugin
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/
void CBinarisationPanel::setMessageDebug(QString message)
{
	ui->debugLabel->setText(message);
}

/**********************************************************************************************//**
 * \fn	void CBinarisationPanel::binarise()
 *
 * \brief Fonction de binarisation na�ve :
 *	On parcourt les voxels de l'image
 *	si la densit� est inf�rieure au seuil => le voxel devient noir
 *	sinon il devient blanc
 *
 *  La valeur de densit� d'un voxel est repr�sent� sur l'�chelle de hounsfield (-1500 � 7000)
 *  
 * Note pour la V2 : utilisation de foncteur : pForEach sur le volume de donn�es (parall�lisable)
 *
 * \author	Simon
 * \date	02/03/2017
 **************************************************************************************************/

void CBinarisationPanel::binarise()
{
	setMessageDebug(tr("Test binarise debut "));

	//Pointer to data
	data::CObjectPtr< data::CActiveDataSet > ptrDataset(PLUGIN_APP_STORAGE.getEntry(data::Storage::ActiveDataSet::Id));
	data::CObjectPtr< data::CDensityData > pVolumeSrc(PLUGIN_APP_STORAGE.getEntry(ptrDataset->getId()));

	//Save original before working on it
	data::CObjectPtr< data::CUndoManager > undoManager(PLUGIN_APP_STORAGE.getEntry(data::Storage::UndoManager::Id));
	undoManager->insert(pVolumeSrc->getVolumeSnapshot());

	int xSize = pVolumeSrc->getSize().getX();
	int ySize = pVolumeSrc->getSize().getY();
	int zSize = pVolumeSrc->getSize().getZ();

	if (pVolumeSrc->hasData())
	{		
		#pragma omp parallel for
		for (int z = 0; z < zSize; z++)
		{
			for (int y = 0; y < ySize; y++)
			{
				for (int x = 0; x < xSize; x++)
				{
					if (pVolumeSrc->at(x, y, z) <= seuil)
					{
						pVolumeSrc->at(x, y, z) = borneMinSeuil;
					}
					else
					{
						pVolumeSrc->at(x, y, z) = borneMaxSeuil; 
					}
				}
			}
		}

		PLUGIN_APP_STORAGE.invalidate(pVolumeSrc.getEntryPtr());

		setMessageDebug(tr("Test binarise fin "));
	}
	else
	{
		setMessageDebug(tr("Pas de donn�es"));
	}	
}