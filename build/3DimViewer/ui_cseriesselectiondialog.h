/********************************************************************************
** Form generated from reading UI file 'cseriesselectiondialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CSERIESSELECTIONDIALOG_H
#define UI_CSERIESSELECTIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_CSeriesSelectionDialog
{
public:
    QGridLayout *gridLayout;
    QLabel *label_9;
    QGridLayout *gridLayout_3;
    QDoubleSpinBox *spinSubsamplingX;
    QDoubleSpinBox *spinSubsamplingY;
    QDoubleSpinBox *spinSubsamplingZ;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QTableWidget *tableWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CSeriesSelectionDialog)
    {
        if (CSeriesSelectionDialog->objectName().isEmpty())
            CSeriesSelectionDialog->setObjectName(QStringLiteral("CSeriesSelectionDialog"));
        CSeriesSelectionDialog->resize(539, 455);
        CSeriesSelectionDialog->setMinimumSize(QSize(539, 354));
        CSeriesSelectionDialog->setSizeGripEnabled(true);
        gridLayout = new QGridLayout(CSeriesSelectionDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_9 = new QLabel(CSeriesSelectionDialog);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 1, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        spinSubsamplingX = new QDoubleSpinBox(CSeriesSelectionDialog);
        spinSubsamplingX->setObjectName(QStringLiteral("spinSubsamplingX"));
        spinSubsamplingX->setMinimum(0.01);
        spinSubsamplingX->setMaximum(10);
        spinSubsamplingX->setSingleStep(0.05);
        spinSubsamplingX->setValue(1);

        gridLayout_3->addWidget(spinSubsamplingX, 0, 1, 1, 1);

        spinSubsamplingY = new QDoubleSpinBox(CSeriesSelectionDialog);
        spinSubsamplingY->setObjectName(QStringLiteral("spinSubsamplingY"));
        spinSubsamplingY->setMinimum(0.01);
        spinSubsamplingY->setMaximum(10);
        spinSubsamplingY->setSingleStep(0.05);
        spinSubsamplingY->setValue(1);

        gridLayout_3->addWidget(spinSubsamplingY, 0, 3, 1, 1);

        spinSubsamplingZ = new QDoubleSpinBox(CSeriesSelectionDialog);
        spinSubsamplingZ->setObjectName(QStringLiteral("spinSubsamplingZ"));
        spinSubsamplingZ->setMinimum(0.01);
        spinSubsamplingZ->setMaximum(10);
        spinSubsamplingZ->setSingleStep(0.05);
        spinSubsamplingZ->setValue(1);

        gridLayout_3->addWidget(spinSubsamplingZ, 0, 5, 1, 1);

        label = new QLabel(CSeriesSelectionDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(CSeriesSelectionDialog);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_2, 0, 2, 1, 1);

        label_3 = new QLabel(CSeriesSelectionDialog);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(label_3, 0, 4, 1, 1);

        gridLayout_3->setColumnStretch(0, 1);
        gridLayout_3->setColumnStretch(1, 5);
        gridLayout_3->setColumnStretch(2, 1);
        gridLayout_3->setColumnStretch(3, 5);
        gridLayout_3->setColumnStretch(4, 1);
        gridLayout_3->setColumnStretch(5, 5);

        gridLayout->addLayout(gridLayout_3, 1, 1, 1, 1);

        tableWidget = new QTableWidget(CSeriesSelectionDialog);
        if (tableWidget->columnCount() < 3)
            tableWidget->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->setColumnCount(3);
        tableWidget->horizontalHeader()->setVisible(true);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setDefaultSectionSize(128);
        tableWidget->horizontalHeader()->setHighlightSections(false);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setVisible(false);

        gridLayout->addWidget(tableWidget, 0, 0, 1, 2);

        buttonBox = new QDialogButtonBox(CSeriesSelectionDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 3, 0, 1, 2);


        retranslateUi(CSeriesSelectionDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CSeriesSelectionDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CSeriesSelectionDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CSeriesSelectionDialog);
    } // setupUi

    void retranslateUi(QDialog *CSeriesSelectionDialog)
    {
        CSeriesSelectionDialog->setWindowTitle(QApplication::translate("CSeriesSelectionDialog", "Please, select one of the found DICOM series...", 0));
        label_9->setText(QApplication::translate("CSeriesSelectionDialog", "Sampling Factors", 0));
        label->setText(QApplication::translate("CSeriesSelectionDialog", "X", 0));
        label_2->setText(QApplication::translate("CSeriesSelectionDialog", "Y", 0));
        label_3->setText(QApplication::translate("CSeriesSelectionDialog", "Z", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("CSeriesSelectionDialog", "Preview", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("CSeriesSelectionDialog", "Series", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("CSeriesSelectionDialog", "Image Data", 0));
    } // retranslateUi

};

namespace Ui {
    class CSeriesSelectionDialog: public Ui_CSeriesSelectionDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CSERIESSELECTIONDIALOG_H
