/********************************************************************************
** Form generated from reading UI file 'cvolumelimiterdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CVOLUMELIMITERDIALOG_H
#define UI_CVOLUMELIMITERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CVolumeLimiterDialog
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QGroupBox *groupBoxXY;
    QGroupBox *groupBoxXZ;
    QGroupBox *groupBoxYZ;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QLabel *label;
    QComboBox *comboBoxMode;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout;
    QFormLayout *formLayout_2;
    QLabel *label_2;
    QLineEdit *editMinX;
    QLabel *label_3;
    QLineEdit *editMinY;
    QLabel *label_4;
    QLineEdit *editMinZ;
    QFormLayout *formLayout_3;
    QLabel *label_5;
    QLineEdit *editMaxX;
    QLabel *label_6;
    QLineEdit *editMaxY;
    QLabel *label_7;
    QLineEdit *editMaxZ;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonResetZoom;
    QSpacerItem *horizontalSpacer;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CVolumeLimiterDialog)
    {
        if (CVolumeLimiterDialog->objectName().isEmpty())
            CVolumeLimiterDialog->setObjectName(QStringLiteral("CVolumeLimiterDialog"));
        CVolumeLimiterDialog->resize(650, 486);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CVolumeLimiterDialog->sizePolicy().hasHeightForWidth());
        CVolumeLimiterDialog->setSizePolicy(sizePolicy);
        CVolumeLimiterDialog->setSizeGripEnabled(true);
        gridLayout_2 = new QGridLayout(CVolumeLimiterDialog);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBoxXY = new QGroupBox(CVolumeLimiterDialog);
        groupBoxXY->setObjectName(QStringLiteral("groupBoxXY"));
        groupBoxXY->setMinimumSize(QSize(260, 230));

        gridLayout->addWidget(groupBoxXY, 0, 0, 1, 1);

        groupBoxXZ = new QGroupBox(CVolumeLimiterDialog);
        groupBoxXZ->setObjectName(QStringLiteral("groupBoxXZ"));
        sizePolicy.setHeightForWidth(groupBoxXZ->sizePolicy().hasHeightForWidth());
        groupBoxXZ->setSizePolicy(sizePolicy);
        groupBoxXZ->setMinimumSize(QSize(260, 230));

        gridLayout->addWidget(groupBoxXZ, 0, 1, 1, 1);

        groupBoxYZ = new QGroupBox(CVolumeLimiterDialog);
        groupBoxYZ->setObjectName(QStringLiteral("groupBoxYZ"));
        sizePolicy.setHeightForWidth(groupBoxYZ->sizePolicy().hasHeightForWidth());
        groupBoxYZ->setSizePolicy(sizePolicy);
        groupBoxYZ->setMinimumSize(QSize(260, 230));

        gridLayout->addWidget(groupBoxYZ, 1, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(CVolumeLimiterDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout = new QFormLayout(groupBox);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        comboBoxMode = new QComboBox(groupBox);
        comboBoxMode->setObjectName(QStringLiteral("comboBoxMode"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBoxMode);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(CVolumeLimiterDialog);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        horizontalLayout = new QHBoxLayout(groupBox_2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_2);

        editMinX = new QLineEdit(groupBox_2);
        editMinX->setObjectName(QStringLiteral("editMinX"));
        editMinX->setEnabled(false);
        editMinX->setReadOnly(true);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, editMinX);

        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_3);

        editMinY = new QLineEdit(groupBox_2);
        editMinY->setObjectName(QStringLiteral("editMinY"));
        editMinY->setEnabled(false);
        editMinY->setReadOnly(true);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, editMinY);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        editMinZ = new QLineEdit(groupBox_2);
        editMinZ->setObjectName(QStringLiteral("editMinZ"));
        editMinZ->setEnabled(false);
        editMinZ->setReadOnly(true);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, editMinZ);


        horizontalLayout->addLayout(formLayout_2);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        label_5 = new QLabel(groupBox_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_5);

        editMaxX = new QLineEdit(groupBox_2);
        editMaxX->setObjectName(QStringLiteral("editMaxX"));
        editMaxX->setEnabled(false);
        editMaxX->setReadOnly(true);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, editMaxX);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_6);

        editMaxY = new QLineEdit(groupBox_2);
        editMaxY->setObjectName(QStringLiteral("editMaxY"));
        editMaxY->setEnabled(false);
        editMaxY->setReadOnly(true);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, editMaxY);

        label_7 = new QLabel(groupBox_2);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_3->setWidget(2, QFormLayout::LabelRole, label_7);

        editMaxZ = new QLineEdit(groupBox_2);
        editMaxZ->setObjectName(QStringLiteral("editMaxZ"));
        editMaxZ->setEnabled(false);
        editMaxZ->setReadOnly(true);

        formLayout_3->setWidget(2, QFormLayout::FieldRole, editMaxZ);


        horizontalLayout->addLayout(formLayout_3);


        verticalLayout->addWidget(groupBox_2);

        verticalSpacer = new QSpacerItem(27, 13, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        buttonResetZoom = new QPushButton(CVolumeLimiterDialog);
        buttonResetZoom->setObjectName(QStringLiteral("buttonResetZoom"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/magnifier.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonResetZoom->setIcon(icon);

        horizontalLayout_2->addWidget(buttonResetZoom);

        horizontalSpacer = new QSpacerItem(38, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        buttonBox = new QDialogButtonBox(CVolumeLimiterDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        horizontalLayout_2->addWidget(buttonBox);


        verticalLayout->addLayout(horizontalLayout_2);


        gridLayout->addLayout(verticalLayout, 1, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(CVolumeLimiterDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CVolumeLimiterDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CVolumeLimiterDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CVolumeLimiterDialog);
    } // setupUi

    void retranslateUi(QDialog *CVolumeLimiterDialog)
    {
        CVolumeLimiterDialog->setWindowTitle(QApplication::translate("CVolumeLimiterDialog", "Please, select volume of your interest...", 0));
        groupBoxXY->setTitle(QApplication::translate("CVolumeLimiterDialog", "XY View", 0));
        groupBoxXZ->setTitle(QApplication::translate("CVolumeLimiterDialog", "XZ View", 0));
        groupBoxYZ->setTitle(QApplication::translate("CVolumeLimiterDialog", "YZ View", 0));
        groupBox->setTitle(QApplication::translate("CVolumeLimiterDialog", "Options", 0));
        label->setText(QApplication::translate("CVolumeLimiterDialog", "Imaging Mode", 0));
        comboBoxMode->clear();
        comboBoxMode->insertItems(0, QStringList()
         << QApplication::translate("CVolumeLimiterDialog", "RTG", 0)
         << QApplication::translate("CVolumeLimiterDialog", "MIP", 0)
        );
#ifndef QT_NO_TOOLTIP
        comboBoxMode->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Mode of imaging", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        comboBoxMode->setStatusTip(QApplication::translate("CVolumeLimiterDialog", "Selects mode of imaging.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_2->setTitle(QApplication::translate("CVolumeLimiterDialog", "Volume of Interest", 0));
        label_2->setText(QApplication::translate("CVolumeLimiterDialog", "Min X", 0));
#ifndef QT_NO_TOOLTIP
        editMinX->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("CVolumeLimiterDialog", "Min Y", 0));
#ifndef QT_NO_TOOLTIP
        editMinY->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("CVolumeLimiterDialog", "Min Z", 0));
#ifndef QT_NO_TOOLTIP
        editMinZ->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("CVolumeLimiterDialog", "Max X", 0));
#ifndef QT_NO_TOOLTIP
        editMaxX->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("CVolumeLimiterDialog", "Max Y", 0));
#ifndef QT_NO_TOOLTIP
        editMaxY->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("CVolumeLimiterDialog", "Max Z", 0));
#ifndef QT_NO_TOOLTIP
        editMaxZ->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Volume of interest", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        buttonResetZoom->setToolTip(QApplication::translate("CVolumeLimiterDialog", "Resets window zoom.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        buttonResetZoom->setStatusTip(QApplication::translate("CVolumeLimiterDialog", "Resets window zoom.", 0));
#endif // QT_NO_STATUSTIP
        buttonResetZoom->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CVolumeLimiterDialog: public Ui_CVolumeLimiterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CVOLUMELIMITERDIALOG_H
