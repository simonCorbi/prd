/********************************************************************************
** Form generated from reading UI file 'orthosliceswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORTHOSLICESWIDGET_H
#define UI_ORTHOSLICESWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OrthoSlicesWidget
{
public:
    QVBoxLayout *verticalLayout_4;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QSlider *xySliceSlider;
    QSpinBox *xySpinBox;
    QComboBox *xySliceModeCombo;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QSlider *xzSliceSlider;
    QSpinBox *xzSpinBox;
    QComboBox *xzSliceModeCombo;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_3;
    QSlider *yzSliceSlider;
    QSpinBox *yzSpinBox;
    QComboBox *yzSliceModeCombo;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *OrthoSlicesWidget)
    {
        if (OrthoSlicesWidget->objectName().isEmpty())
            OrthoSlicesWidget->setObjectName(QStringLiteral("OrthoSlicesWidget"));
        OrthoSlicesWidget->resize(252, 280);
        OrthoSlicesWidget->setMinimumSize(QSize(252, 280));
        OrthoSlicesWidget->setMaximumSize(QSize(16777215, 16777215));
        verticalLayout_4 = new QVBoxLayout(OrthoSlicesWidget);
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        scrollArea = new QScrollArea(OrthoSlicesWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 252, 280));
        verticalLayout = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        xySliceSlider = new QSlider(groupBox);
        xySliceSlider->setObjectName(QStringLiteral("xySliceSlider"));
        xySliceSlider->setMaximum(64);
        xySliceSlider->setOrientation(Qt::Horizontal);
        xySliceSlider->setTickPosition(QSlider::NoTicks);

        gridLayout->addWidget(xySliceSlider, 0, 0, 1, 1);

        xySpinBox = new QSpinBox(groupBox);
        xySpinBox->setObjectName(QStringLiteral("xySpinBox"));
        xySpinBox->setMinimumSize(QSize(60, 0));
        xySpinBox->setMaximum(64);

        gridLayout->addWidget(xySpinBox, 0, 1, 1, 1);

        xySliceModeCombo = new QComboBox(groupBox);
        xySliceModeCombo->setObjectName(QStringLiteral("xySliceModeCombo"));

        gridLayout->addWidget(xySliceModeCombo, 1, 0, 1, 2);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        xzSliceSlider = new QSlider(groupBox_2);
        xzSliceSlider->setObjectName(QStringLiteral("xzSliceSlider"));
        xzSliceSlider->setMaximum(64);
        xzSliceSlider->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(xzSliceSlider, 0, 0, 1, 1);

        xzSpinBox = new QSpinBox(groupBox_2);
        xzSpinBox->setObjectName(QStringLiteral("xzSpinBox"));
        xzSpinBox->setMinimumSize(QSize(60, 0));

        gridLayout_2->addWidget(xzSpinBox, 0, 1, 1, 1);

        xzSliceModeCombo = new QComboBox(groupBox_2);
        xzSliceModeCombo->setObjectName(QStringLiteral("xzSliceModeCombo"));

        gridLayout_2->addWidget(xzSliceModeCombo, 1, 0, 1, 2);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        gridLayout_3 = new QGridLayout(groupBox_3);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        yzSliceSlider = new QSlider(groupBox_3);
        yzSliceSlider->setObjectName(QStringLiteral("yzSliceSlider"));
        yzSliceSlider->setMaximum(64);
        yzSliceSlider->setOrientation(Qt::Horizontal);

        gridLayout_3->addWidget(yzSliceSlider, 0, 0, 1, 1);

        yzSpinBox = new QSpinBox(groupBox_3);
        yzSpinBox->setObjectName(QStringLiteral("yzSpinBox"));
        yzSpinBox->setMinimumSize(QSize(60, 0));
        yzSpinBox->setMaximum(64);

        gridLayout_3->addWidget(yzSpinBox, 0, 1, 1, 1);

        yzSliceModeCombo = new QComboBox(groupBox_3);
        yzSliceModeCombo->setObjectName(QStringLiteral("yzSliceModeCombo"));

        gridLayout_3->addWidget(yzSliceModeCombo, 1, 0, 1, 2);


        verticalLayout->addWidget(groupBox_3);

        verticalSpacer = new QSpacerItem(20, 4, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_4->addWidget(scrollArea);


        retranslateUi(OrthoSlicesWidget);

        QMetaObject::connectSlotsByName(OrthoSlicesWidget);
    } // setupUi

    void retranslateUi(QWidget *OrthoSlicesWidget)
    {
        OrthoSlicesWidget->setWindowTitle(QApplication::translate("OrthoSlicesWidget", "Ortho Slices", 0));
        groupBox->setTitle(QApplication::translate("OrthoSlicesWidget", "Axial / XY Slice", 0));
#ifndef QT_NO_TOOLTIP
        xySliceSlider->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xySliceSlider->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        xySpinBox->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xySpinBox->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
        xySliceModeCombo->clear();
        xySliceModeCombo->insertItems(0, QStringList()
         << QApplication::translate("OrthoSlicesWidget", "Default", 0)
         << QApplication::translate("OrthoSlicesWidget", "MIP", 0)
         << QApplication::translate("OrthoSlicesWidget", "RTG", 0)
        );
#ifndef QT_NO_TOOLTIP
        xySliceModeCombo->setToolTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xySliceModeCombo->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_2->setTitle(QApplication::translate("OrthoSlicesWidget", "Coronal / XZ Slice", 0));
#ifndef QT_NO_TOOLTIP
        xzSliceSlider->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xzSliceSlider->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        xzSpinBox->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xzSpinBox->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
        xzSliceModeCombo->clear();
        xzSliceModeCombo->insertItems(0, QStringList()
         << QApplication::translate("OrthoSlicesWidget", "Default", 0)
         << QApplication::translate("OrthoSlicesWidget", "MIP", 0)
         << QApplication::translate("OrthoSlicesWidget", "RTG", 0)
        );
#ifndef QT_NO_TOOLTIP
        xzSliceModeCombo->setToolTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        xzSliceModeCombo->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_3->setTitle(QApplication::translate("OrthoSlicesWidget", "Sagittal / YZ Slice", 0));
#ifndef QT_NO_TOOLTIP
        yzSliceSlider->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        yzSliceSlider->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        yzSpinBox->setToolTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        yzSpinBox->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Adjusts slice position.", 0));
#endif // QT_NO_STATUSTIP
        yzSliceModeCombo->clear();
        yzSliceModeCombo->insertItems(0, QStringList()
         << QApplication::translate("OrthoSlicesWidget", "Default", 0)
         << QApplication::translate("OrthoSlicesWidget", "MIP", 0)
         << QApplication::translate("OrthoSlicesWidget", "RTG", 0)
        );
#ifndef QT_NO_TOOLTIP
        yzSliceModeCombo->setToolTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        yzSliceModeCombo->setStatusTip(QApplication::translate("OrthoSlicesWidget", "Selects displaying mode of slice.", 0));
#endif // QT_NO_STATUSTIP
    } // retranslateUi

};

namespace Ui {
    class OrthoSlicesWidget: public Ui_OrthoSlicesWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORTHOSLICESWIDGET_H
