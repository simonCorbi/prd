/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[126];
    char stringdata0[1906];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 10), // "firstEvent"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 7), // "openVLM"
QT_MOC_LITERAL(4, 31, 10), // "wsFileName"
QT_MOC_LITERAL(5, 42, 9), // "openDICOM"
QT_MOC_LITERAL(6, 52, 12), // "openDICOMZIP"
QT_MOC_LITERAL(7, 65, 8), // "fileName"
QT_MOC_LITERAL(8, 74, 8), // "realName"
QT_MOC_LITERAL(9, 83, 7), // "openSTL"
QT_MOC_LITERAL(10, 91, 17), // "saveOriginalDICOM"
QT_MOC_LITERAL(11, 109, 9), // "saveDICOM"
QT_MOC_LITERAL(12, 119, 9), // "saveVLMAs"
QT_MOC_LITERAL(13, 129, 7), // "saveSTL"
QT_MOC_LITERAL(14, 137, 5), // "print"
QT_MOC_LITERAL(15, 143, 19), // "triggerPluginAction"
QT_MOC_LITERAL(16, 163, 10), // "pluginName"
QT_MOC_LITERAL(17, 174, 10), // "actionName"
QT_MOC_LITERAL(18, 185, 19), // "sendDataExpressData"
QT_MOC_LITERAL(19, 205, 14), // "measureDensity"
QT_MOC_LITERAL(20, 220, 15), // "measureDistance"
QT_MOC_LITERAL(21, 236, 17), // "clearMeasurements"
QT_MOC_LITERAL(22, 254, 21), // "showPreferencesDialog"
QT_MOC_LITERAL(23, 276, 18), // "showDataProperties"
QT_MOC_LITERAL(24, 295, 8), // "loadHelp"
QT_MOC_LITERAL(25, 304, 8), // "showHelp"
QT_MOC_LITERAL(26, 313, 9), // "showAbout"
QT_MOC_LITERAL(27, 323, 16), // "showAboutPlugins"
QT_MOC_LITERAL(28, 340, 14), // "actionsEnabler"
QT_MOC_LITERAL(29, 355, 15), // "toolbarsEnabler"
QT_MOC_LITERAL(30, 371, 20), // "shallUpdateOSGCanvas"
QT_MOC_LITERAL(31, 392, 10), // "OSGCanvas*"
QT_MOC_LITERAL(32, 403, 7), // "pCanvas"
QT_MOC_LITERAL(33, 411, 8), // "mousePos"
QT_MOC_LITERAL(34, 420, 10), // "show_frame"
QT_MOC_LITERAL(35, 431, 15), // "showMainToolBar"
QT_MOC_LITERAL(36, 447, 16), // "showViewsToolBar"
QT_MOC_LITERAL(37, 464, 16), // "showMouseToolBar"
QT_MOC_LITERAL(38, 481, 21), // "showVisibilityToolBar"
QT_MOC_LITERAL(39, 503, 17), // "showPanelsToolBar"
QT_MOC_LITERAL(40, 521, 10), // "show3DView"
QT_MOC_LITERAL(41, 532, 13), // "showAxialView"
QT_MOC_LITERAL(42, 546, 15), // "showCoronalView"
QT_MOC_LITERAL(43, 562, 16), // "showSagittalView"
QT_MOC_LITERAL(44, 579, 22), // "showDensityWindowPanel"
QT_MOC_LITERAL(45, 602, 20), // "showOrthoSlicesPanel"
QT_MOC_LITERAL(46, 623, 21), // "showSegmentationPanel"
QT_MOC_LITERAL(47, 645, 11), // "showVRPanel"
QT_MOC_LITERAL(48, 657, 19), // "showModelsListPanel"
QT_MOC_LITERAL(49, 677, 14), // "showAxialSlice"
QT_MOC_LITERAL(50, 692, 5), // "bShow"
QT_MOC_LITERAL(51, 698, 16), // "showCoronalSlice"
QT_MOC_LITERAL(52, 715, 17), // "showSagittalSlice"
QT_MOC_LITERAL(53, 733, 12), // "showMergedVR"
QT_MOC_LITERAL(54, 746, 16), // "showSurfaceModel"
QT_MOC_LITERAL(55, 763, 25), // "processSurfaceModelExtern"
QT_MOC_LITERAL(56, 789, 24), // "modelVisualizationSmooth"
QT_MOC_LITERAL(57, 814, 22), // "modelVisualizationFlat"
QT_MOC_LITERAL(58, 837, 22), // "modelVisualizationWire"
QT_MOC_LITERAL(59, 860, 22), // "showInformationWidgets"
QT_MOC_LITERAL(60, 883, 16), // "setUpWorkSpace3D"
QT_MOC_LITERAL(61, 900, 18), // "setUpWorkSpaceTabs"
QT_MOC_LITERAL(62, 919, 18), // "setUpWorkSpaceGrid"
QT_MOC_LITERAL(63, 938, 19), // "saveUserPerspective"
QT_MOC_LITERAL(64, 958, 19), // "loadUserPerspective"
QT_MOC_LITERAL(65, 978, 22), // "loadDefaultPerspective"
QT_MOC_LITERAL(66, 1001, 22), // "mouseModeDensityWindow"
QT_MOC_LITERAL(67, 1024, 18), // "mouseModeTrackball"
QT_MOC_LITERAL(68, 1043, 27), // "mouseModeObjectManipulation"
QT_MOC_LITERAL(69, 1071, 13), // "mouseModeZoom"
QT_MOC_LITERAL(70, 1085, 14), // "filterGaussian"
QT_MOC_LITERAL(71, 1100, 12), // "filterMedian"
QT_MOC_LITERAL(72, 1113, 17), // "filterAnisotropic"
QT_MOC_LITERAL(73, 1131, 13), // "filterSharpen"
QT_MOC_LITERAL(74, 1145, 10), // "mixVolumes"
QT_MOC_LITERAL(75, 1156, 25), // "vpl::img::CDensityVolume*"
QT_MOC_LITERAL(76, 1182, 4), // "main"
QT_MOC_LITERAL(77, 1187, 4), // "temp"
QT_MOC_LITERAL(78, 1192, 6), // "mixing"
QT_MOC_LITERAL(79, 1199, 11), // "performUndo"
QT_MOC_LITERAL(80, 1211, 11), // "performRedo"
QT_MOC_LITERAL(81, 1223, 16), // "topSplitterMoved"
QT_MOC_LITERAL(82, 1240, 3), // "pos"
QT_MOC_LITERAL(83, 1244, 5), // "index"
QT_MOC_LITERAL(84, 1250, 19), // "bottomSplitterMoved"
QT_MOC_LITERAL(85, 1270, 24), // "canvasScreenShotToQImage"
QT_MOC_LITERAL(86, 1295, 7), // "QImage*"
QT_MOC_LITERAL(87, 1303, 14), // "nRenderingSize"
QT_MOC_LITERAL(88, 1318, 15), // "bIncludeWidgets"
QT_MOC_LITERAL(89, 1334, 14), // "saveScreenshot"
QT_MOC_LITERAL(90, 1349, 25), // "copyScreenshotToClipboard"
QT_MOC_LITERAL(91, 1375, 9), // "saveSlice"
QT_MOC_LITERAL(92, 1385, 4), // "mode"
QT_MOC_LITERAL(93, 1390, 14), // "updateTabIcons"
QT_MOC_LITERAL(94, 1405, 26), // "dockWidgetVisiblityChanged"
QT_MOC_LITERAL(95, 1432, 7), // "visible"
QT_MOC_LITERAL(96, 1440, 19), // "dockLocationChanged"
QT_MOC_LITERAL(97, 1460, 18), // "Qt::DockWidgetArea"
QT_MOC_LITERAL(98, 1479, 4), // "area"
QT_MOC_LITERAL(99, 1484, 16), // "addToRecentFiles"
QT_MOC_LITERAL(100, 1501, 12), // "onRecentFile"
QT_MOC_LITERAL(101, 1514, 22), // "aboutToShowRecentFiles"
QT_MOC_LITERAL(102, 1537, 15), // "createPopupMenu"
QT_MOC_LITERAL(103, 1553, 6), // "QMenu*"
QT_MOC_LITERAL(104, 1560, 18), // "onPanelContextMenu"
QT_MOC_LITERAL(105, 1579, 22), // "onDockWidgetToggleView"
QT_MOC_LITERAL(106, 1602, 14), // "showPanelsMenu"
QT_MOC_LITERAL(107, 1617, 10), // "fullscreen"
QT_MOC_LITERAL(108, 1628, 24), // "setTextureFilterEqualize"
QT_MOC_LITERAL(109, 1653, 23), // "setTextureFilterSharpen"
QT_MOC_LITERAL(110, 1677, 25), // "aboutToShowViewFilterMenu"
QT_MOC_LITERAL(111, 1703, 13), // "loadShortcuts"
QT_MOC_LITERAL(112, 1717, 13), // "saveShortcuts"
QT_MOC_LITERAL(113, 1731, 20), // "loadShortcutsForMenu"
QT_MOC_LITERAL(114, 1752, 4), // "menu"
QT_MOC_LITERAL(115, 1757, 10), // "QSettings&"
QT_MOC_LITERAL(116, 1768, 8), // "settings"
QT_MOC_LITERAL(117, 1777, 20), // "saveShortcutsForMenu"
QT_MOC_LITERAL(118, 1798, 19), // "isDockWidgetVisible"
QT_MOC_LITERAL(119, 1818, 12), // "QDockWidget*"
QT_MOC_LITERAL(120, 1831, 3), // "pDW"
QT_MOC_LITERAL(121, 1835, 14), // "getActivePanel"
QT_MOC_LITERAL(122, 1850, 16), // "closeActivePanel"
QT_MOC_LITERAL(123, 1867, 9), // "prevPanel"
QT_MOC_LITERAL(124, 1877, 9), // "nextPanel"
QT_MOC_LITERAL(125, 1887, 18) // "createSurfaceModel"

    },
    "MainWindow\0firstEvent\0\0openVLM\0"
    "wsFileName\0openDICOM\0openDICOMZIP\0"
    "fileName\0realName\0openSTL\0saveOriginalDICOM\0"
    "saveDICOM\0saveVLMAs\0saveSTL\0print\0"
    "triggerPluginAction\0pluginName\0"
    "actionName\0sendDataExpressData\0"
    "measureDensity\0measureDistance\0"
    "clearMeasurements\0showPreferencesDialog\0"
    "showDataProperties\0loadHelp\0showHelp\0"
    "showAbout\0showAboutPlugins\0actionsEnabler\0"
    "toolbarsEnabler\0shallUpdateOSGCanvas\0"
    "OSGCanvas*\0pCanvas\0mousePos\0show_frame\0"
    "showMainToolBar\0showViewsToolBar\0"
    "showMouseToolBar\0showVisibilityToolBar\0"
    "showPanelsToolBar\0show3DView\0showAxialView\0"
    "showCoronalView\0showSagittalView\0"
    "showDensityWindowPanel\0showOrthoSlicesPanel\0"
    "showSegmentationPanel\0showVRPanel\0"
    "showModelsListPanel\0showAxialSlice\0"
    "bShow\0showCoronalSlice\0showSagittalSlice\0"
    "showMergedVR\0showSurfaceModel\0"
    "processSurfaceModelExtern\0"
    "modelVisualizationSmooth\0"
    "modelVisualizationFlat\0modelVisualizationWire\0"
    "showInformationWidgets\0setUpWorkSpace3D\0"
    "setUpWorkSpaceTabs\0setUpWorkSpaceGrid\0"
    "saveUserPerspective\0loadUserPerspective\0"
    "loadDefaultPerspective\0mouseModeDensityWindow\0"
    "mouseModeTrackball\0mouseModeObjectManipulation\0"
    "mouseModeZoom\0filterGaussian\0filterMedian\0"
    "filterAnisotropic\0filterSharpen\0"
    "mixVolumes\0vpl::img::CDensityVolume*\0"
    "main\0temp\0mixing\0performUndo\0performRedo\0"
    "topSplitterMoved\0pos\0index\0"
    "bottomSplitterMoved\0canvasScreenShotToQImage\0"
    "QImage*\0nRenderingSize\0bIncludeWidgets\0"
    "saveScreenshot\0copyScreenshotToClipboard\0"
    "saveSlice\0mode\0updateTabIcons\0"
    "dockWidgetVisiblityChanged\0visible\0"
    "dockLocationChanged\0Qt::DockWidgetArea\0"
    "area\0addToRecentFiles\0onRecentFile\0"
    "aboutToShowRecentFiles\0createPopupMenu\0"
    "QMenu*\0onPanelContextMenu\0"
    "onDockWidgetToggleView\0showPanelsMenu\0"
    "fullscreen\0setTextureFilterEqualize\0"
    "setTextureFilterSharpen\0"
    "aboutToShowViewFilterMenu\0loadShortcuts\0"
    "saveShortcuts\0loadShortcutsForMenu\0"
    "menu\0QSettings&\0settings\0saveShortcutsForMenu\0"
    "isDockWidgetVisible\0QDockWidget*\0pDW\0"
    "getActivePanel\0closeActivePanel\0"
    "prevPanel\0nextPanel\0createSurfaceModel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
     100,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  514,    2, 0x08 /* Private */,
       3,    0,  515,    2, 0x08 /* Private */,
       3,    1,  516,    2, 0x08 /* Private */,
       5,    0,  519,    2, 0x08 /* Private */,
       6,    0,  520,    2, 0x08 /* Private */,
       6,    1,  521,    2, 0x08 /* Private */,
       5,    2,  524,    2, 0x08 /* Private */,
       9,    0,  529,    2, 0x08 /* Private */,
       9,    1,  530,    2, 0x08 /* Private */,
      10,    0,  533,    2, 0x08 /* Private */,
      11,    0,  534,    2, 0x08 /* Private */,
      12,    0,  535,    2, 0x08 /* Private */,
      13,    0,  536,    2, 0x08 /* Private */,
      14,    0,  537,    2, 0x08 /* Private */,
      15,    2,  538,    2, 0x08 /* Private */,
      18,    0,  543,    2, 0x08 /* Private */,
      19,    1,  544,    2, 0x08 /* Private */,
      20,    1,  547,    2, 0x08 /* Private */,
      21,    0,  550,    2, 0x08 /* Private */,
      22,    0,  551,    2, 0x08 /* Private */,
      23,    0,  552,    2, 0x08 /* Private */,
      24,    0,  553,    2, 0x08 /* Private */,
      25,    0,  554,    2, 0x08 /* Private */,
      26,    0,  555,    2, 0x08 /* Private */,
      27,    0,  556,    2, 0x08 /* Private */,
      28,    0,  557,    2, 0x08 /* Private */,
      29,    0,  558,    2, 0x08 /* Private */,
      30,    2,  559,    2, 0x08 /* Private */,
      34,    0,  564,    2, 0x08 /* Private */,
      35,    0,  565,    2, 0x08 /* Private */,
      36,    0,  566,    2, 0x08 /* Private */,
      37,    0,  567,    2, 0x08 /* Private */,
      38,    0,  568,    2, 0x08 /* Private */,
      39,    0,  569,    2, 0x08 /* Private */,
      40,    1,  570,    2, 0x08 /* Private */,
      41,    1,  573,    2, 0x08 /* Private */,
      42,    1,  576,    2, 0x08 /* Private */,
      43,    1,  579,    2, 0x08 /* Private */,
      44,    1,  582,    2, 0x08 /* Private */,
      45,    1,  585,    2, 0x08 /* Private */,
      46,    1,  588,    2, 0x08 /* Private */,
      47,    1,  591,    2, 0x08 /* Private */,
      48,    1,  594,    2, 0x08 /* Private */,
      49,    1,  597,    2, 0x08 /* Private */,
      51,    1,  600,    2, 0x08 /* Private */,
      52,    1,  603,    2, 0x08 /* Private */,
      53,    1,  606,    2, 0x08 /* Private */,
      54,    1,  609,    2, 0x08 /* Private */,
      55,    0,  612,    2, 0x08 /* Private */,
      56,    0,  613,    2, 0x08 /* Private */,
      57,    0,  614,    2, 0x08 /* Private */,
      58,    0,  615,    2, 0x08 /* Private */,
      59,    1,  616,    2, 0x08 /* Private */,
      60,    0,  619,    2, 0x08 /* Private */,
      61,    0,  620,    2, 0x08 /* Private */,
      62,    0,  621,    2, 0x08 /* Private */,
      63,    0,  622,    2, 0x08 /* Private */,
      64,    0,  623,    2, 0x08 /* Private */,
      65,    0,  624,    2, 0x08 /* Private */,
      66,    1,  625,    2, 0x08 /* Private */,
      67,    1,  628,    2, 0x08 /* Private */,
      68,    1,  631,    2, 0x08 /* Private */,
      69,    1,  634,    2, 0x08 /* Private */,
      70,    0,  637,    2, 0x08 /* Private */,
      71,    0,  638,    2, 0x08 /* Private */,
      72,    0,  639,    2, 0x08 /* Private */,
      73,    0,  640,    2, 0x08 /* Private */,
      74,    3,  641,    2, 0x08 /* Private */,
      79,    0,  648,    2, 0x08 /* Private */,
      80,    0,  649,    2, 0x08 /* Private */,
      81,    2,  650,    2, 0x08 /* Private */,
      84,    2,  655,    2, 0x08 /* Private */,
      85,    3,  660,    2, 0x08 /* Private */,
      89,    1,  667,    2, 0x08 /* Private */,
      90,    1,  670,    2, 0x08 /* Private */,
      91,    2,  673,    2, 0x08 /* Private */,
      93,    0,  678,    2, 0x08 /* Private */,
      94,    1,  679,    2, 0x08 /* Private */,
      96,    1,  682,    2, 0x08 /* Private */,
      99,    1,  685,    2, 0x08 /* Private */,
     100,    0,  688,    2, 0x08 /* Private */,
     101,    0,  689,    2, 0x08 /* Private */,
     102,    0,  690,    2, 0x08 /* Private */,
     104,    1,  691,    2, 0x08 /* Private */,
     105,    1,  694,    2, 0x08 /* Private */,
     106,    0,  697,    2, 0x08 /* Private */,
     107,    1,  698,    2, 0x08 /* Private */,
     108,    1,  701,    2, 0x08 /* Private */,
     109,    1,  704,    2, 0x08 /* Private */,
     110,    0,  707,    2, 0x08 /* Private */,
     111,    0,  708,    2, 0x08 /* Private */,
     112,    0,  709,    2, 0x08 /* Private */,
     113,    2,  710,    2, 0x08 /* Private */,
     117,    2,  715,    2, 0x08 /* Private */,
     118,    1,  720,    2, 0x08 /* Private */,
     121,    0,  723,    2, 0x08 /* Private */,
     122,    0,  724,    2, 0x08 /* Private */,
     123,    0,  725,    2, 0x08 /* Private */,
     124,    0,  726,    2, 0x08 /* Private */,
     125,    0,  727,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Bool, QMetaType::QString,    4,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Bool, QMetaType::QString,    7,
    QMetaType::Bool, QMetaType::QString, QMetaType::QString,    7,    8,
    QMetaType::Bool,
    QMetaType::Bool, QMetaType::QString,    4,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   16,   17,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, 0x80000000 | 31, QMetaType::QPoint,   32,   33,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   50,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 75, 0x80000000 | 75, QMetaType::Int,   76,   77,   78,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   82,   83,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   82,   83,
    0x80000000 | 86, 0x80000000 | 31, QMetaType::Int, QMetaType::Bool,   32,   87,   88,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void, 0x80000000 | 31,   32,
    QMetaType::Void, 0x80000000 | 31, QMetaType::Int,   32,   92,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   95,
    QMetaType::Void, 0x80000000 | 97,   98,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void,
    QMetaType::Void,
    0x80000000 | 103,
    QMetaType::Void, QMetaType::QPoint,   82,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 103, 0x80000000 | 115,  114,  116,
    QMetaType::Void, 0x80000000 | 103, 0x80000000 | 115,  114,  116,
    QMetaType::Bool, 0x80000000 | 119,  120,
    0x80000000 | 119,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->firstEvent(); break;
        case 1: { bool _r = _t->openVLM();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 2: { bool _r = _t->openVLM((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 3: { bool _r = _t->openDICOM();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 4: { bool _r = _t->openDICOMZIP();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 5: { bool _r = _t->openDICOMZIP((*reinterpret_cast< QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 6: { bool _r = _t->openDICOM((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->openSTL();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: { bool _r = _t->openSTL((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 9: { bool _r = _t->saveOriginalDICOM();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 10: { bool _r = _t->saveDICOM();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 11: { bool _r = _t->saveVLMAs();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 12: { bool _r = _t->saveSTL();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 13: _t->print(); break;
        case 14: _t->triggerPluginAction((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 15: _t->sendDataExpressData(); break;
        case 16: _t->measureDensity((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->measureDistance((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->clearMeasurements(); break;
        case 19: _t->showPreferencesDialog(); break;
        case 20: _t->showDataProperties(); break;
        case 21: _t->loadHelp(); break;
        case 22: _t->showHelp(); break;
        case 23: _t->showAbout(); break;
        case 24: _t->showAboutPlugins(); break;
        case 25: _t->actionsEnabler(); break;
        case 26: _t->toolbarsEnabler(); break;
        case 27: { bool _r = _t->shallUpdateOSGCanvas((*reinterpret_cast< OSGCanvas*(*)>(_a[1])),(*reinterpret_cast< const QPoint(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 28: _t->show_frame(); break;
        case 29: _t->showMainToolBar(); break;
        case 30: _t->showViewsToolBar(); break;
        case 31: _t->showMouseToolBar(); break;
        case 32: _t->showVisibilityToolBar(); break;
        case 33: _t->showPanelsToolBar(); break;
        case 34: _t->show3DView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 35: _t->showAxialView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->showCoronalView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 37: _t->showSagittalView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 38: _t->showDensityWindowPanel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 39: _t->showOrthoSlicesPanel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 40: _t->showSegmentationPanel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 41: _t->showVRPanel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 42: _t->showModelsListPanel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 43: _t->showAxialSlice((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 44: _t->showCoronalSlice((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 45: _t->showSagittalSlice((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 46: _t->showMergedVR((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 47: _t->showSurfaceModel((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 48: _t->processSurfaceModelExtern(); break;
        case 49: _t->modelVisualizationSmooth(); break;
        case 50: _t->modelVisualizationFlat(); break;
        case 51: _t->modelVisualizationWire(); break;
        case 52: _t->showInformationWidgets((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 53: _t->setUpWorkSpace3D(); break;
        case 54: _t->setUpWorkSpaceTabs(); break;
        case 55: _t->setUpWorkSpaceGrid(); break;
        case 56: _t->saveUserPerspective(); break;
        case 57: _t->loadUserPerspective(); break;
        case 58: _t->loadDefaultPerspective(); break;
        case 59: _t->mouseModeDensityWindow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 60: _t->mouseModeTrackball((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 61: _t->mouseModeObjectManipulation((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 62: _t->mouseModeZoom((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 63: _t->filterGaussian(); break;
        case 64: _t->filterMedian(); break;
        case 65: _t->filterAnisotropic(); break;
        case 66: _t->filterSharpen(); break;
        case 67: _t->mixVolumes((*reinterpret_cast< vpl::img::CDensityVolume*(*)>(_a[1])),(*reinterpret_cast< vpl::img::CDensityVolume*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 68: _t->performUndo(); break;
        case 69: _t->performRedo(); break;
        case 70: _t->topSplitterMoved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 71: _t->bottomSplitterMoved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 72: { QImage* _r = _t->canvasScreenShotToQImage((*reinterpret_cast< OSGCanvas*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QImage**>(_a[0]) = _r; }  break;
        case 73: _t->saveScreenshot((*reinterpret_cast< OSGCanvas*(*)>(_a[1]))); break;
        case 74: _t->copyScreenshotToClipboard((*reinterpret_cast< OSGCanvas*(*)>(_a[1]))); break;
        case 75: _t->saveSlice((*reinterpret_cast< OSGCanvas*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 76: _t->updateTabIcons(); break;
        case 77: _t->dockWidgetVisiblityChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 78: _t->dockLocationChanged((*reinterpret_cast< Qt::DockWidgetArea(*)>(_a[1]))); break;
        case 79: _t->addToRecentFiles((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 80: _t->onRecentFile(); break;
        case 81: _t->aboutToShowRecentFiles(); break;
        case 82: { QMenu* _r = _t->createPopupMenu();
            if (_a[0]) *reinterpret_cast< QMenu**>(_a[0]) = _r; }  break;
        case 83: _t->onPanelContextMenu((*reinterpret_cast< const QPoint(*)>(_a[1]))); break;
        case 84: _t->onDockWidgetToggleView((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 85: _t->showPanelsMenu(); break;
        case 86: _t->fullscreen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 87: _t->setTextureFilterEqualize((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 88: _t->setTextureFilterSharpen((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 89: _t->aboutToShowViewFilterMenu(); break;
        case 90: _t->loadShortcuts(); break;
        case 91: _t->saveShortcuts(); break;
        case 92: _t->loadShortcutsForMenu((*reinterpret_cast< QMenu*(*)>(_a[1])),(*reinterpret_cast< QSettings(*)>(_a[2]))); break;
        case 93: _t->saveShortcutsForMenu((*reinterpret_cast< QMenu*(*)>(_a[1])),(*reinterpret_cast< QSettings(*)>(_a[2]))); break;
        case 94: { bool _r = _t->isDockWidgetVisible((*reinterpret_cast< QDockWidget*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 95: { QDockWidget* _r = _t->getActivePanel();
            if (_a[0]) *reinterpret_cast< QDockWidget**>(_a[0]) = _r; }  break;
        case 96: _t->closeActivePanel(); break;
        case 97: _t->prevPanel(); break;
        case 98: _t->nextPanel(); break;
        case 99: _t->createSurfaceModel(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 27:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< OSGCanvas* >(); break;
            }
            break;
        case 72:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< OSGCanvas* >(); break;
            }
            break;
        case 73:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< OSGCanvas* >(); break;
            }
            break;
        case 74:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< OSGCanvas* >(); break;
            }
            break;
        case 75:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< OSGCanvas* >(); break;
            }
            break;
        case 92:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 93:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMenu* >(); break;
            }
            break;
        case 94:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDockWidget* >(); break;
            }
            break;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 100)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 100;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 100)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 100;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
