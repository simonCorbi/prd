/****************************************************************************
** Meta object code from reading C++ file 'cpreferencesdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/cpreferencesdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cpreferencesdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CPreferencesDialog_t {
    QByteArrayData data[14];
    char stringdata0[237];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CPreferencesDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CPreferencesDialog_t qt_meta_stringdata_CPreferencesDialog = {
    {
QT_MOC_LITERAL(0, 0, 18), // "CPreferencesDialog"
QT_MOC_LITERAL(1, 19, 30), // "on_CPreferencesDialog_accepted"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 24), // "on_buttonBGColor_clicked"
QT_MOC_LITERAL(4, 76, 20), // "resetDefaultsPressed"
QT_MOC_LITERAL(5, 97, 10), // "pageChange"
QT_MOC_LITERAL(6, 108, 5), // "index"
QT_MOC_LITERAL(7, 114, 24), // "treeItemSelectionChanged"
QT_MOC_LITERAL(8, 139, 32), // "on_pushButtonSetShortcut_clicked"
QT_MOC_LITERAL(9, 172, 34), // "on_pushButtonClearShortcut_cl..."
QT_MOC_LITERAL(10, 207, 11), // "eventFilter"
QT_MOC_LITERAL(11, 219, 3), // "obj"
QT_MOC_LITERAL(12, 223, 7), // "QEvent*"
QT_MOC_LITERAL(13, 231, 5) // "event"

    },
    "CPreferencesDialog\0on_CPreferencesDialog_accepted\0"
    "\0on_buttonBGColor_clicked\0"
    "resetDefaultsPressed\0pageChange\0index\0"
    "treeItemSelectionChanged\0"
    "on_pushButtonSetShortcut_clicked\0"
    "on_pushButtonClearShortcut_clicked\0"
    "eventFilter\0obj\0QEvent*\0event"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CPreferencesDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x08 /* Private */,
       3,    0,   55,    2, 0x08 /* Private */,
       4,    0,   56,    2, 0x08 /* Private */,
       5,    1,   57,    2, 0x08 /* Private */,
       7,    0,   60,    2, 0x08 /* Private */,
       8,    0,   61,    2, 0x08 /* Private */,
       9,    0,   62,    2, 0x08 /* Private */,
      10,    2,   63,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool, QMetaType::QObjectStar, 0x80000000 | 12,   11,   13,

       0        // eod
};

void CPreferencesDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CPreferencesDialog *_t = static_cast<CPreferencesDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_CPreferencesDialog_accepted(); break;
        case 1: _t->on_buttonBGColor_clicked(); break;
        case 2: _t->resetDefaultsPressed(); break;
        case 3: _t->pageChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->treeItemSelectionChanged(); break;
        case 5: _t->on_pushButtonSetShortcut_clicked(); break;
        case 6: _t->on_pushButtonClearShortcut_clicked(); break;
        case 7: { bool _r = _t->eventFilter((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< QEvent*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    }
}

const QMetaObject CPreferencesDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CPreferencesDialog.data,
      qt_meta_data_CPreferencesDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CPreferencesDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CPreferencesDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CPreferencesDialog.stringdata0))
        return static_cast<void*>(const_cast< CPreferencesDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int CPreferencesDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
