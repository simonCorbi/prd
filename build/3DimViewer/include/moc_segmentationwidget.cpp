/****************************************************************************
** Meta object code from reading C++ file 'segmentationwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/segmentationwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'segmentationwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CSegmentationWidget_t {
    QByteArrayData data[12];
    char stringdata0[294];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CSegmentationWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CSegmentationWidget_t qt_meta_stringdata_CSegmentationWidget = {
    {
QT_MOC_LITERAL(0, 0, 19), // "CSegmentationWidget"
QT_MOC_LITERAL(1, 20, 22), // "on_buttonColor_clicked"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 34), // "on_checkBoxApplyThresholds_cl..."
QT_MOC_LITERAL(4, 79, 35), // "on_highThresholdSlider_valueC..."
QT_MOC_LITERAL(5, 115, 5), // "value"
QT_MOC_LITERAL(6, 121, 34), // "on_lowThresholdSlider_valueCh..."
QT_MOC_LITERAL(7, 156, 23), // "on_buttonPickLo_clicked"
QT_MOC_LITERAL(8, 180, 23), // "on_buttonPickHi_clicked"
QT_MOC_LITERAL(9, 204, 39), // "on_pushButtonCreateSurfaceMod..."
QT_MOC_LITERAL(10, 244, 38), // "on_pushButtonSetToActiveRegio..."
QT_MOC_LITERAL(11, 283, 10) // "firstEvent"

    },
    "CSegmentationWidget\0on_buttonColor_clicked\0"
    "\0on_checkBoxApplyThresholds_clicked\0"
    "on_highThresholdSlider_valueChanged\0"
    "value\0on_lowThresholdSlider_valueChanged\0"
    "on_buttonPickLo_clicked\0on_buttonPickHi_clicked\0"
    "on_pushButtonCreateSurfaceModel_clicked\0"
    "on_pushButtonSetToActiveRegion_clicked\0"
    "firstEvent"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CSegmentationWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    1,   61,    2, 0x08 /* Private */,
       6,    1,   64,    2, 0x08 /* Private */,
       7,    0,   67,    2, 0x08 /* Private */,
       8,    0,   68,    2, 0x08 /* Private */,
       9,    0,   69,    2, 0x08 /* Private */,
      10,    0,   70,    2, 0x08 /* Private */,
      11,    0,   71,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void CSegmentationWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CSegmentationWidget *_t = static_cast<CSegmentationWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_buttonColor_clicked(); break;
        case 1: _t->on_checkBoxApplyThresholds_clicked(); break;
        case 2: _t->on_highThresholdSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_lowThresholdSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_buttonPickLo_clicked(); break;
        case 5: _t->on_buttonPickHi_clicked(); break;
        case 6: _t->on_pushButtonCreateSurfaceModel_clicked(); break;
        case 7: _t->on_pushButtonSetToActiveRegion_clicked(); break;
        case 8: _t->firstEvent(); break;
        default: ;
        }
    }
}

const QMetaObject CSegmentationWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CSegmentationWidget.data,
      qt_meta_data_CSegmentationWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CSegmentationWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CSegmentationWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CSegmentationWidget.stringdata0))
        return static_cast<void*>(const_cast< CSegmentationWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int CSegmentationWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
