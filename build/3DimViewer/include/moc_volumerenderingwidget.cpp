/****************************************************************************
** Meta object code from reading C++ file 'volumerenderingwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/volumerenderingwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'volumerenderingwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CVolumeRenderingWidget_t {
    QByteArrayData data[22];
    char stringdata0[531];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CVolumeRenderingWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CVolumeRenderingWidget_t qt_meta_stringdata_CVolumeRenderingWidget = {
    {
QT_MOC_LITERAL(0, 0, 22), // "CVolumeRenderingWidget"
QT_MOC_LITERAL(1, 23, 25), // "on_radioButtonMIP_toggled"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 7), // "checked"
QT_MOC_LITERAL(4, 58, 28), // "on_radioButtonShaded_toggled"
QT_MOC_LITERAL(5, 87, 26), // "on_radioButtonXRay_toggled"
QT_MOC_LITERAL(6, 114, 29), // "on_radioButtonSurface_toggled"
QT_MOC_LITERAL(7, 144, 28), // "on_radioButtonCustom_toggled"
QT_MOC_LITERAL(8, 173, 39), // "on_comboBoxColoring_currentIn..."
QT_MOC_LITERAL(9, 213, 5), // "index"
QT_MOC_LITERAL(10, 219, 38), // "on_sliderRenderingQuality_val..."
QT_MOC_LITERAL(11, 258, 5), // "value"
QT_MOC_LITERAL(12, 264, 34), // "on_sliderCuttingPlane_valueCh..."
QT_MOC_LITERAL(13, 299, 31), // "on_sliderWindShift_valueChanged"
QT_MOC_LITERAL(14, 331, 31), // "on_sliderWindWidth_valueChanged"
QT_MOC_LITERAL(15, 363, 32), // "on_sliderBrightness_valueChanged"
QT_MOC_LITERAL(16, 396, 30), // "on_sliderContrast_valueChanged"
QT_MOC_LITERAL(17, 427, 35), // "on_sliderSurfTolerance_valueC..."
QT_MOC_LITERAL(18, 463, 35), // "on_sliderSurfSharpness_valueC..."
QT_MOC_LITERAL(19, 499, 12), // "packGroupBox"
QT_MOC_LITERAL(20, 512, 10), // "QGroupBox*"
QT_MOC_LITERAL(21, 523, 7) // "pWidget"

    },
    "CVolumeRenderingWidget\0on_radioButtonMIP_toggled\0"
    "\0checked\0on_radioButtonShaded_toggled\0"
    "on_radioButtonXRay_toggled\0"
    "on_radioButtonSurface_toggled\0"
    "on_radioButtonCustom_toggled\0"
    "on_comboBoxColoring_currentIndexChanged\0"
    "index\0on_sliderRenderingQuality_valueChanged\0"
    "value\0on_sliderCuttingPlane_valueChanged\0"
    "on_sliderWindShift_valueChanged\0"
    "on_sliderWindWidth_valueChanged\0"
    "on_sliderBrightness_valueChanged\0"
    "on_sliderContrast_valueChanged\0"
    "on_sliderSurfTolerance_valueChanged\0"
    "on_sliderSurfSharpness_valueChanged\0"
    "packGroupBox\0QGroupBox*\0pWidget"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CVolumeRenderingWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x08 /* Private */,
       4,    1,   97,    2, 0x08 /* Private */,
       5,    1,  100,    2, 0x08 /* Private */,
       6,    1,  103,    2, 0x08 /* Private */,
       7,    1,  106,    2, 0x08 /* Private */,
       8,    1,  109,    2, 0x08 /* Private */,
      10,    1,  112,    2, 0x08 /* Private */,
      12,    1,  115,    2, 0x08 /* Private */,
      13,    1,  118,    2, 0x08 /* Private */,
      14,    1,  121,    2, 0x08 /* Private */,
      15,    1,  124,    2, 0x08 /* Private */,
      16,    1,  127,    2, 0x08 /* Private */,
      17,    1,  130,    2, 0x08 /* Private */,
      18,    1,  133,    2, 0x08 /* Private */,
      19,    1,  136,    2, 0x08 /* Private */,
      19,    2,  139,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 20, QMetaType::Bool,   21,    3,

       0        // eod
};

void CVolumeRenderingWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CVolumeRenderingWidget *_t = static_cast<CVolumeRenderingWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_radioButtonMIP_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->on_radioButtonShaded_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->on_radioButtonXRay_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->on_radioButtonSurface_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->on_radioButtonCustom_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->on_comboBoxColoring_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_sliderRenderingQuality_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_sliderCuttingPlane_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_sliderWindShift_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_sliderWindWidth_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_sliderBrightness_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_sliderContrast_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->on_sliderSurfTolerance_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_sliderSurfSharpness_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->packGroupBox((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->packGroupBox((*reinterpret_cast< QGroupBox*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject CVolumeRenderingWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CVolumeRenderingWidget.data,
      qt_meta_data_CVolumeRenderingWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CVolumeRenderingWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CVolumeRenderingWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CVolumeRenderingWidget.stringdata0))
        return static_cast<void*>(const_cast< CVolumeRenderingWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int CVolumeRenderingWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
