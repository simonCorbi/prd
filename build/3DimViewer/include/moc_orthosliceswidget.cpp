/****************************************************************************
** Meta object code from reading C++ file 'orthosliceswidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/orthosliceswidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'orthosliceswidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_COrthoSlicesWidget_t {
    QByteArrayData data[10];
    char stringdata0[242];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_COrthoSlicesWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_COrthoSlicesWidget_t qt_meta_stringdata_COrthoSlicesWidget = {
    {
QT_MOC_LITERAL(0, 0, 18), // "COrthoSlicesWidget"
QT_MOC_LITERAL(1, 19, 29), // "on_xySliceSlider_valueChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 5), // "value"
QT_MOC_LITERAL(4, 56, 29), // "on_xzSliceSlider_valueChanged"
QT_MOC_LITERAL(5, 86, 29), // "on_yzSliceSlider_valueChanged"
QT_MOC_LITERAL(6, 116, 39), // "on_xySliceModeCombo_currentIn..."
QT_MOC_LITERAL(7, 156, 5), // "index"
QT_MOC_LITERAL(8, 162, 39), // "on_xzSliceModeCombo_currentIn..."
QT_MOC_LITERAL(9, 202, 39) // "on_yzSliceModeCombo_currentIn..."

    },
    "COrthoSlicesWidget\0on_xySliceSlider_valueChanged\0"
    "\0value\0on_xzSliceSlider_valueChanged\0"
    "on_yzSliceSlider_valueChanged\0"
    "on_xySliceModeCombo_currentIndexChanged\0"
    "index\0on_xzSliceModeCombo_currentIndexChanged\0"
    "on_yzSliceModeCombo_currentIndexChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_COrthoSlicesWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x08 /* Private */,
       4,    1,   47,    2, 0x08 /* Private */,
       5,    1,   50,    2, 0x08 /* Private */,
       6,    1,   53,    2, 0x08 /* Private */,
       8,    1,   56,    2, 0x08 /* Private */,
       9,    1,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void COrthoSlicesWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        COrthoSlicesWidget *_t = static_cast<COrthoSlicesWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_xySliceSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_xzSliceSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_yzSliceSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_xySliceModeCombo_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_xzSliceModeCombo_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_yzSliceModeCombo_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject COrthoSlicesWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_COrthoSlicesWidget.data,
      qt_meta_data_COrthoSlicesWidget,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *COrthoSlicesWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *COrthoSlicesWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_COrthoSlicesWidget.stringdata0))
        return static_cast<void*>(const_cast< COrthoSlicesWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int COrthoSlicesWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
