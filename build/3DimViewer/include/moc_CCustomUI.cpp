/****************************************************************************
** Meta object code from reading C++ file 'CCustomUI.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../applications/3DimViewer/include/CCustomUI.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CCustomUI.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_TabBarMouseFunctionalityEx_t {
    QByteArrayData data[1];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TabBarMouseFunctionalityEx_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TabBarMouseFunctionalityEx_t qt_meta_stringdata_TabBarMouseFunctionalityEx = {
    {
QT_MOC_LITERAL(0, 0, 26) // "TabBarMouseFunctionalityEx"

    },
    "TabBarMouseFunctionalityEx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TabBarMouseFunctionalityEx[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void TabBarMouseFunctionalityEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject TabBarMouseFunctionalityEx::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TabBarMouseFunctionalityEx.data,
      qt_meta_data_TabBarMouseFunctionalityEx,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TabBarMouseFunctionalityEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TabBarMouseFunctionalityEx::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TabBarMouseFunctionalityEx.stringdata0))
        return static_cast<void*>(const_cast< TabBarMouseFunctionalityEx*>(this));
    return QObject::qt_metacast(_clname);
}

int TabBarMouseFunctionalityEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_CRapedSplitter_t {
    QByteArrayData data[1];
    char stringdata0[15];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CRapedSplitter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CRapedSplitter_t qt_meta_stringdata_CRapedSplitter = {
    {
QT_MOC_LITERAL(0, 0, 14) // "CRapedSplitter"

    },
    "CRapedSplitter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CRapedSplitter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void CRapedSplitter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject CRapedSplitter::staticMetaObject = {
    { &QSplitter::staticMetaObject, qt_meta_stringdata_CRapedSplitter.data,
      qt_meta_data_CRapedSplitter,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CRapedSplitter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CRapedSplitter::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CRapedSplitter.stringdata0))
        return static_cast<void*>(const_cast< CRapedSplitter*>(this));
    return QSplitter::qt_metacast(_clname);
}

int CRapedSplitter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSplitter::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_CSliderEx_t {
    QByteArrayData data[1];
    char stringdata0[10];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CSliderEx_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CSliderEx_t qt_meta_stringdata_CSliderEx = {
    {
QT_MOC_LITERAL(0, 0, 9) // "CSliderEx"

    },
    "CSliderEx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CSliderEx[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void CSliderEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject CSliderEx::staticMetaObject = {
    { &QSlider::staticMetaObject, qt_meta_stringdata_CSliderEx.data,
      qt_meta_data_CSliderEx,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CSliderEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CSliderEx::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CSliderEx.stringdata0))
        return static_cast<void*>(const_cast< CSliderEx*>(this));
    return QSlider::qt_metacast(_clname);
}

int CSliderEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSlider::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_CCustomDockWidgetTitle_t {
    QByteArrayData data[16];
    char stringdata0[221];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CCustomDockWidgetTitle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CCustomDockWidgetTitle_t qt_meta_stringdata_CCustomDockWidgetTitle = {
    {
QT_MOC_LITERAL(0, 0, 22), // "CCustomDockWidgetTitle"
QT_MOC_LITERAL(1, 23, 14), // "saveScreenshot"
QT_MOC_LITERAL(2, 38, 0), // ""
QT_MOC_LITERAL(3, 39, 10), // "OSGCanvas*"
QT_MOC_LITERAL(4, 50, 7), // "pCanvas"
QT_MOC_LITERAL(5, 58, 25), // "copyScreenshotToClipboard"
QT_MOC_LITERAL(6, 84, 9), // "saveSlice"
QT_MOC_LITERAL(7, 94, 4), // "mode"
QT_MOC_LITERAL(8, 99, 15), // "btnCloseClicked"
QT_MOC_LITERAL(9, 115, 18), // "btnMaximizeClicked"
QT_MOC_LITERAL(10, 134, 20), // "btnScreenshotClicked"
QT_MOC_LITERAL(11, 155, 14), // "vrValueChanged"
QT_MOC_LITERAL(12, 170, 14), // "xySliderChange"
QT_MOC_LITERAL(13, 185, 5), // "value"
QT_MOC_LITERAL(14, 191, 14), // "xzSliderChange"
QT_MOC_LITERAL(15, 206, 14) // "yzSliderChange"

    },
    "CCustomDockWidgetTitle\0saveScreenshot\0"
    "\0OSGCanvas*\0pCanvas\0copyScreenshotToClipboard\0"
    "saveSlice\0mode\0btnCloseClicked\0"
    "btnMaximizeClicked\0btnScreenshotClicked\0"
    "vrValueChanged\0xySliderChange\0value\0"
    "xzSliderChange\0yzSliderChange"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CCustomDockWidgetTitle[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   64,    2, 0x06 /* Public */,
       5,    1,   67,    2, 0x06 /* Public */,
       6,    2,   70,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   75,    2, 0x09 /* Protected */,
       9,    0,   76,    2, 0x09 /* Protected */,
      10,    0,   77,    2, 0x09 /* Protected */,
      11,    1,   78,    2, 0x09 /* Protected */,
      12,    1,   81,    2, 0x09 /* Protected */,
      14,    1,   84,    2, 0x09 /* Protected */,
      15,    1,   87,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    4,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   13,

       0        // eod
};

void CCustomDockWidgetTitle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CCustomDockWidgetTitle *_t = static_cast<CCustomDockWidgetTitle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->saveScreenshot((*reinterpret_cast< OSGCanvas*(*)>(_a[1]))); break;
        case 1: _t->copyScreenshotToClipboard((*reinterpret_cast< OSGCanvas*(*)>(_a[1]))); break;
        case 2: _t->saveSlice((*reinterpret_cast< OSGCanvas*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->btnCloseClicked(); break;
        case 4: _t->btnMaximizeClicked(); break;
        case 5: _t->btnScreenshotClicked(); break;
        case 6: _t->vrValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->xySliderChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->xzSliderChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->yzSliderChange((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CCustomDockWidgetTitle::*_t)(OSGCanvas * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CCustomDockWidgetTitle::saveScreenshot)) {
                *result = 0;
            }
        }
        {
            typedef void (CCustomDockWidgetTitle::*_t)(OSGCanvas * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CCustomDockWidgetTitle::copyScreenshotToClipboard)) {
                *result = 1;
            }
        }
        {
            typedef void (CCustomDockWidgetTitle::*_t)(OSGCanvas * , int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CCustomDockWidgetTitle::saveSlice)) {
                *result = 2;
            }
        }
    }
}

const QMetaObject CCustomDockWidgetTitle::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CCustomDockWidgetTitle.data,
      qt_meta_data_CCustomDockWidgetTitle,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CCustomDockWidgetTitle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CCustomDockWidgetTitle::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CCustomDockWidgetTitle.stringdata0))
        return static_cast<void*>(const_cast< CCustomDockWidgetTitle*>(this));
    return QWidget::qt_metacast(_clname);
}

int CCustomDockWidgetTitle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void CCustomDockWidgetTitle::saveScreenshot(OSGCanvas * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void CCustomDockWidgetTitle::copyScreenshotToClipboard(OSGCanvas * _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void CCustomDockWidgetTitle::saveSlice(OSGCanvas * _t1, int _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
