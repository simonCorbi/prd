/********************************************************************************
** Form generated from reading UI file 'modelswidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODELSWIDGET_H
#define UI_MODELSWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ModelsWidget
{
public:
    QVBoxLayout *verticalLayout;
    QTableWidget *tableModels;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBoxIOApplyDICOM;

    void setupUi(QWidget *ModelsWidget)
    {
        if (ModelsWidget->objectName().isEmpty())
            ModelsWidget->setObjectName(QStringLiteral("ModelsWidget"));
        ModelsWidget->resize(280, 373);
        verticalLayout = new QVBoxLayout(ModelsWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tableModels = new QTableWidget(ModelsWidget);
        if (tableModels->columnCount() < 5)
            tableModels->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableModels->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableModels->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableModels->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableModels->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableModels->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableModels->setObjectName(QStringLiteral("tableModels"));

        verticalLayout->addWidget(tableModels);

        groupBox = new QGroupBox(ModelsWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        checkBoxIOApplyDICOM = new QCheckBox(groupBox);
        checkBoxIOApplyDICOM->setObjectName(QStringLiteral("checkBoxIOApplyDICOM"));

        verticalLayout_2->addWidget(checkBoxIOApplyDICOM);


        verticalLayout->addWidget(groupBox);


        retranslateUi(ModelsWidget);

        QMetaObject::connectSlotsByName(ModelsWidget);
    } // setupUi

    void retranslateUi(QWidget *ModelsWidget)
    {
        ModelsWidget->setWindowTitle(QApplication::translate("ModelsWidget", "Models", 0));
        QTableWidgetItem *___qtablewidgetitem = tableModels->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("ModelsWidget", "Name", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableModels->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("ModelsWidget", "Visible", 0));
        QTableWidgetItem *___qtablewidgetitem2 = tableModels->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("ModelsWidget", "Cut", 0));
        QTableWidgetItem *___qtablewidgetitem3 = tableModels->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("ModelsWidget", "Color", 0));
        QTableWidgetItem *___qtablewidgetitem4 = tableModels->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("ModelsWidget", "Remove", 0));
        groupBox->setTitle(QApplication::translate("ModelsWidget", "Load/Save", 0));
#ifndef QT_NO_TOOLTIP
        checkBoxIOApplyDICOM->setToolTip(QApplication::translate("ModelsWidget", "Transform according to original DICOM coordinates for later use in other software.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        checkBoxIOApplyDICOM->setStatusTip(QApplication::translate("ModelsWidget", "Transform according to original DICOM coordinates for later use in other software.", 0));
#endif // QT_NO_STATUSTIP
        checkBoxIOApplyDICOM->setText(QApplication::translate("ModelsWidget", "Use DICOM coordinates", 0));
    } // retranslateUi

};

namespace Ui {
    class ModelsWidget: public Ui_ModelsWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODELSWIDGET_H
