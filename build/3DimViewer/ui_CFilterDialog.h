/********************************************************************************
** Form generated from reading UI file 'CFilterDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CFILTERDIALOG_H
#define UI_CFILTERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>

QT_BEGIN_NAMESPACE

class Ui_CFilterDialog
{
public:
    QGridLayout *gridLayout;
    QPushButton *preview;
    QLabel *label_2;
    QDialogButtonBox *buttonBox;
    QSlider *sliderSlice;
    QGroupBox *groupBoxSettings;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSlider *sliderStrength;
    QSpinBox *spinBoxStrength;

    void setupUi(QDialog *CFilterDialog)
    {
        if (CFilterDialog->objectName().isEmpty())
            CFilterDialog->setObjectName(QStringLiteral("CFilterDialog"));
        CFilterDialog->resize(560, 349);
        CFilterDialog->setMinimumSize(QSize(400, 300));
        CFilterDialog->setSizeGripEnabled(true);
        gridLayout = new QGridLayout(CFilterDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        preview = new QPushButton(CFilterDialog);
        preview->setObjectName(QStringLiteral("preview"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(preview->sizePolicy().hasHeightForWidth());
        preview->setSizePolicy(sizePolicy);
        preview->setAutoFillBackground(false);
        preview->setStyleSheet(QStringLiteral("border:1px solid black;"));
        preview->setAutoDefault(false);
        preview->setFlat(true);

        gridLayout->addWidget(preview, 0, 0, 2, 2);

        label_2 = new QLabel(CFilterDialog);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 2, 1, 1);

        buttonBox = new QDialogButtonBox(CFilterDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 4, 0, 1, 3);

        sliderSlice = new QSlider(CFilterDialog);
        sliderSlice->setObjectName(QStringLiteral("sliderSlice"));
        sliderSlice->setOrientation(Qt::Vertical);

        gridLayout->addWidget(sliderSlice, 1, 2, 1, 1);

        groupBoxSettings = new QGroupBox(CFilterDialog);
        groupBoxSettings->setObjectName(QStringLiteral("groupBoxSettings"));
        horizontalLayout = new QHBoxLayout(groupBoxSettings);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(groupBoxSettings);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        sliderStrength = new QSlider(groupBoxSettings);
        sliderStrength->setObjectName(QStringLiteral("sliderStrength"));
        sliderStrength->setMinimum(1);
        sliderStrength->setMaximum(100);
        sliderStrength->setValue(75);
        sliderStrength->setOrientation(Qt::Horizontal);
        sliderStrength->setTickPosition(QSlider::NoTicks);

        horizontalLayout->addWidget(sliderStrength);

        spinBoxStrength = new QSpinBox(groupBoxSettings);
        spinBoxStrength->setObjectName(QStringLiteral("spinBoxStrength"));
        spinBoxStrength->setMinimumSize(QSize(60, 0));
        spinBoxStrength->setMinimum(1);
        spinBoxStrength->setMaximum(100);
        spinBoxStrength->setValue(75);

        horizontalLayout->addWidget(spinBoxStrength);


        gridLayout->addWidget(groupBoxSettings, 2, 0, 1, 3);


        retranslateUi(CFilterDialog);

        QMetaObject::connectSlotsByName(CFilterDialog);
    } // setupUi

    void retranslateUi(QDialog *CFilterDialog)
    {
        CFilterDialog->setWindowTitle(QApplication::translate("CFilterDialog", "Volume Filter", 0));
        preview->setText(QString());
        label_2->setText(QApplication::translate("CFilterDialog", "Slice", 0));
        groupBoxSettings->setTitle(QApplication::translate("CFilterDialog", "Settings", 0));
        label->setText(QApplication::translate("CFilterDialog", "Strength", 0));
        spinBoxStrength->setSuffix(QApplication::translate("CFilterDialog", "%", 0));
    } // retranslateUi

};

namespace Ui {
    class CFilterDialog: public Ui_CFilterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CFILTERDIALOG_H
