/********************************************************************************
** Form generated from reading UI file 'cpreferencesdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CPREFERENCESDIALOG_H
#define UI_CPREFERENCESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CPreferencesDialog
{
public:
    QGridLayout *gridLayout;
    QFrame *line;
    QStackedWidget *stackedWidget;
    QWidget *pageGeneral;
    QFormLayout *formLayout;
    QLabel *label;
    QComboBox *comboBoxLanguage;
    QLabel *label_2;
    QComboBox *comboBoxRenderingMode;
    QCheckBox *checkBoxLogging;
    QPushButton *pushButtonShowLog;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButtonPathLastUsed;
    QRadioButton *radioButtonPathProject;
    QLabel *label_3;
    QPushButton *buttonBGColor;
    QSpacerItem *verticalSpacer;
    QCheckBox *checkBoxLinkModels;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *treeWidget;
    QGroupBox *groupBox_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *lineEditShortCut;
    QPushButton *pushButtonSetShortcut;
    QPushButton *pushButtonClearShortcut;
    QLabel *labelPageName;
    QListWidget *listPages;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CPreferencesDialog)
    {
        if (CPreferencesDialog->objectName().isEmpty())
            CPreferencesDialog->setObjectName(QStringLiteral("CPreferencesDialog"));
        CPreferencesDialog->resize(640, 453);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CPreferencesDialog->sizePolicy().hasHeightForWidth());
        CPreferencesDialog->setSizePolicy(sizePolicy);
        CPreferencesDialog->setMinimumSize(QSize(640, 453));
        CPreferencesDialog->setSizeGripEnabled(true);
        gridLayout = new QGridLayout(CPreferencesDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        line = new QFrame(CPreferencesDialog);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 2, 0, 1, 2);

        stackedWidget = new QStackedWidget(CPreferencesDialog);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setMinimumSize(QSize(364, 221));
        pageGeneral = new QWidget();
        pageGeneral->setObjectName(QStringLiteral("pageGeneral"));
        formLayout = new QFormLayout(pageGeneral);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(pageGeneral);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        comboBoxLanguage = new QComboBox(pageGeneral);
        comboBoxLanguage->setObjectName(QStringLiteral("comboBoxLanguage"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBoxLanguage);

        label_2 = new QLabel(pageGeneral);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        comboBoxRenderingMode = new QComboBox(pageGeneral);
        comboBoxRenderingMode->setObjectName(QStringLiteral("comboBoxRenderingMode"));

        formLayout->setWidget(2, QFormLayout::FieldRole, comboBoxRenderingMode);

        checkBoxLogging = new QCheckBox(pageGeneral);
        checkBoxLogging->setObjectName(QStringLiteral("checkBoxLogging"));

        formLayout->setWidget(4, QFormLayout::LabelRole, checkBoxLogging);

        pushButtonShowLog = new QPushButton(pageGeneral);
        pushButtonShowLog->setObjectName(QStringLiteral("pushButtonShowLog"));

        formLayout->setWidget(4, QFormLayout::FieldRole, pushButtonShowLog);

        groupBox = new QGroupBox(pageGeneral);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        radioButtonPathLastUsed = new QRadioButton(groupBox);
        radioButtonPathLastUsed->setObjectName(QStringLiteral("radioButtonPathLastUsed"));

        verticalLayout->addWidget(radioButtonPathLastUsed);

        radioButtonPathProject = new QRadioButton(groupBox);
        radioButtonPathProject->setObjectName(QStringLiteral("radioButtonPathProject"));

        verticalLayout->addWidget(radioButtonPathProject);


        formLayout->setWidget(6, QFormLayout::SpanningRole, groupBox);

        label_3 = new QLabel(pageGeneral);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_3);

        buttonBGColor = new QPushButton(pageGeneral);
        buttonBGColor->setObjectName(QStringLiteral("buttonBGColor"));
        buttonBGColor->setMaximumSize(QSize(80, 16777215));

        formLayout->setWidget(8, QFormLayout::FieldRole, buttonBGColor);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(9, QFormLayout::SpanningRole, verticalSpacer);

        checkBoxLinkModels = new QCheckBox(pageGeneral);
        checkBoxLinkModels->setObjectName(QStringLiteral("checkBoxLinkModels"));

        formLayout->setWidget(5, QFormLayout::LabelRole, checkBoxLinkModels);

        stackedWidget->addWidget(pageGeneral);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        treeWidget = new QTreeWidget(page);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->header()->setDefaultSectionSize(275);

        verticalLayout_2->addWidget(treeWidget);

        groupBox_2 = new QGroupBox(page);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        horizontalLayout = new QHBoxLayout(groupBox_2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        lineEditShortCut = new QLineEdit(groupBox_2);
        lineEditShortCut->setObjectName(QStringLiteral("lineEditShortCut"));
        lineEditShortCut->setAcceptDrops(false);

        horizontalLayout->addWidget(lineEditShortCut);

        pushButtonSetShortcut = new QPushButton(groupBox_2);
        pushButtonSetShortcut->setObjectName(QStringLiteral("pushButtonSetShortcut"));

        horizontalLayout->addWidget(pushButtonSetShortcut);

        pushButtonClearShortcut = new QPushButton(groupBox_2);
        pushButtonClearShortcut->setObjectName(QStringLiteral("pushButtonClearShortcut"));

        horizontalLayout->addWidget(pushButtonClearShortcut);


        verticalLayout_2->addWidget(groupBox_2);

        stackedWidget->addWidget(page);

        gridLayout->addWidget(stackedWidget, 1, 1, 1, 1);

        labelPageName = new QLabel(CPreferencesDialog);
        labelPageName->setObjectName(QStringLiteral("labelPageName"));
        labelPageName->setMinimumSize(QSize(0, 48));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelPageName->setFont(font);
        labelPageName->setStyleSheet(QLatin1String("background: qlineargradient(x1:1, y1:0, x2:1, y2:1,\n"
"                 stop:0 rgb(0, 100, 176), stop:1 rgb(1, 71, 123));\n"
"color: rgb(255, 255, 255);\n"
"padding-left: 5;\n"
"padding-right: 2;\n"
"background-image: url(:icons/bsp.ico);\n"
"background-position: center right;\n"
"background-origin: content;\n"
"background-repeat: none;"));
        labelPageName->setFrameShape(QFrame::NoFrame);
        labelPageName->setFrameShadow(QFrame::Plain);
        labelPageName->setLineWidth(1);

        gridLayout->addWidget(labelPageName, 0, 1, 1, 1);

        listPages = new QListWidget(CPreferencesDialog);
        new QListWidgetItem(listPages);
        new QListWidgetItem(listPages);
        listPages->setObjectName(QStringLiteral("listPages"));
        listPages->setMinimumSize(QSize(150, 0));
        listPages->setMaximumSize(QSize(200, 16777215));

        gridLayout->addWidget(listPages, 0, 0, 2, 1);

        buttonBox = new QDialogButtonBox(CPreferencesDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok|QDialogButtonBox::RestoreDefaults);

        gridLayout->addWidget(buttonBox, 3, 0, 1, 2);


        retranslateUi(CPreferencesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CPreferencesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CPreferencesDialog, SLOT(reject()));

        stackedWidget->setCurrentIndex(0);
        listPages->setCurrentRow(0);


        QMetaObject::connectSlotsByName(CPreferencesDialog);
    } // setupUi

    void retranslateUi(QDialog *CPreferencesDialog)
    {
        CPreferencesDialog->setWindowTitle(QApplication::translate("CPreferencesDialog", "Preferences", 0));
        label->setText(QApplication::translate("CPreferencesDialog", "Language", 0));
#ifndef QT_NO_TOOLTIP
        comboBoxLanguage->setToolTip(QApplication::translate("CPreferencesDialog", "Selects language used in application.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        comboBoxLanguage->setStatusTip(QApplication::translate("CPreferencesDialog", "Selects language used in application.", 0));
#endif // QT_NO_STATUSTIP
        label_2->setText(QApplication::translate("CPreferencesDialog", "Rendering Mode", 0));
        comboBoxRenderingMode->clear();
        comboBoxRenderingMode->insertItems(0, QStringList()
         << QApplication::translate("CPreferencesDialog", "Single Threaded", 0)
         << QApplication::translate("CPreferencesDialog", "Multi Threaded", 0)
        );
#ifndef QT_NO_TOOLTIP
        comboBoxRenderingMode->setToolTip(QApplication::translate("CPreferencesDialog", "Selects mode of rendering.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        comboBoxRenderingMode->setStatusTip(QApplication::translate("CPreferencesDialog", "Selects mode of rendering.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        checkBoxLogging->setToolTip(QApplication::translate("CPreferencesDialog", "Allows logging.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        checkBoxLogging->setStatusTip(QApplication::translate("CPreferencesDialog", "Allows logging.", 0));
#endif // QT_NO_STATUSTIP
        checkBoxLogging->setText(QApplication::translate("CPreferencesDialog", "Logging", 0));
        pushButtonShowLog->setText(QApplication::translate("CPreferencesDialog", "Show log...", 0));
#ifndef QT_NO_TOOLTIP
        groupBox->setToolTip(QApplication::translate("CPreferencesDialog", "Save path for segmentation and model data", 0));
#endif // QT_NO_TOOLTIP
        groupBox->setTitle(QApplication::translate("CPreferencesDialog", "Save Path", 0));
        radioButtonPathLastUsed->setText(QApplication::translate("CPreferencesDialog", "Last used path", 0));
        radioButtonPathProject->setText(QApplication::translate("CPreferencesDialog", "Current project path", 0));
        label_3->setText(QApplication::translate("CPreferencesDialog", "Background Color", 0));
        buttonBGColor->setText(QString());
        checkBoxLinkModels->setText(QApplication::translate("CPreferencesDialog", "Models mirror region data", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("CPreferencesDialog", "Shortcut", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("CPreferencesDialog", "Action", 0));
        groupBox_2->setTitle(QApplication::translate("CPreferencesDialog", "Shortcut", 0));
        lineEditShortCut->setPlaceholderText(QApplication::translate("CPreferencesDialog", "Type to set shortcut", 0));
        pushButtonSetShortcut->setText(QApplication::translate("CPreferencesDialog", "Set", 0));
        pushButtonClearShortcut->setText(QApplication::translate("CPreferencesDialog", "Clear", 0));
        labelPageName->setText(QApplication::translate("CPreferencesDialog", "General", 0));

        const bool __sortingEnabled = listPages->isSortingEnabled();
        listPages->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listPages->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("CPreferencesDialog", "General", 0));
        QListWidgetItem *___qlistwidgetitem1 = listPages->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("CPreferencesDialog", "Shortcuts", 0));
        listPages->setSortingEnabled(__sortingEnabled);

#ifndef QT_NO_TOOLTIP
        listPages->setToolTip(QString());
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        listPages->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
    } // retranslateUi

};

namespace Ui {
    class CPreferencesDialog: public Ui_CPreferencesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CPREFERENCESDIALOG_H
