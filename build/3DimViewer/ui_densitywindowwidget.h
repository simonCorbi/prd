/********************************************************************************
** Form generated from reading UI file 'densitywindowwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DENSITYWINDOWWIDGET_H
#define UI_DENSITYWINDOWWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DensityWindowWidget
{
public:
    QVBoxLayout *verticalLayout_2;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QSpinBox *densityWindowWidth;
    QSpinBox *densityWindowCenter;
    QLabel *label;
    QLabel *label_2;
    QSlider *densityWindowCenterSlider;
    QSlider *densityWindowWidthSlider;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *DensityWindowWidget)
    {
        if (DensityWindowWidget->objectName().isEmpty())
            DensityWindowWidget->setObjectName(QStringLiteral("DensityWindowWidget"));
        DensityWindowWidget->resize(251, 250);
        DensityWindowWidget->setMinimumSize(QSize(251, 250));
        verticalLayout_2 = new QVBoxLayout(DensityWindowWidget);
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        scrollArea = new QScrollArea(DensityWindowWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 251, 250));
        verticalLayout_3 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        densityWindowWidth = new QSpinBox(groupBox);
        densityWindowWidth->setObjectName(QStringLiteral("densityWindowWidth"));
        densityWindowWidth->setMaximum(8500);
        densityWindowWidth->setSingleStep(100);

        gridLayout->addWidget(densityWindowWidth, 2, 2, 1, 1);

        densityWindowCenter = new QSpinBox(groupBox);
        densityWindowCenter->setObjectName(QStringLiteral("densityWindowCenter"));
        densityWindowCenter->setMinimum(-1500);
        densityWindowCenter->setMaximum(7000);
        densityWindowCenter->setSingleStep(100);

        gridLayout->addWidget(densityWindowCenter, 1, 2, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        densityWindowCenterSlider = new QSlider(groupBox);
        densityWindowCenterSlider->setObjectName(QStringLiteral("densityWindowCenterSlider"));
        densityWindowCenterSlider->setMinimum(-1500);
        densityWindowCenterSlider->setMaximum(7000);
        densityWindowCenterSlider->setSingleStep(100);
        densityWindowCenterSlider->setPageStep(500);
        densityWindowCenterSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(densityWindowCenterSlider, 1, 1, 1, 1);

        densityWindowWidthSlider = new QSlider(groupBox);
        densityWindowWidthSlider->setObjectName(QStringLiteral("densityWindowWidthSlider"));
        densityWindowWidthSlider->setMaximum(8500);
        densityWindowWidthSlider->setSingleStep(100);
        densityWindowWidthSlider->setPageStep(500);
        densityWindowWidthSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(densityWindowWidthSlider, 2, 1, 1, 1);


        verticalLayout_3->addWidget(groupBox);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(groupBox_2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(groupBox_2);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);


        verticalLayout_3->addWidget(groupBox_2);

        verticalSpacer = new QSpacerItem(20, 24, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_2->addWidget(scrollArea);


        retranslateUi(DensityWindowWidget);

        QMetaObject::connectSlotsByName(DensityWindowWidget);
    } // setupUi

    void retranslateUi(QWidget *DensityWindowWidget)
    {
        DensityWindowWidget->setWindowTitle(QApplication::translate("DensityWindowWidget", "Density Window", 0));
        groupBox->setTitle(QApplication::translate("DensityWindowWidget", "Current Settings", 0));
#ifndef QT_NO_TOOLTIP
        densityWindowWidth->setToolTip(QApplication::translate("DensityWindowWidget", "Adjusts width of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        densityWindowWidth->setStatusTip(QApplication::translate("DensityWindowWidget", "Adjusts width of density window.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        densityWindowCenter->setToolTip(QApplication::translate("DensityWindowWidget", "Adjusts center of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        densityWindowCenter->setStatusTip(QApplication::translate("DensityWindowWidget", "Adjusts center of density window.", 0));
#endif // QT_NO_STATUSTIP
        label->setText(QApplication::translate("DensityWindowWidget", "Center", 0));
        label_2->setText(QApplication::translate("DensityWindowWidget", "Width", 0));
        groupBox_2->setTitle(QApplication::translate("DensityWindowWidget", "Predefined Windows", 0));
#ifndef QT_NO_TOOLTIP
        pushButton->setToolTip(QApplication::translate("DensityWindowWidget", "Selects default setting of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pushButton->setStatusTip(QApplication::translate("DensityWindowWidget", "Selects default setting of density window.", 0));
#endif // QT_NO_STATUSTIP
        pushButton->setText(QApplication::translate("DensityWindowWidget", "Default", 0));
#ifndef QT_NO_TOOLTIP
        pushButton_2->setToolTip(QApplication::translate("DensityWindowWidget", "Analyzes data and selects optimal setting of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pushButton_2->setStatusTip(QApplication::translate("DensityWindowWidget", "Analyzes data and selects optimal setting of density window.", 0));
#endif // QT_NO_STATUSTIP
        pushButton_2->setText(QApplication::translate("DensityWindowWidget", "Find Optimal", 0));
#ifndef QT_NO_TOOLTIP
        pushButton_3->setToolTip(QApplication::translate("DensityWindowWidget", "Selects setting of density window optimal for showing bones.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pushButton_3->setStatusTip(QApplication::translate("DensityWindowWidget", "Selects setting of density window optimal for showing bones.", 0));
#endif // QT_NO_STATUSTIP
        pushButton_3->setText(QApplication::translate("DensityWindowWidget", "Show Bones", 0));
    } // retranslateUi

};

namespace Ui {
    class DensityWindowWidget: public Ui_DensityWindowWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DENSITYWINDOWWIDGET_H
