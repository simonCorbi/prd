/********************************************************************************
** Form generated from reading UI file 'csegmentationpluginpanel.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CSEGMENTATIONPLUGINPANEL_H
#define UI_CSEGMENTATIONPLUGINPANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CSegmentationPluginPanel
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QFormLayout *formLayout;
    QPushButton *pushButtonPickValue;
    QLabel *label;
    QLineEdit *editDensity;
    QLabel *label_2;
    QLineEdit *editCoords;
    QFormLayout *formLayout_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *segmenteButton;
    QDoubleSpinBox *spinBoxCoeffMulti;
    QSpinBox *spinBoxNbItera;
    QSpinBox *spinBoxRadius;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *CSegmentationPluginPanel)
    {
        if (CSegmentationPluginPanel->objectName().isEmpty())
            CSegmentationPluginPanel->setObjectName(QStringLiteral("CSegmentationPluginPanel"));
        CSegmentationPluginPanel->resize(221, 303);
        CSegmentationPluginPanel->setMinimumSize(QSize(221, 303));
        CSegmentationPluginPanel->setFocusPolicy(Qt::ClickFocus);
        verticalLayout_3 = new QVBoxLayout(CSegmentationPluginPanel);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox = new QGroupBox(CSegmentationPluginPanel);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        formLayout = new QFormLayout(groupBox);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        pushButtonPickValue = new QPushButton(groupBox);
        pushButtonPickValue->setObjectName(QStringLiteral("pushButtonPickValue"));

        formLayout->setWidget(0, QFormLayout::SpanningRole, pushButtonPickValue);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        editDensity = new QLineEdit(groupBox);
        editDensity->setObjectName(QStringLiteral("editDensity"));
        editDensity->setEnabled(false);
        editDensity->setAcceptDrops(false);

        formLayout->setWidget(1, QFormLayout::FieldRole, editDensity);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        editCoords = new QLineEdit(groupBox);
        editCoords->setObjectName(QStringLiteral("editCoords"));
        editCoords->setEnabled(false);
        editCoords->setAcceptDrops(false);

        formLayout->setWidget(2, QFormLayout::FieldRole, editCoords);


        verticalLayout_3->addWidget(groupBox);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_3 = new QLabel(CSegmentationPluginPanel);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_3);

        label_4 = new QLabel(CSegmentationPluginPanel);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_4);

        label_5 = new QLabel(CSegmentationPluginPanel);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_5);

        segmenteButton = new QPushButton(CSegmentationPluginPanel);
        segmenteButton->setObjectName(QStringLiteral("segmenteButton"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, segmenteButton);

        spinBoxCoeffMulti = new QDoubleSpinBox(CSegmentationPluginPanel);
        spinBoxCoeffMulti->setObjectName(QStringLiteral("spinBoxCoeffMulti"));
        spinBoxCoeffMulti->setMaximum(5);
        spinBoxCoeffMulti->setSingleStep(0.25);
        spinBoxCoeffMulti->setValue(1.5);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, spinBoxCoeffMulti);

        spinBoxNbItera = new QSpinBox(CSegmentationPluginPanel);
        spinBoxNbItera->setObjectName(QStringLiteral("spinBoxNbItera"));
        spinBoxNbItera->setMaximum(100);
        spinBoxNbItera->setValue(20);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, spinBoxNbItera);

        spinBoxRadius = new QSpinBox(CSegmentationPluginPanel);
        spinBoxRadius->setObjectName(QStringLiteral("spinBoxRadius"));
        spinBoxRadius->setMaximum(10);
        spinBoxRadius->setValue(2);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, spinBoxRadius);


        verticalLayout_3->addLayout(formLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        retranslateUi(CSegmentationPluginPanel);

        QMetaObject::connectSlotsByName(CSegmentationPluginPanel);
    } // setupUi

    void retranslateUi(QWidget *CSegmentationPluginPanel)
    {
        CSegmentationPluginPanel->setWindowTitle(QApplication::translate("CSegmentationPluginPanel", "Segmentation plugin", 0));
        groupBox->setTitle(QApplication::translate("CSegmentationPluginPanel", "Point Selection", 0));
#ifndef QT_NO_TOOLTIP
        pushButtonPickValue->setToolTip(QApplication::translate("CSegmentationPluginPanel", "Press the button and select any point the volume data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        pushButtonPickValue->setStatusTip(QApplication::translate("CSegmentationPluginPanel", "Press the button and select any point the volume data.", 0));
#endif // QT_NO_STATUSTIP
        pushButtonPickValue->setText(QApplication::translate("CSegmentationPluginPanel", "Pick Value", 0));
        label->setText(QApplication::translate("CSegmentationPluginPanel", "Density", 0));
        label_2->setText(QApplication::translate("CSegmentationPluginPanel", "Coords", 0));
        label_3->setText(QApplication::translate("CSegmentationPluginPanel", "Coeff multiplicateur :", 0));
        label_4->setText(QApplication::translate("CSegmentationPluginPanel", "Nb it\303\251rations :", 0));
        label_5->setText(QApplication::translate("CSegmentationPluginPanel", "Radius :", 0));
        segmenteButton->setText(QApplication::translate("CSegmentationPluginPanel", "Segmenter", 0));
    } // retranslateUi

};

namespace Ui {
    class CSegmentationPluginPanel: public Ui_CSegmentationPluginPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CSEGMENTATIONPLUGINPANEL_H
