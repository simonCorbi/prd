/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoad_Patient_Dicom_data;
    QAction *actionLoad_Patient_VLM_Data;
    QAction *actionSave_Volumetric_Data;
    QAction *actionExit;
    QAction *actionPrint;
    QAction *actionUndo;
    QAction *actionRedo;
    QAction *action3D_View;
    QAction *actionAxial_View;
    QAction *actionCoronal_View;
    QAction *actionSagittal_View;
    QAction *actionPanoramic_View;
    QAction *actionNormal_Slice;
    QAction *actionVolume_Rendering_View;
    QAction *actionMain_Toolbar;
    QAction *actionViews_Toolbar;
    QAction *actionDensity_Window;
    QAction *actionOrtho_Slices_Panel;
    QAction *actionTrackball_Mode;
    QAction *actionObject_Manipulation;
    QAction *actionDensity_Window_Adjusting;
    QAction *actionCreate_Surface_Model;
    QAction *actionShow_Surface_Model;
    QAction *actionAxial_Slice;
    QAction *actionCoronal_Slice;
    QAction *actionSagittal_Slice;
    QAction *actionSegmentation_Panel;
    QAction *actionWorkspaceTabs;
    QAction *actionWorkspace3DView;
    QAction *actionMouse_Toolbar;
    QAction *actionVisibility_Toolbar;
    QAction *actionVolume_Rendering_Panel;
    QAction *actionPreferences;
    QAction *actionShow_Data_Properties;
    QAction *actionLoad_STL_Model;
    QAction *actionAbout;
    QAction *actionAbout_Plugins;
    QAction *actionGaussian_Filter;
    QAction *actionMedian_Filter;
    QAction *action3D_Anisotropic_Filter;
    QAction *actionWorkspaceGrid;
    QAction *actionShow_Information_Widgets;
    QAction *actionPanels_Toolbar;
    QAction *actionShow_Help;
    QAction *actionScale_Scene;
    QAction *actionLoad_User_Perspective;
    QAction *actionSave_User_Perspective;
    QAction *actionLoad_Default_Perspective;
    QAction *actionSave_STL_Model;
    QAction *actionSend_Data;
    QAction *actionMeasure_Density_Value;
    QAction *actionMeasure_Distance;
    QAction *actionClear_Measurements;
    QAction *actionVolume_Rendering;
    QAction *actionSave_DICOM_Series;
    QAction *actionImport_DICOM_Data_from_ZIP_archive;
    QAction *actionModels_List_Panel;
    QAction *actionViewEqualize;
    QAction *actionViewSharpen;
    QAction *actionSave_Original_DICOM_Series;
    QAction *actionClose_Active_Panel;
    QAction *actionPrevious_Panel;
    QAction *actionNext_Panel;
    QAction *actionVolume_Filter;
    QAction *actionSharpening_Filter;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuArchive;
    QMenu *menuRecent;
    QMenu *menuEdit;
    QMenu *menuSwitch_Mouse_Mode;
    QMenu *menuView;
    QMenu *menuViews;
    QMenu *menuToolbars;
    QMenu *menuControl_Panels;
    QMenu *menu3D_Scene;
    QMenu *menuWorkspaces;
    QMenu *menuModel;
    QMenu *menuHelp;
    QMenu *menuFiltering;
    QMenu *menuViewFilter;
    QMenu *menuMeasurements;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *panelsToolBar;
    QToolBar *viewsToolBar;
    QToolBar *mouseToolBar;
    QToolBar *visibilityToolBar;
    QToolBar *appToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(667, 437);
        MainWindow->setDockOptions(QMainWindow::AllowNestedDocks|QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        actionLoad_Patient_Dicom_data = new QAction(MainWindow);
        actionLoad_Patient_Dicom_data->setObjectName(QStringLiteral("actionLoad_Patient_Dicom_data"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/folder_open_up.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLoad_Patient_Dicom_data->setIcon(icon);
        actionLoad_Patient_VLM_Data = new QAction(MainWindow);
        actionLoad_Patient_VLM_Data->setObjectName(QStringLiteral("actionLoad_Patient_VLM_Data"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/folder_open_arrow.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionLoad_Patient_VLM_Data->setIcon(icon1);
        actionSave_Volumetric_Data = new QAction(MainWindow);
        actionSave_Volumetric_Data->setObjectName(QStringLiteral("actionSave_Volumetric_Data"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/disk.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave_Volumetric_Data->setIcon(icon2);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionExit->setIcon(icon3);
        actionPrint = new QAction(MainWindow);
        actionPrint->setObjectName(QStringLiteral("actionPrint"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/printer_menu.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPrint->setIcon(icon4);
        actionPrint->setVisible(false);
        actionUndo = new QAction(MainWindow);
        actionUndo->setObjectName(QStringLiteral("actionUndo"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/arrow_undo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionUndo->setIcon(icon5);
        actionRedo = new QAction(MainWindow);
        actionRedo->setObjectName(QStringLiteral("actionRedo"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/arrow_redo.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRedo->setIcon(icon6);
        action3D_View = new QAction(MainWindow);
        action3D_View->setObjectName(QStringLiteral("action3D_View"));
        action3D_View->setCheckable(true);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/3d_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        action3D_View->setIcon(icon7);
        action3D_View->setVisible(false);
        actionAxial_View = new QAction(MainWindow);
        actionAxial_View->setObjectName(QStringLiteral("actionAxial_View"));
        actionAxial_View->setCheckable(true);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/xy_ortho_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAxial_View->setIcon(icon8);
        actionCoronal_View = new QAction(MainWindow);
        actionCoronal_View->setObjectName(QStringLiteral("actionCoronal_View"));
        actionCoronal_View->setCheckable(true);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/xz_ortho_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCoronal_View->setIcon(icon9);
        actionSagittal_View = new QAction(MainWindow);
        actionSagittal_View->setObjectName(QStringLiteral("actionSagittal_View"));
        actionSagittal_View->setCheckable(true);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/yz_ortho_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSagittal_View->setIcon(icon10);
        actionPanoramic_View = new QAction(MainWindow);
        actionPanoramic_View->setObjectName(QStringLiteral("actionPanoramic_View"));
        actionPanoramic_View->setCheckable(true);
        actionPanoramic_View->setEnabled(false);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/curved_slice.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPanoramic_View->setIcon(icon11);
        actionPanoramic_View->setVisible(true);
        actionNormal_Slice = new QAction(MainWindow);
        actionNormal_Slice->setObjectName(QStringLiteral("actionNormal_Slice"));
        actionNormal_Slice->setCheckable(true);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/normal_slice.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionNormal_Slice->setIcon(icon12);
        actionNormal_Slice->setVisible(true);
        actionVolume_Rendering_View = new QAction(MainWindow);
        actionVolume_Rendering_View->setObjectName(QStringLiteral("actionVolume_Rendering_View"));
        actionVolume_Rendering_View->setCheckable(true);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/volume_rendering_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionVolume_Rendering_View->setIcon(icon13);
        actionMain_Toolbar = new QAction(MainWindow);
        actionMain_Toolbar->setObjectName(QStringLiteral("actionMain_Toolbar"));
        actionMain_Toolbar->setCheckable(true);
        actionMain_Toolbar->setChecked(true);
        actionViews_Toolbar = new QAction(MainWindow);
        actionViews_Toolbar->setObjectName(QStringLiteral("actionViews_Toolbar"));
        actionViews_Toolbar->setCheckable(true);
        actionViews_Toolbar->setChecked(true);
        actionDensity_Window = new QAction(MainWindow);
        actionDensity_Window->setObjectName(QStringLiteral("actionDensity_Window"));
        actionDensity_Window->setCheckable(true);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/density_window.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDensity_Window->setIcon(icon14);
        actionOrtho_Slices_Panel = new QAction(MainWindow);
        actionOrtho_Slices_Panel->setObjectName(QStringLiteral("actionOrtho_Slices_Panel"));
        actionOrtho_Slices_Panel->setCheckable(true);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/ortho_slices_window.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionOrtho_Slices_Panel->setIcon(icon15);
        actionTrackball_Mode = new QAction(MainWindow);
        actionTrackball_Mode->setObjectName(QStringLiteral("actionTrackball_Mode"));
        actionTrackball_Mode->setCheckable(true);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/trackball.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionTrackball_Mode->setIcon(icon16);
        actionObject_Manipulation = new QAction(MainWindow);
        actionObject_Manipulation->setObjectName(QStringLiteral("actionObject_Manipulation"));
        actionObject_Manipulation->setCheckable(true);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/slice_move.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionObject_Manipulation->setIcon(icon17);
        actionDensity_Window_Adjusting = new QAction(MainWindow);
        actionDensity_Window_Adjusting->setObjectName(QStringLiteral("actionDensity_Window_Adjusting"));
        actionDensity_Window_Adjusting->setCheckable(true);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/density_window2.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionDensity_Window_Adjusting->setIcon(icon18);
        actionCreate_Surface_Model = new QAction(MainWindow);
        actionCreate_Surface_Model->setObjectName(QStringLiteral("actionCreate_Surface_Model"));
        actionShow_Surface_Model = new QAction(MainWindow);
        actionShow_Surface_Model->setObjectName(QStringLiteral("actionShow_Surface_Model"));
        actionShow_Surface_Model->setCheckable(true);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/show_bone_model.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionShow_Surface_Model->setIcon(icon19);
        actionAxial_Slice = new QAction(MainWindow);
        actionAxial_Slice->setObjectName(QStringLiteral("actionAxial_Slice"));
        actionAxial_Slice->setCheckable(true);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/show_xy_slice3.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionAxial_Slice->setIcon(icon20);
        actionCoronal_Slice = new QAction(MainWindow);
        actionCoronal_Slice->setObjectName(QStringLiteral("actionCoronal_Slice"));
        actionCoronal_Slice->setCheckable(true);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/show_xz_slice3.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionCoronal_Slice->setIcon(icon21);
        actionSagittal_Slice = new QAction(MainWindow);
        actionSagittal_Slice->setObjectName(QStringLiteral("actionSagittal_Slice"));
        actionSagittal_Slice->setCheckable(true);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/show_yz_slice3.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSagittal_Slice->setIcon(icon22);
        actionSegmentation_Panel = new QAction(MainWindow);
        actionSegmentation_Panel->setObjectName(QStringLiteral("actionSegmentation_Panel"));
        actionSegmentation_Panel->setCheckable(true);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/segmentation_window.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionSegmentation_Panel->setIcon(icon23);
        actionWorkspaceTabs = new QAction(MainWindow);
        actionWorkspaceTabs->setObjectName(QStringLiteral("actionWorkspaceTabs"));
        actionWorkspaceTabs->setCheckable(true);
        actionWorkspace3DView = new QAction(MainWindow);
        actionWorkspace3DView->setObjectName(QStringLiteral("actionWorkspace3DView"));
        actionWorkspace3DView->setCheckable(true);
        actionMouse_Toolbar = new QAction(MainWindow);
        actionMouse_Toolbar->setObjectName(QStringLiteral("actionMouse_Toolbar"));
        actionMouse_Toolbar->setCheckable(true);
        actionMouse_Toolbar->setChecked(true);
        actionVisibility_Toolbar = new QAction(MainWindow);
        actionVisibility_Toolbar->setObjectName(QStringLiteral("actionVisibility_Toolbar"));
        actionVisibility_Toolbar->setCheckable(true);
        actionVisibility_Toolbar->setChecked(true);
        actionVolume_Rendering_Panel = new QAction(MainWindow);
        actionVolume_Rendering_Panel->setObjectName(QStringLiteral("actionVolume_Rendering_Panel"));
        actionVolume_Rendering_Panel->setCheckable(true);
        actionVolume_Rendering_Panel->setIcon(icon13);
        actionPreferences = new QAction(MainWindow);
        actionPreferences->setObjectName(QStringLiteral("actionPreferences"));
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/cog.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionPreferences->setIcon(icon24);
        actionShow_Data_Properties = new QAction(MainWindow);
        actionShow_Data_Properties->setObjectName(QStringLiteral("actionShow_Data_Properties"));
        QIcon icon25;
        icon25.addFile(QStringLiteral(":/icons/page.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionShow_Data_Properties->setIcon(icon25);
        actionLoad_STL_Model = new QAction(MainWindow);
        actionLoad_STL_Model->setObjectName(QStringLiteral("actionLoad_STL_Model"));
        actionLoad_STL_Model->setIcon(icon19);
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionAbout_Plugins = new QAction(MainWindow);
        actionAbout_Plugins->setObjectName(QStringLiteral("actionAbout_Plugins"));
        actionGaussian_Filter = new QAction(MainWindow);
        actionGaussian_Filter->setObjectName(QStringLiteral("actionGaussian_Filter"));
        actionMedian_Filter = new QAction(MainWindow);
        actionMedian_Filter->setObjectName(QStringLiteral("actionMedian_Filter"));
        action3D_Anisotropic_Filter = new QAction(MainWindow);
        action3D_Anisotropic_Filter->setObjectName(QStringLiteral("action3D_Anisotropic_Filter"));
        actionWorkspaceGrid = new QAction(MainWindow);
        actionWorkspaceGrid->setObjectName(QStringLiteral("actionWorkspaceGrid"));
        actionWorkspaceGrid->setCheckable(true);
        actionShow_Information_Widgets = new QAction(MainWindow);
        actionShow_Information_Widgets->setObjectName(QStringLiteral("actionShow_Information_Widgets"));
        actionShow_Information_Widgets->setCheckable(true);
        actionShow_Information_Widgets->setChecked(true);
        actionPanels_Toolbar = new QAction(MainWindow);
        actionPanels_Toolbar->setObjectName(QStringLiteral("actionPanels_Toolbar"));
        actionPanels_Toolbar->setCheckable(true);
        actionPanels_Toolbar->setChecked(true);
        actionShow_Help = new QAction(MainWindow);
        actionShow_Help->setObjectName(QStringLiteral("actionShow_Help"));
        actionScale_Scene = new QAction(MainWindow);
        actionScale_Scene->setObjectName(QStringLiteral("actionScale_Scene"));
        actionScale_Scene->setCheckable(true);
        QIcon icon26;
        icon26.addFile(QStringLiteral(":/icons/magnifier.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionScale_Scene->setIcon(icon26);
        actionLoad_User_Perspective = new QAction(MainWindow);
        actionLoad_User_Perspective->setObjectName(QStringLiteral("actionLoad_User_Perspective"));
        actionLoad_User_Perspective->setVisible(false);
        actionSave_User_Perspective = new QAction(MainWindow);
        actionSave_User_Perspective->setObjectName(QStringLiteral("actionSave_User_Perspective"));
        actionSave_User_Perspective->setVisible(false);
        actionLoad_Default_Perspective = new QAction(MainWindow);
        actionLoad_Default_Perspective->setObjectName(QStringLiteral("actionLoad_Default_Perspective"));
        actionSave_STL_Model = new QAction(MainWindow);
        actionSave_STL_Model->setObjectName(QStringLiteral("actionSave_STL_Model"));
        actionSend_Data = new QAction(MainWindow);
        actionSend_Data->setObjectName(QStringLiteral("actionSend_Data"));
        actionMeasure_Density_Value = new QAction(MainWindow);
        actionMeasure_Density_Value->setObjectName(QStringLiteral("actionMeasure_Density_Value"));
        actionMeasure_Density_Value->setCheckable(true);
        QIcon icon27;
        icon27.addFile(QStringLiteral(":/icons/measure_density.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMeasure_Density_Value->setIcon(icon27);
        actionMeasure_Distance = new QAction(MainWindow);
        actionMeasure_Distance->setObjectName(QStringLiteral("actionMeasure_Distance"));
        actionMeasure_Distance->setCheckable(true);
        QIcon icon28;
        icon28.addFile(QStringLiteral(":/icons/measure_distance.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionMeasure_Distance->setIcon(icon28);
        actionClear_Measurements = new QAction(MainWindow);
        actionClear_Measurements->setObjectName(QStringLiteral("actionClear_Measurements"));
        actionClear_Measurements->setIcon(icon3);
        actionVolume_Rendering = new QAction(MainWindow);
        actionVolume_Rendering->setObjectName(QStringLiteral("actionVolume_Rendering"));
        actionVolume_Rendering->setCheckable(true);
        QIcon icon29;
        icon29.addFile(QStringLiteral(":/icons/show_volume_rendering.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionVolume_Rendering->setIcon(icon29);
        actionSave_DICOM_Series = new QAction(MainWindow);
        actionSave_DICOM_Series->setObjectName(QStringLiteral("actionSave_DICOM_Series"));
        actionImport_DICOM_Data_from_ZIP_archive = new QAction(MainWindow);
        actionImport_DICOM_Data_from_ZIP_archive->setObjectName(QStringLiteral("actionImport_DICOM_Data_from_ZIP_archive"));
        QIcon icon30;
        icon30.addFile(QStringLiteral(":/icons/folder_open_up_menu.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionImport_DICOM_Data_from_ZIP_archive->setIcon(icon30);
        actionModels_List_Panel = new QAction(MainWindow);
        actionModels_List_Panel->setObjectName(QStringLiteral("actionModels_List_Panel"));
        actionModels_List_Panel->setCheckable(true);
        QIcon icon31;
        icon31.addFile(QStringLiteral(":/icons/resources/models.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionModels_List_Panel->setIcon(icon31);
        actionViewEqualize = new QAction(MainWindow);
        actionViewEqualize->setObjectName(QStringLiteral("actionViewEqualize"));
        actionViewEqualize->setCheckable(true);
        actionViewSharpen = new QAction(MainWindow);
        actionViewSharpen->setObjectName(QStringLiteral("actionViewSharpen"));
        actionViewSharpen->setCheckable(true);
        actionSave_Original_DICOM_Series = new QAction(MainWindow);
        actionSave_Original_DICOM_Series->setObjectName(QStringLiteral("actionSave_Original_DICOM_Series"));
        actionClose_Active_Panel = new QAction(MainWindow);
        actionClose_Active_Panel->setObjectName(QStringLiteral("actionClose_Active_Panel"));
        actionPrevious_Panel = new QAction(MainWindow);
        actionPrevious_Panel->setObjectName(QStringLiteral("actionPrevious_Panel"));
        actionNext_Panel = new QAction(MainWindow);
        actionNext_Panel->setObjectName(QStringLiteral("actionNext_Panel"));
        actionVolume_Filter = new QAction(MainWindow);
        actionVolume_Filter->setObjectName(QStringLiteral("actionVolume_Filter"));
        actionSharpening_Filter = new QAction(MainWindow);
        actionSharpening_Filter->setObjectName(QStringLiteral("actionSharpening_Filter"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 667, 21));
        menuArchive = new QMenu(menuBar);
        menuArchive->setObjectName(QStringLiteral("menuArchive"));
        menuRecent = new QMenu(menuArchive);
        menuRecent->setObjectName(QStringLiteral("menuRecent"));
        menuEdit = new QMenu(menuBar);
        menuEdit->setObjectName(QStringLiteral("menuEdit"));
        menuSwitch_Mouse_Mode = new QMenu(menuEdit);
        menuSwitch_Mouse_Mode->setObjectName(QStringLiteral("menuSwitch_Mouse_Mode"));
        menuView = new QMenu(menuBar);
        menuView->setObjectName(QStringLiteral("menuView"));
        menuViews = new QMenu(menuView);
        menuViews->setObjectName(QStringLiteral("menuViews"));
        menuToolbars = new QMenu(menuView);
        menuToolbars->setObjectName(QStringLiteral("menuToolbars"));
        menuControl_Panels = new QMenu(menuView);
        menuControl_Panels->setObjectName(QStringLiteral("menuControl_Panels"));
        menu3D_Scene = new QMenu(menuView);
        menu3D_Scene->setObjectName(QStringLiteral("menu3D_Scene"));
        menuWorkspaces = new QMenu(menuView);
        menuWorkspaces->setObjectName(QStringLiteral("menuWorkspaces"));
        menuModel = new QMenu(menuBar);
        menuModel->setObjectName(QStringLiteral("menuModel"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        menuFiltering = new QMenu(menuBar);
        menuFiltering->setObjectName(QStringLiteral("menuFiltering"));
        menuViewFilter = new QMenu(menuFiltering);
        menuViewFilter->setObjectName(QStringLiteral("menuViewFilter"));
        menuMeasurements = new QMenu(menuBar);
        menuMeasurements->setObjectName(QStringLiteral("menuMeasurements"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        panelsToolBar = new QToolBar(MainWindow);
        panelsToolBar->setObjectName(QStringLiteral("panelsToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, panelsToolBar);
        viewsToolBar = new QToolBar(MainWindow);
        viewsToolBar->setObjectName(QStringLiteral("viewsToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, viewsToolBar);
        mouseToolBar = new QToolBar(MainWindow);
        mouseToolBar->setObjectName(QStringLiteral("mouseToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mouseToolBar);
        visibilityToolBar = new QToolBar(MainWindow);
        visibilityToolBar->setObjectName(QStringLiteral("visibilityToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, visibilityToolBar);
        appToolBar = new QToolBar(MainWindow);
        appToolBar->setObjectName(QStringLiteral("appToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, appToolBar);

        menuBar->addAction(menuArchive->menuAction());
        menuBar->addAction(menuEdit->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuFiltering->menuAction());
        menuBar->addAction(menuModel->menuAction());
        menuBar->addAction(menuMeasurements->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuArchive->addAction(actionLoad_Patient_Dicom_data);
        menuArchive->addAction(actionImport_DICOM_Data_from_ZIP_archive);
        menuArchive->addAction(actionLoad_Patient_VLM_Data);
        menuArchive->addAction(actionLoad_STL_Model);
        menuArchive->addAction(menuRecent->menuAction());
        menuArchive->addSeparator();
        menuArchive->addAction(actionSave_Original_DICOM_Series);
        menuArchive->addAction(actionSave_DICOM_Series);
        menuArchive->addAction(actionSave_Volumetric_Data);
        menuArchive->addAction(actionSave_STL_Model);
        menuArchive->addSeparator();
        menuArchive->addAction(actionPrint);
        menuArchive->addAction(actionShow_Data_Properties);
        menuArchive->addAction(actionSend_Data);
        menuArchive->addSeparator();
        menuArchive->addAction(actionExit);
        menuEdit->addAction(actionUndo);
        menuEdit->addAction(actionRedo);
        menuEdit->addSeparator();
        menuEdit->addAction(menuSwitch_Mouse_Mode->menuAction());
        menuSwitch_Mouse_Mode->addAction(actionDensity_Window_Adjusting);
        menuSwitch_Mouse_Mode->addAction(actionTrackball_Mode);
        menuSwitch_Mouse_Mode->addAction(actionObject_Manipulation);
        menuSwitch_Mouse_Mode->addAction(actionScale_Scene);
        menuView->addAction(menuControl_Panels->menuAction());
        menuView->addAction(menuViews->menuAction());
        menuView->addAction(menuToolbars->menuAction());
        menuView->addAction(menu3D_Scene->menuAction());
        menuView->addAction(menuWorkspaces->menuAction());
        menuView->addAction(actionShow_Information_Widgets);
        menuView->addSeparator();
        menuView->addAction(actionLoad_User_Perspective);
        menuView->addAction(actionSave_User_Perspective);
        menuView->addAction(actionLoad_Default_Perspective);
        menuView->addSeparator();
        menuView->addAction(actionPreferences);
        menuViews->addAction(action3D_View);
        menuViews->addAction(actionAxial_View);
        menuViews->addAction(actionCoronal_View);
        menuViews->addAction(actionSagittal_View);
        menuViews->addAction(actionVolume_Rendering_View);
        menuToolbars->addAction(actionMain_Toolbar);
        menuToolbars->addAction(actionPanels_Toolbar);
        menuToolbars->addAction(actionViews_Toolbar);
        menuToolbars->addAction(actionMouse_Toolbar);
        menuToolbars->addAction(actionVisibility_Toolbar);
        menuControl_Panels->addAction(actionDensity_Window);
        menuControl_Panels->addAction(actionOrtho_Slices_Panel);
        menuControl_Panels->addAction(actionVolume_Rendering_Panel);
        menuControl_Panels->addAction(actionSegmentation_Panel);
        menuControl_Panels->addAction(actionModels_List_Panel);
        menuControl_Panels->addSeparator();
        menuControl_Panels->addAction(actionPrevious_Panel);
        menuControl_Panels->addAction(actionNext_Panel);
        menuControl_Panels->addAction(actionClose_Active_Panel);
        menu3D_Scene->addAction(actionVolume_Rendering);
        menu3D_Scene->addAction(actionAxial_Slice);
        menu3D_Scene->addAction(actionCoronal_Slice);
        menu3D_Scene->addAction(actionSagittal_Slice);
        menuWorkspaces->addAction(actionWorkspace3DView);
        menuWorkspaces->addAction(actionWorkspaceTabs);
        menuWorkspaces->addAction(actionWorkspaceGrid);
        menuModel->addAction(actionCreate_Surface_Model);
        menuModel->addAction(actionShow_Surface_Model);
        menuHelp->addAction(actionShow_Help);
        menuHelp->addSeparator();
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionAbout_Plugins);
        menuFiltering->addAction(actionGaussian_Filter);
        menuFiltering->addAction(actionMedian_Filter);
        menuFiltering->addAction(action3D_Anisotropic_Filter);
        menuFiltering->addAction(actionSharpening_Filter);
        menuFiltering->addAction(menuViewFilter->menuAction());
        menuViewFilter->addAction(actionViewSharpen);
        menuViewFilter->addAction(actionViewEqualize);
        menuMeasurements->addAction(actionMeasure_Density_Value);
        menuMeasurements->addAction(actionMeasure_Distance);
        menuMeasurements->addSeparator();
        menuMeasurements->addAction(actionClear_Measurements);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "3DimViewer", 0));
        actionLoad_Patient_Dicom_data->setText(QApplication::translate("MainWindow", "Import Patient DICOM Data...", 0));
#ifndef QT_NO_TOOLTIP
        actionLoad_Patient_Dicom_data->setToolTip(QApplication::translate("MainWindow", "Loads DICOM dataset from a given directory.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionLoad_Patient_Dicom_data->setStatusTip(QApplication::translate("MainWindow", "Loads DICOM dataset from a given directory.", 0));
#endif // QT_NO_STATUSTIP
        actionLoad_Patient_Dicom_data->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        actionLoad_Patient_VLM_Data->setText(QApplication::translate("MainWindow", "Load Patient VLM Data...", 0));
#ifndef QT_NO_TOOLTIP
        actionLoad_Patient_VLM_Data->setToolTip(QApplication::translate("MainWindow", "Loads density data from a specified VLM file.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionLoad_Patient_VLM_Data->setStatusTip(QApplication::translate("MainWindow", "Loads density data from a specified VLM file.", 0));
#endif // QT_NO_STATUSTIP
        actionLoad_Patient_VLM_Data->setShortcut(QApplication::translate("MainWindow", "Ctrl+V", 0));
        actionSave_Volumetric_Data->setText(QApplication::translate("MainWindow", "Save Volumetric VLM Data...", 0));
#ifndef QT_NO_TOOLTIP
        actionSave_Volumetric_Data->setToolTip(QApplication::translate("MainWindow", "Saves density data to a specified VLM file.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSave_Volumetric_Data->setStatusTip(QApplication::translate("MainWindow", "Saves density data to a specified VLM file.", 0));
#endif // QT_NO_STATUSTIP
        actionSave_Volumetric_Data->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
#ifndef QT_NO_TOOLTIP
        actionExit->setToolTip(QApplication::translate("MainWindow", "Terminates the program.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionExit->setStatusTip(QApplication::translate("MainWindow", "Terminates the program.", 0));
#endif // QT_NO_STATUSTIP
        actionExit->setShortcut(QApplication::translate("MainWindow", "Ctrl+X", 0));
        actionPrint->setText(QApplication::translate("MainWindow", "Print...", 0));
#ifndef QT_NO_STATUSTIP
        actionPrint->setStatusTip(QApplication::translate("MainWindow", "Print", 0));
#endif // QT_NO_STATUSTIP
        actionUndo->setText(QApplication::translate("MainWindow", "Undo", 0));
#ifndef QT_NO_TOOLTIP
        actionUndo->setToolTip(QApplication::translate("MainWindow", "Undoes the last command.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionUndo->setStatusTip(QApplication::translate("MainWindow", "Undoes the last command.", 0));
#endif // QT_NO_STATUSTIP
        actionUndo->setShortcut(QApplication::translate("MainWindow", "Ctrl+Z", 0));
        actionRedo->setText(QApplication::translate("MainWindow", "Redo", 0));
#ifndef QT_NO_TOOLTIP
        actionRedo->setToolTip(QApplication::translate("MainWindow", "Redoes the last command.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionRedo->setStatusTip(QApplication::translate("MainWindow", "Redoes the last command.", 0));
#endif // QT_NO_STATUSTIP
        actionRedo->setShortcut(QApplication::translate("MainWindow", "Ctrl+Y", 0));
        action3D_View->setText(QApplication::translate("MainWindow", "3D View", 0));
#ifndef QT_NO_STATUSTIP
        action3D_View->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the main 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionAxial_View->setText(QApplication::translate("MainWindow", "Axial View", 0));
#ifndef QT_NO_TOOLTIP
        actionAxial_View->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the axial (XY) plane view.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionAxial_View->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the axial (XY) plane view.", 0));
#endif // QT_NO_STATUSTIP
        actionCoronal_View->setText(QApplication::translate("MainWindow", "Coronal View", 0));
#ifndef QT_NO_TOOLTIP
        actionCoronal_View->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the coronal (XZ) plane view.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionCoronal_View->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the coronal (XZ) plane view.", 0));
#endif // QT_NO_STATUSTIP
        actionSagittal_View->setText(QApplication::translate("MainWindow", "Sagittal View", 0));
#ifndef QT_NO_TOOLTIP
        actionSagittal_View->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the sagittal (YZ) plane view.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSagittal_View->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the sagittal (YZ) plane view.", 0));
#endif // QT_NO_STATUSTIP
        actionPanoramic_View->setText(QApplication::translate("MainWindow", "Panoramic View", 0));
        actionNormal_Slice->setText(QApplication::translate("MainWindow", "Normal Slice", 0));
        actionVolume_Rendering_View->setText(QApplication::translate("MainWindow", "Volume Rendering View", 0));
#ifndef QT_NO_TOOLTIP
        actionVolume_Rendering_View->setToolTip(QApplication::translate("MainWindow", "Shows/Hides main volume rendering window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionVolume_Rendering_View->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides main volume rendering window.", 0));
#endif // QT_NO_STATUSTIP
        actionMain_Toolbar->setText(QApplication::translate("MainWindow", "Main Toolbar", 0));
#ifndef QT_NO_STATUSTIP
        actionMain_Toolbar->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides main toolbar.", 0));
#endif // QT_NO_STATUSTIP
        actionViews_Toolbar->setText(QApplication::translate("MainWindow", "Views Toolbar", 0));
#ifndef QT_NO_STATUSTIP
        actionViews_Toolbar->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides views toolbar.", 0));
#endif // QT_NO_STATUSTIP
        actionDensity_Window->setText(QApplication::translate("MainWindow", "Density Window Panel", 0));
#ifndef QT_NO_TOOLTIP
        actionDensity_Window->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the density window adjusting panel.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionDensity_Window->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the density window adjusting panel.", 0));
#endif // QT_NO_STATUSTIP
        actionDensity_Window->setShortcut(QApplication::translate("MainWindow", "Ctrl+1", 0));
        actionOrtho_Slices_Panel->setText(QApplication::translate("MainWindow", "Ortho Slices Panel", 0));
#ifndef QT_NO_TOOLTIP
        actionOrtho_Slices_Panel->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the ortho slices panel.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionOrtho_Slices_Panel->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the ortho slices panel.", 0));
#endif // QT_NO_STATUSTIP
        actionOrtho_Slices_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+2", 0));
        actionTrackball_Mode->setText(QApplication::translate("MainWindow", "Trackball Mode", 0));
#ifndef QT_NO_TOOLTIP
        actionTrackball_Mode->setToolTip(QApplication::translate("MainWindow", "Switches mouse to scene manipulation mode.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionTrackball_Mode->setStatusTip(QApplication::translate("MainWindow", "Switches mouse to scene manipulation mode.", 0));
#endif // QT_NO_STATUSTIP
        actionObject_Manipulation->setText(QApplication::translate("MainWindow", "Object Manipulation", 0));
#ifndef QT_NO_TOOLTIP
        actionObject_Manipulation->setToolTip(QApplication::translate("MainWindow", "Switches mouse to the object manipulation mode so that you can change position of cross-sectional slices, implants, etc.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionObject_Manipulation->setStatusTip(QApplication::translate("MainWindow", "Switches mouse to the object manipulation mode so that you can change position of cross-sectional slices, implants, etc.", 0));
#endif // QT_NO_STATUSTIP
        actionDensity_Window_Adjusting->setText(QApplication::translate("MainWindow", "Density Window Adjusting", 0));
#ifndef QT_NO_TOOLTIP
        actionDensity_Window_Adjusting->setToolTip(QApplication::translate("MainWindow", "Switches mouse to the density window adjusting mode. Press the left mouse button and modify the density window moving the mouse cursor.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionDensity_Window_Adjusting->setStatusTip(QApplication::translate("MainWindow", "Switches mouse to the density window adjusting mode. Press the left mouse button and modify the density window moving the mouse cursor.", 0));
#endif // QT_NO_STATUSTIP
        actionCreate_Surface_Model->setText(QApplication::translate("MainWindow", "Create Surface Model", 0));
#ifndef QT_NO_TOOLTIP
        actionCreate_Surface_Model->setToolTip(QApplication::translate("MainWindow", "Creates polygonal surface model regarding the current tissue segmentation. For options see the tissue segmentation panel...", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionCreate_Surface_Model->setStatusTip(QApplication::translate("MainWindow", "Creates polygonal surface model regarding the current tissue segmentation. For options see the tissue segmentation panel...", 0));
#endif // QT_NO_STATUSTIP
        actionShow_Surface_Model->setText(QApplication::translate("MainWindow", "Show Surface Model", 0));
#ifndef QT_NO_TOOLTIP
        actionShow_Surface_Model->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the surface model in the 3D view.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionShow_Surface_Model->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the surface model in the 3D view.", 0));
#endif // QT_NO_STATUSTIP
        actionAxial_Slice->setText(QApplication::translate("MainWindow", "Axial Slice", 0));
#ifndef QT_NO_TOOLTIP
        actionAxial_Slice->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the axial (XY) slice in the 3D scene.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionAxial_Slice->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the axial (XY) slice in the 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionCoronal_Slice->setText(QApplication::translate("MainWindow", "Coronal Slice", 0));
#ifndef QT_NO_TOOLTIP
        actionCoronal_Slice->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the coronal (XZ) slice in the 3D scene.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionCoronal_Slice->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the coronal (XZ) slice in the 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionSagittal_Slice->setText(QApplication::translate("MainWindow", "Sagittal Slice", 0));
#ifndef QT_NO_TOOLTIP
        actionSagittal_Slice->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the sagittal (YZ) slice in the 3D scene.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSagittal_Slice->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the sagittal (YZ) slice in the 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionSegmentation_Panel->setText(QApplication::translate("MainWindow", "Segmentation Panel", 0));
#ifndef QT_NO_TOOLTIP
        actionSegmentation_Panel->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the tissue segmentation panel.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSegmentation_Panel->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the tissue segmentation panel.", 0));
#endif // QT_NO_STATUSTIP
        actionSegmentation_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+4", 0));
        actionWorkspaceTabs->setText(QApplication::translate("MainWindow", "Tabs", 0));
#ifndef QT_NO_TOOLTIP
        actionWorkspaceTabs->setToolTip(QApplication::translate("MainWindow", "Switches workspace to tabbed layout.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionWorkspaceTabs->setStatusTip(QApplication::translate("MainWindow", "Switches workspace to tabbed layout.", 0));
#endif // QT_NO_STATUSTIP
        actionWorkspace3DView->setText(QApplication::translate("MainWindow", "3D View", 0));
#ifndef QT_NO_TOOLTIP
        actionWorkspace3DView->setToolTip(QApplication::translate("MainWindow", "Switches workspace to a layout with the 3D scene as the main window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionWorkspace3DView->setStatusTip(QApplication::translate("MainWindow", "Switches workspace to a layout with the 3D scene as the main window.", 0));
#endif // QT_NO_STATUSTIP
        actionMouse_Toolbar->setText(QApplication::translate("MainWindow", "Mouse Toolbar", 0));
#ifndef QT_NO_STATUSTIP
        actionMouse_Toolbar->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides mouse mode toolbar.", 0));
#endif // QT_NO_STATUSTIP
        actionVisibility_Toolbar->setText(QApplication::translate("MainWindow", "Visibility Toolbar", 0));
#ifndef QT_NO_STATUSTIP
        actionVisibility_Toolbar->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides slices visibility toolbar.", 0));
#endif // QT_NO_STATUSTIP
        actionVolume_Rendering_Panel->setText(QApplication::translate("MainWindow", "Volume Rendering Panel", 0));
#ifndef QT_NO_TOOLTIP
        actionVolume_Rendering_Panel->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the volume rendering configuration panel.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionVolume_Rendering_Panel->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the volume rendering configuration panel.", 0));
#endif // QT_NO_STATUSTIP
        actionVolume_Rendering_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+3", 0));
        actionPreferences->setText(QApplication::translate("MainWindow", "Preferences...", 0));
#ifndef QT_NO_TOOLTIP
        actionPreferences->setToolTip(QApplication::translate("MainWindow", "Shows application's options.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionPreferences->setStatusTip(QApplication::translate("MainWindow", "Shows application's options.", 0));
#endif // QT_NO_STATUSTIP
        actionShow_Data_Properties->setText(QApplication::translate("MainWindow", "Show Data Properties...", 0));
#ifndef QT_NO_TOOLTIP
        actionShow_Data_Properties->setToolTip(QApplication::translate("MainWindow", "Shows information about the loaded dataset.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionShow_Data_Properties->setStatusTip(QApplication::translate("MainWindow", "Shows information about the loaded dataset.", 0));
#endif // QT_NO_STATUSTIP
        actionLoad_STL_Model->setText(QApplication::translate("MainWindow", "Load STL Model...", 0));
#ifndef QT_NO_TOOLTIP
        actionLoad_STL_Model->setToolTip(QApplication::translate("MainWindow", "Loads surface model from a specified STL file.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionLoad_STL_Model->setStatusTip(QApplication::translate("MainWindow", "Loads surface model from a specified STL file.", 0));
#endif // QT_NO_STATUSTIP
        actionAbout->setText(QApplication::translate("MainWindow", "About...", 0));
#ifndef QT_NO_TOOLTIP
        actionAbout->setToolTip(QApplication::translate("MainWindow", "Shows basic information about the program.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionAbout->setStatusTip(QApplication::translate("MainWindow", "Shows basic information about the program.", 0));
#endif // QT_NO_STATUSTIP
        actionAbout_Plugins->setText(QApplication::translate("MainWindow", "About Plugins...", 0));
#ifndef QT_NO_TOOLTIP
        actionAbout_Plugins->setToolTip(QApplication::translate("MainWindow", "Shows basic information about loaded plugins.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionAbout_Plugins->setStatusTip(QApplication::translate("MainWindow", "Shows basic information about loaded plugins.", 0));
#endif // QT_NO_STATUSTIP
        actionGaussian_Filter->setText(QApplication::translate("MainWindow", "Gaussian Filter...", 0));
#ifndef QT_NO_TOOLTIP
        actionGaussian_Filter->setToolTip(QApplication::translate("MainWindow", "Applies 3D Gaussian filter on the volumetric data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionGaussian_Filter->setStatusTip(QApplication::translate("MainWindow", "Applies 3D Gaussian filter on the volumetric data.", 0));
#endif // QT_NO_STATUSTIP
        actionMedian_Filter->setText(QApplication::translate("MainWindow", "Median Filter...", 0));
#ifndef QT_NO_TOOLTIP
        actionMedian_Filter->setToolTip(QApplication::translate("MainWindow", "Applies median filter on the volumetric data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionMedian_Filter->setStatusTip(QApplication::translate("MainWindow", "Applies median filter on the volumetric data.", 0));
#endif // QT_NO_STATUSTIP
        action3D_Anisotropic_Filter->setText(QApplication::translate("MainWindow", "3D Anisotropic Filter...", 0));
#ifndef QT_NO_TOOLTIP
        action3D_Anisotropic_Filter->setToolTip(QApplication::translate("MainWindow", "Applies 3D anisotropic filter on the volumetric data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        action3D_Anisotropic_Filter->setStatusTip(QApplication::translate("MainWindow", "Applies 3D anisotropic filter on the volumetric data.", 0));
#endif // QT_NO_STATUSTIP
        actionWorkspaceGrid->setText(QApplication::translate("MainWindow", "Grid", 0));
#ifndef QT_NO_TOOLTIP
        actionWorkspaceGrid->setToolTip(QApplication::translate("MainWindow", "Switches workspace to a grid layout with cross-sectional slices and 3D scene.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionWorkspaceGrid->setStatusTip(QApplication::translate("MainWindow", "Switches workspace to a grid layout with cross-sectional slices and 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionShow_Information_Widgets->setText(QApplication::translate("MainWindow", "Show Information Widgets", 0));
#ifndef QT_NO_TOOLTIP
        actionShow_Information_Widgets->setToolTip(QApplication::translate("MainWindow", "Shows/Hides special information widgets in all views.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionShow_Information_Widgets->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides special information widgets in all views.", 0));
#endif // QT_NO_STATUSTIP
        actionPanels_Toolbar->setText(QApplication::translate("MainWindow", "Panels Toolbar", 0));
        actionShow_Help->setText(QApplication::translate("MainWindow", "Show Help", 0));
#ifndef QT_NO_TOOLTIP
        actionShow_Help->setToolTip(QApplication::translate("MainWindow", "Shows Help.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionShow_Help->setStatusTip(QApplication::translate("MainWindow", "Shows Help.", 0));
#endif // QT_NO_STATUSTIP
        actionShow_Help->setShortcut(QApplication::translate("MainWindow", "F1", 0));
        actionScale_Scene->setText(QApplication::translate("MainWindow", "Scale Scene", 0));
#ifndef QT_NO_TOOLTIP
        actionScale_Scene->setToolTip(QApplication::translate("MainWindow", "Switches mouse to scene scale so that you can scale scene to fit into window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionScale_Scene->setStatusTip(QApplication::translate("MainWindow", "Switches mouse to scene scale so that you can scale scene to fit into window.", 0));
#endif // QT_NO_STATUSTIP
        actionLoad_User_Perspective->setText(QApplication::translate("MainWindow", "Load User Perspective", 0));
#ifndef QT_NO_STATUSTIP
        actionLoad_User_Perspective->setStatusTip(QApplication::translate("MainWindow", "Loads the user defined windows layout.", 0));
#endif // QT_NO_STATUSTIP
        actionSave_User_Perspective->setText(QApplication::translate("MainWindow", "Save User Perspective", 0));
#ifndef QT_NO_STATUSTIP
        actionSave_User_Perspective->setStatusTip(QApplication::translate("MainWindow", "Saves the current windows layout.", 0));
#endif // QT_NO_STATUSTIP
        actionLoad_Default_Perspective->setText(QApplication::translate("MainWindow", "Load Default Perspective", 0));
#ifndef QT_NO_TOOLTIP
        actionLoad_Default_Perspective->setToolTip(QApplication::translate("MainWindow", "Loads the default windows layout.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionLoad_Default_Perspective->setStatusTip(QApplication::translate("MainWindow", "Loads the default windows layout.", 0));
#endif // QT_NO_STATUSTIP
        actionSave_STL_Model->setText(QApplication::translate("MainWindow", "Save STL Model...", 0));
#ifndef QT_NO_TOOLTIP
        actionSave_STL_Model->setToolTip(QApplication::translate("MainWindow", "Saves generated model to a specified STL file.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSave_STL_Model->setStatusTip(QApplication::translate("MainWindow", "Saves generated model to a specified STL file.", 0));
#endif // QT_NO_STATUSTIP
        actionSend_Data->setText(QApplication::translate("MainWindow", "Send Data...  (DataExpress Service)", 0));
#ifndef QT_NO_TOOLTIP
        actionSend_Data->setToolTip(QApplication::translate("MainWindow", "Sends volumetric data via DataExpress Service.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSend_Data->setStatusTip(QApplication::translate("MainWindow", "Sends volumetric data via DataExpress Service.", 0));
#endif // QT_NO_STATUSTIP
        actionMeasure_Density_Value->setText(QApplication::translate("MainWindow", "Measure Density Value [Hu]", 0));
#ifndef QT_NO_TOOLTIP
        actionMeasure_Density_Value->setToolTip(QApplication::translate("MainWindow", "To measure local density, specify a point using the mouse cursor and click the left button.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionMeasure_Density_Value->setStatusTip(QApplication::translate("MainWindow", "To measure local density, specify a point using the mouse cursor and click the left button.", 0));
#endif // QT_NO_STATUSTIP
        actionMeasure_Distance->setText(QApplication::translate("MainWindow", "Measure Distance [mm]", 0));
#ifndef QT_NO_TOOLTIP
        actionMeasure_Distance->setToolTip(QApplication::translate("MainWindow", "Measure distance by clicking the left mouse button and dragging.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionMeasure_Distance->setStatusTip(QApplication::translate("MainWindow", "Measure distance by clicking the left mouse button and dragging.", 0));
#endif // QT_NO_STATUSTIP
        actionClear_Measurements->setText(QApplication::translate("MainWindow", "Clear Measurements", 0));
#ifndef QT_NO_TOOLTIP
        actionClear_Measurements->setToolTip(QApplication::translate("MainWindow", "Clears all visible measurement results.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionClear_Measurements->setStatusTip(QApplication::translate("MainWindow", "Clears all visible measurement results.", 0));
#endif // QT_NO_STATUSTIP
        actionVolume_Rendering->setText(QApplication::translate("MainWindow", "Volume Rendering", 0));
#ifndef QT_NO_TOOLTIP
        actionVolume_Rendering->setToolTip(QApplication::translate("MainWindow", "Shows/hides volume rendering in the 3D scene.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionVolume_Rendering->setStatusTip(QApplication::translate("MainWindow", "Shows/hides volume rendering in the 3D scene.", 0));
#endif // QT_NO_STATUSTIP
        actionSave_DICOM_Series->setText(QApplication::translate("MainWindow", "Save DICOM Series...", 0));
#ifndef QT_NO_TOOLTIP
        actionSave_DICOM_Series->setToolTip(QApplication::translate("MainWindow", "Saves DICOM Series to a given directory.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSave_DICOM_Series->setStatusTip(QApplication::translate("MainWindow", "Saves DICOM Series to a given directory.", 0));
#endif // QT_NO_STATUSTIP
        actionImport_DICOM_Data_from_ZIP_archive->setText(QApplication::translate("MainWindow", "Import DICOM Data from ZIP...", 0));
        actionModels_List_Panel->setText(QApplication::translate("MainWindow", "Models List", 0));
#ifndef QT_NO_TOOLTIP
        actionModels_List_Panel->setToolTip(QApplication::translate("MainWindow", "Shows/Hides the models list.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionModels_List_Panel->setStatusTip(QApplication::translate("MainWindow", "Shows/Hides the models list.", 0));
#endif // QT_NO_STATUSTIP
        actionModels_List_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+5", 0));
        actionViewEqualize->setText(QApplication::translate("MainWindow", "Equalize", 0));
        actionViewSharpen->setText(QApplication::translate("MainWindow", "Sharpen", 0));
        actionSave_Original_DICOM_Series->setText(QApplication::translate("MainWindow", "Save Original DICOM Series...", 0));
#ifndef QT_NO_TOOLTIP
        actionSave_Original_DICOM_Series->setToolTip(QApplication::translate("MainWindow", "Saves original DICOM Series to a given directory.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSave_Original_DICOM_Series->setStatusTip(QApplication::translate("MainWindow", "Saves original DICOM Series to a given directory.", 0));
#endif // QT_NO_STATUSTIP
        actionClose_Active_Panel->setText(QApplication::translate("MainWindow", "Close Active Panel", 0));
        actionClose_Active_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+F4", 0));
        actionPrevious_Panel->setText(QApplication::translate("MainWindow", "Previous Panel", 0));
        actionPrevious_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+Tab", 0));
        actionNext_Panel->setText(QApplication::translate("MainWindow", "Next Panel", 0));
        actionNext_Panel->setShortcut(QApplication::translate("MainWindow", "Ctrl+Tab", 0));
        actionVolume_Filter->setText(QApplication::translate("MainWindow", "Volume Filter...", 0));
        actionSharpening_Filter->setText(QApplication::translate("MainWindow", "Sharpening Filter...", 0));
#ifndef QT_NO_TOOLTIP
        actionSharpening_Filter->setToolTip(QApplication::translate("MainWindow", "Applies sharpening filter on the volumetric data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        actionSharpening_Filter->setStatusTip(QApplication::translate("MainWindow", "Applies sharpening filter on the volumetric data.", 0));
#endif // QT_NO_STATUSTIP
        menuArchive->setTitle(QApplication::translate("MainWindow", "&File", 0));
        menuRecent->setTitle(QApplication::translate("MainWindow", "Recent", 0));
        menuEdit->setTitle(QApplication::translate("MainWindow", "&Edit", 0));
        menuSwitch_Mouse_Mode->setTitle(QApplication::translate("MainWindow", "Switch Mouse Mode", 0));
        menuView->setTitle(QApplication::translate("MainWindow", "&View", 0));
        menuViews->setTitle(QApplication::translate("MainWindow", "Views", 0));
        menuToolbars->setTitle(QApplication::translate("MainWindow", "Toolbars", 0));
        menuControl_Panels->setTitle(QApplication::translate("MainWindow", "Control Panels", 0));
        menu3D_Scene->setTitle(QApplication::translate("MainWindow", "3D Scene", 0));
        menuWorkspaces->setTitle(QApplication::translate("MainWindow", "Layout", 0));
        menuModel->setTitle(QApplication::translate("MainWindow", "&Model", 0));
        menuHelp->setTitle(QApplication::translate("MainWindow", "&Help", 0));
        menuFiltering->setTitle(QApplication::translate("MainWindow", "Fil&tering", 0));
        menuViewFilter->setTitle(QApplication::translate("MainWindow", "View", 0));
        menuMeasurements->setTitle(QApplication::translate("MainWindow", "Me&asurements", 0));
        mainToolBar->setWindowTitle(QApplication::translate("MainWindow", "Main Toolbar", 0));
        panelsToolBar->setWindowTitle(QApplication::translate("MainWindow", "Panels Toolbar", 0));
        viewsToolBar->setWindowTitle(QApplication::translate("MainWindow", "Views Toolbar", 0));
        mouseToolBar->setWindowTitle(QApplication::translate("MainWindow", "Mouse Toolbar", 0));
        visibilityToolBar->setWindowTitle(QApplication::translate("MainWindow", "Visibility Toolbar", 0));
        appToolBar->setWindowTitle(QApplication::translate("MainWindow", "Application Toolbar", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
