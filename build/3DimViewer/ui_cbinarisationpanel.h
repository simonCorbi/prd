/********************************************************************************
** Form generated from reading UI file 'cbinarisationpanel.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CBINARISATIONPANEL_H
#define UI_CBINARISATIONPANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CBinarisationPanel
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *seuilLabel;
    QPushButton *binariserButton;
    QLabel *debugLabel;
    QSpinBox *spinBoxSeuil;
    QSlider *seuilSlider;

    void setupUi(QWidget *CBinarisationPanel)
    {
        if (CBinarisationPanel->objectName().isEmpty())
            CBinarisationPanel->setObjectName(QStringLiteral("CBinarisationPanel"));
        CBinarisationPanel->resize(221, 169);
        CBinarisationPanel->setMinimumSize(QSize(221, 169));
        verticalLayout = new QVBoxLayout(CBinarisationPanel);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        seuilLabel = new QLabel(CBinarisationPanel);
        seuilLabel->setObjectName(QStringLiteral("seuilLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, seuilLabel);

        binariserButton = new QPushButton(CBinarisationPanel);
        binariserButton->setObjectName(QStringLiteral("binariserButton"));

        formLayout->setWidget(4, QFormLayout::LabelRole, binariserButton);

        debugLabel = new QLabel(CBinarisationPanel);
        debugLabel->setObjectName(QStringLiteral("debugLabel"));

        formLayout->setWidget(5, QFormLayout::LabelRole, debugLabel);

        spinBoxSeuil = new QSpinBox(CBinarisationPanel);
        spinBoxSeuil->setObjectName(QStringLiteral("spinBoxSeuil"));
        spinBoxSeuil->setMinimum(-1500);
        spinBoxSeuil->setMaximum(7000);

        formLayout->setWidget(2, QFormLayout::FieldRole, spinBoxSeuil);

        seuilSlider = new QSlider(CBinarisationPanel);
        seuilSlider->setObjectName(QStringLiteral("seuilSlider"));
        seuilSlider->setMinimum(-1500);
        seuilSlider->setMaximum(7000);
        seuilSlider->setPageStep(100);
        seuilSlider->setSliderPosition(0);
        seuilSlider->setOrientation(Qt::Horizontal);

        formLayout->setWidget(2, QFormLayout::LabelRole, seuilSlider);


        verticalLayout->addLayout(formLayout);


        retranslateUi(CBinarisationPanel);

        QMetaObject::connectSlotsByName(CBinarisationPanel);
    } // setupUi

    void retranslateUi(QWidget *CBinarisationPanel)
    {
        CBinarisationPanel->setWindowTitle(QApplication::translate("CBinarisationPanel", "Binarisation Panel", 0));
        seuilLabel->setText(QApplication::translate("CBinarisationPanel", "Seuil :", 0));
        binariserButton->setText(QApplication::translate("CBinarisationPanel", "Binariser", 0));
        debugLabel->setText(QApplication::translate("CBinarisationPanel", "Info", 0));
    } // retranslateUi

};

namespace Ui {
    class CBinarisationPanel: public Ui_CBinarisationPanel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CBINARISATIONPANEL_H
