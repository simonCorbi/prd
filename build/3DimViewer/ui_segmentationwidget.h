/********************************************************************************
** Form generated from reading UI file 'segmentationwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEGMENTATIONWIDGET_H
#define UI_SEGMENTATIONWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SegmentationWidget
{
public:
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QSlider *lowThresholdSlider;
    QSlider *highThresholdSlider;
    QSpinBox *highSpinBox;
    QSpinBox *lowSpinBox;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *checkBoxApplyThresholds;
    QPushButton *buttonColor;
    QPushButton *buttonPickLo;
    QPushButton *buttonPickHi;
    QPushButton *pushButtonSetToActiveRegion;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButtonCreateSurfaceModel;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *SegmentationWidget)
    {
        if (SegmentationWidget->objectName().isEmpty())
            SegmentationWidget->setObjectName(QStringLiteral("SegmentationWidget"));
        SegmentationWidget->resize(251, 233);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SegmentationWidget->sizePolicy().hasHeightForWidth());
        SegmentationWidget->setSizePolicy(sizePolicy);
        SegmentationWidget->setMinimumSize(QSize(251, 233));
        verticalLayout = new QVBoxLayout(SegmentationWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(SegmentationWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 251, 233));
        verticalLayout_2 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setFlat(false);
        groupBox->setCheckable(false);
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        lowThresholdSlider = new QSlider(groupBox);
        lowThresholdSlider->setObjectName(QStringLiteral("lowThresholdSlider"));
        lowThresholdSlider->setMinimum(-1500);
        lowThresholdSlider->setMaximum(7000);
        lowThresholdSlider->setPageStep(100);
        lowThresholdSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(lowThresholdSlider, 0, 0, 1, 1);

        highThresholdSlider = new QSlider(groupBox);
        highThresholdSlider->setObjectName(QStringLiteral("highThresholdSlider"));
        highThresholdSlider->setMinimum(-1500);
        highThresholdSlider->setMaximum(7000);
        highThresholdSlider->setPageStep(100);
        highThresholdSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(highThresholdSlider, 1, 0, 1, 1);

        highSpinBox = new QSpinBox(groupBox);
        highSpinBox->setObjectName(QStringLiteral("highSpinBox"));
        highSpinBox->setMinimumSize(QSize(60, 0));
        highSpinBox->setMinimum(-1500);
        highSpinBox->setMaximum(7000);

        gridLayout->addWidget(highSpinBox, 1, 1, 1, 1);

        lowSpinBox = new QSpinBox(groupBox);
        lowSpinBox->setObjectName(QStringLiteral("lowSpinBox"));
        lowSpinBox->setMinimumSize(QSize(60, 0));
        lowSpinBox->setMinimum(-1500);
        lowSpinBox->setMaximum(7000);

        gridLayout->addWidget(lowSpinBox, 0, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        checkBoxApplyThresholds = new QCheckBox(groupBox);
        checkBoxApplyThresholds->setObjectName(QStringLiteral("checkBoxApplyThresholds"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(checkBoxApplyThresholds->sizePolicy().hasHeightForWidth());
        checkBoxApplyThresholds->setSizePolicy(sizePolicy2);

        horizontalLayout_3->addWidget(checkBoxApplyThresholds);

        buttonColor = new QPushButton(groupBox);
        buttonColor->setObjectName(QStringLiteral("buttonColor"));
        buttonColor->setFlat(false);

        horizontalLayout_3->addWidget(buttonColor);


        gridLayout->addLayout(horizontalLayout_3, 2, 0, 1, 3);

        buttonPickLo = new QPushButton(groupBox);
        buttonPickLo->setObjectName(QStringLiteral("buttonPickLo"));
        buttonPickLo->setMinimumSize(QSize(16, 0));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/resources/dropper.png"), QSize(), QIcon::Normal, QIcon::Off);
        buttonPickLo->setIcon(icon);
        buttonPickLo->setCheckable(false);
        buttonPickLo->setChecked(false);

        gridLayout->addWidget(buttonPickLo, 0, 2, 1, 1);

        buttonPickHi = new QPushButton(groupBox);
        buttonPickHi->setObjectName(QStringLiteral("buttonPickHi"));
        buttonPickHi->setMinimumSize(QSize(16, 0));
        buttonPickHi->setIcon(icon);
        buttonPickHi->setCheckable(false);
        buttonPickHi->setChecked(false);

        gridLayout->addWidget(buttonPickHi, 1, 2, 1, 1);

        pushButtonSetToActiveRegion = new QPushButton(groupBox);
        pushButtonSetToActiveRegion->setObjectName(QStringLiteral("pushButtonSetToActiveRegion"));

        gridLayout->addWidget(pushButtonSetToActiveRegion, 3, 0, 1, 3);


        verticalLayout_2->addWidget(groupBox);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        pushButtonCreateSurfaceModel = new QPushButton(groupBox_2);
        pushButtonCreateSurfaceModel->setObjectName(QStringLiteral("pushButtonCreateSurfaceModel"));

        verticalLayout_3->addWidget(pushButtonCreateSurfaceModel);


        verticalLayout_2->addWidget(groupBox_2);

        verticalSpacer = new QSpacerItem(20, 12, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout->addWidget(scrollArea);


        retranslateUi(SegmentationWidget);

        QMetaObject::connectSlotsByName(SegmentationWidget);
    } // setupUi

    void retranslateUi(QWidget *SegmentationWidget)
    {
        SegmentationWidget->setWindowTitle(QApplication::translate("SegmentationWidget", "Tissue Segmentation", 0));
        groupBox->setTitle(QApplication::translate("SegmentationWidget", "Thresholding", 0));
#ifndef QT_NO_TOOLTIP
        lowThresholdSlider->setToolTip(QApplication::translate("SegmentationWidget", "Adjusts lower threshold.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        lowThresholdSlider->setStatusTip(QApplication::translate("SegmentationWidget", "Adjusts lower threshold.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        highThresholdSlider->setToolTip(QApplication::translate("SegmentationWidget", "Adjusts higher threshold.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        highThresholdSlider->setStatusTip(QApplication::translate("SegmentationWidget", "Adjusts higher threshold.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        highSpinBox->setToolTip(QApplication::translate("SegmentationWidget", "Adjusts higher threshold.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        highSpinBox->setStatusTip(QApplication::translate("SegmentationWidget", "Adjusts higher threshold.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        lowSpinBox->setToolTip(QApplication::translate("SegmentationWidget", "Adjusts lower threshold.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        lowSpinBox->setStatusTip(QApplication::translate("SegmentationWidget", "Adjusts lower threshold.", 0));
#endif // QT_NO_STATUSTIP
#ifndef QT_NO_TOOLTIP
        checkBoxApplyThresholds->setToolTip(QApplication::translate("SegmentationWidget", "Applies coloring of data according to thresholds.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        checkBoxApplyThresholds->setStatusTip(QApplication::translate("SegmentationWidget", "Applies coloring of data according to thresholds.", 0));
#endif // QT_NO_STATUSTIP
        checkBoxApplyThresholds->setText(QApplication::translate("SegmentationWidget", "Apply Thresholds", 0));
#ifndef QT_NO_TOOLTIP
        buttonColor->setToolTip(QApplication::translate("SegmentationWidget", "Sets color of thresholded data.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        buttonColor->setStatusTip(QApplication::translate("SegmentationWidget", "Sets color of thresholded data.", 0));
#endif // QT_NO_STATUSTIP
        buttonColor->setText(QString());
#ifndef QT_NO_TOOLTIP
        buttonPickLo->setToolTip(QApplication::translate("SegmentationWidget", "Picks lower threshold from data by clicking with left mose button.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        buttonPickLo->setStatusTip(QApplication::translate("SegmentationWidget", "Picks lower threshold from data by clicking with left mose button.", 0));
#endif // QT_NO_STATUSTIP
        buttonPickLo->setText(QString());
#ifndef QT_NO_TOOLTIP
        buttonPickHi->setToolTip(QApplication::translate("SegmentationWidget", "Picks higher threshold from data by clicking with left mose button.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        buttonPickHi->setStatusTip(QApplication::translate("SegmentationWidget", "Picks higher threshold from data by clicking with left mose button.", 0));
#endif // QT_NO_STATUSTIP
        buttonPickHi->setText(QString());
        pushButtonSetToActiveRegion->setText(QApplication::translate("SegmentationWidget", "Set to Active Region", 0));
        groupBox_2->setTitle(QApplication::translate("SegmentationWidget", "Model Creation", 0));
        pushButtonCreateSurfaceModel->setText(QApplication::translate("SegmentationWidget", "Create Surface Model", 0));
    } // retranslateUi

};

namespace Ui {
    class SegmentationWidget: public Ui_SegmentationWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEGMENTATIONWIDGET_H
