/********************************************************************************
** Form generated from reading UI file 'volumerenderingwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VOLUMERENDERINGWIDGET_H
#define UI_VOLUMERENDERINGWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <controls/CClickableLabel.h>

QT_BEGIN_NAMESPACE

class Ui_VolumeRenderingWidget
{
public:
    QVBoxLayout *verticalLayout_6;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QRadioButton *radioButtonMIP;
    QRadioButton *radioButtonShaded;
    QRadioButton *radioButtonXRay;
    QRadioButton *radioButtonSurface;
    QRadioButton *radioButtonCustom;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QComboBox *comboBoxColoring;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_2;
    QSlider *sliderRenderingQuality;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_5;
    QFormLayout *formLayout;
    CClickableLabel *labelWindShift;
    QSlider *sliderWindShift;
    CClickableLabel *labelWindWidth;
    QSlider *sliderWindWidth;
    CClickableLabel *labelBrightness;
    QSlider *sliderBrightness;
    CClickableLabel *labelContrast;
    QSlider *sliderContrast;
    CClickableLabel *labelSurfTolerance;
    QSlider *sliderSurfTolerance;
    CClickableLabel *labelSurfSharpness;
    QSlider *sliderSurfSharpness;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout;
    QSlider *sliderCuttingPlane;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *VolumeRenderingWidget)
    {
        if (VolumeRenderingWidget->objectName().isEmpty())
            VolumeRenderingWidget->setObjectName(QStringLiteral("VolumeRenderingWidget"));
        VolumeRenderingWidget->resize(251, 487);
        VolumeRenderingWidget->setMinimumSize(QSize(251, 300));
        verticalLayout_6 = new QVBoxLayout(VolumeRenderingWidget);
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        scrollArea = new QScrollArea(VolumeRenderingWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setFrameShadow(QFrame::Plain);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 251, 487));
        verticalLayout_7 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        groupBox = new QGroupBox(scrollAreaWidgetContents);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        radioButtonMIP = new QRadioButton(groupBox);
        radioButtonMIP->setObjectName(QStringLiteral("radioButtonMIP"));

        gridLayout->addWidget(radioButtonMIP, 0, 0, 1, 1);

        radioButtonShaded = new QRadioButton(groupBox);
        radioButtonShaded->setObjectName(QStringLiteral("radioButtonShaded"));

        gridLayout->addWidget(radioButtonShaded, 0, 1, 1, 1);

        radioButtonXRay = new QRadioButton(groupBox);
        radioButtonXRay->setObjectName(QStringLiteral("radioButtonXRay"));

        gridLayout->addWidget(radioButtonXRay, 1, 0, 1, 1);

        radioButtonSurface = new QRadioButton(groupBox);
        radioButtonSurface->setObjectName(QStringLiteral("radioButtonSurface"));

        gridLayout->addWidget(radioButtonSurface, 1, 1, 1, 1);

        radioButtonCustom = new QRadioButton(groupBox);
        radioButtonCustom->setObjectName(QStringLiteral("radioButtonCustom"));

        gridLayout->addWidget(radioButtonCustom, 2, 0, 1, 1);


        verticalLayout_4->addLayout(gridLayout);


        verticalLayout_7->addWidget(groupBox);

        groupBox_2 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        comboBoxColoring = new QComboBox(groupBox_2);
        comboBoxColoring->setObjectName(QStringLiteral("comboBoxColoring"));

        verticalLayout_3->addWidget(comboBoxColoring);


        verticalLayout_7->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_2 = new QVBoxLayout(groupBox_3);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        sliderRenderingQuality = new QSlider(groupBox_3);
        sliderRenderingQuality->setObjectName(QStringLiteral("sliderRenderingQuality"));
        sliderRenderingQuality->setMaximum(3);
        sliderRenderingQuality->setPageStep(1);
        sliderRenderingQuality->setValue(1);
        sliderRenderingQuality->setOrientation(Qt::Horizontal);
        sliderRenderingQuality->setTickPosition(QSlider::TicksAbove);

        verticalLayout_2->addWidget(sliderRenderingQuality);


        verticalLayout_7->addWidget(groupBox_3);

        groupBox_4 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        verticalLayout_5 = new QVBoxLayout(groupBox_4);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        labelWindShift = new CClickableLabel(groupBox_4);
        labelWindShift->setObjectName(QStringLiteral("labelWindShift"));

        formLayout->setWidget(0, QFormLayout::LabelRole, labelWindShift);

        sliderWindShift = new QSlider(groupBox_4);
        sliderWindShift->setObjectName(QStringLiteral("sliderWindShift"));
        sliderWindShift->setMinimum(-100);
        sliderWindShift->setMaximum(200);
        sliderWindShift->setOrientation(Qt::Horizontal);

        formLayout->setWidget(0, QFormLayout::FieldRole, sliderWindShift);

        labelWindWidth = new CClickableLabel(groupBox_4);
        labelWindWidth->setObjectName(QStringLiteral("labelWindWidth"));

        formLayout->setWidget(1, QFormLayout::LabelRole, labelWindWidth);

        sliderWindWidth = new QSlider(groupBox_4);
        sliderWindWidth->setObjectName(QStringLiteral("sliderWindWidth"));
        sliderWindWidth->setMinimum(-100);
        sliderWindWidth->setMaximum(100);
        sliderWindWidth->setOrientation(Qt::Horizontal);

        formLayout->setWidget(1, QFormLayout::FieldRole, sliderWindWidth);

        labelBrightness = new CClickableLabel(groupBox_4);
        labelBrightness->setObjectName(QStringLiteral("labelBrightness"));

        formLayout->setWidget(2, QFormLayout::LabelRole, labelBrightness);

        sliderBrightness = new QSlider(groupBox_4);
        sliderBrightness->setObjectName(QStringLiteral("sliderBrightness"));
        sliderBrightness->setMinimum(-100);
        sliderBrightness->setMaximum(100);
        sliderBrightness->setOrientation(Qt::Horizontal);

        formLayout->setWidget(2, QFormLayout::FieldRole, sliderBrightness);

        labelContrast = new CClickableLabel(groupBox_4);
        labelContrast->setObjectName(QStringLiteral("labelContrast"));

        formLayout->setWidget(3, QFormLayout::LabelRole, labelContrast);

        sliderContrast = new QSlider(groupBox_4);
        sliderContrast->setObjectName(QStringLiteral("sliderContrast"));
        sliderContrast->setMinimum(-100);
        sliderContrast->setMaximum(100);
        sliderContrast->setOrientation(Qt::Horizontal);

        formLayout->setWidget(3, QFormLayout::FieldRole, sliderContrast);

        labelSurfTolerance = new CClickableLabel(groupBox_4);
        labelSurfTolerance->setObjectName(QStringLiteral("labelSurfTolerance"));

        formLayout->setWidget(4, QFormLayout::LabelRole, labelSurfTolerance);

        sliderSurfTolerance = new QSlider(groupBox_4);
        sliderSurfTolerance->setObjectName(QStringLiteral("sliderSurfTolerance"));
        sliderSurfTolerance->setMinimum(-100);
        sliderSurfTolerance->setMaximum(100);
        sliderSurfTolerance->setOrientation(Qt::Horizontal);

        formLayout->setWidget(4, QFormLayout::FieldRole, sliderSurfTolerance);

        labelSurfSharpness = new CClickableLabel(groupBox_4);
        labelSurfSharpness->setObjectName(QStringLiteral("labelSurfSharpness"));

        formLayout->setWidget(5, QFormLayout::LabelRole, labelSurfSharpness);

        sliderSurfSharpness = new QSlider(groupBox_4);
        sliderSurfSharpness->setObjectName(QStringLiteral("sliderSurfSharpness"));
        sliderSurfSharpness->setMinimum(-100);
        sliderSurfSharpness->setMaximum(100);
        sliderSurfSharpness->setOrientation(Qt::Horizontal);

        formLayout->setWidget(5, QFormLayout::FieldRole, sliderSurfSharpness);


        verticalLayout_5->addLayout(formLayout);


        verticalLayout_7->addWidget(groupBox_4);

        groupBox_5 = new QGroupBox(scrollAreaWidgetContents);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        verticalLayout = new QVBoxLayout(groupBox_5);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        sliderCuttingPlane = new QSlider(groupBox_5);
        sliderCuttingPlane->setObjectName(QStringLiteral("sliderCuttingPlane"));
        sliderCuttingPlane->setMinimum(-75);
        sliderCuttingPlane->setMaximum(75);
        sliderCuttingPlane->setValue(75);
        sliderCuttingPlane->setOrientation(Qt::Horizontal);
        sliderCuttingPlane->setInvertedAppearance(true);

        verticalLayout->addWidget(sliderCuttingPlane);


        verticalLayout_7->addWidget(groupBox_5);

        verticalSpacer = new QSpacerItem(20, 22, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer);

        scrollArea->setWidget(scrollAreaWidgetContents);

        verticalLayout_6->addWidget(scrollArea);

        QWidget::setTabOrder(radioButtonMIP, radioButtonXRay);
        QWidget::setTabOrder(radioButtonXRay, radioButtonShaded);
        QWidget::setTabOrder(radioButtonShaded, radioButtonSurface);
        QWidget::setTabOrder(radioButtonSurface, comboBoxColoring);
        QWidget::setTabOrder(comboBoxColoring, sliderRenderingQuality);
        QWidget::setTabOrder(sliderRenderingQuality, sliderWindShift);
        QWidget::setTabOrder(sliderWindShift, sliderWindWidth);
        QWidget::setTabOrder(sliderWindWidth, sliderBrightness);
        QWidget::setTabOrder(sliderBrightness, sliderContrast);
        QWidget::setTabOrder(sliderContrast, sliderSurfTolerance);
        QWidget::setTabOrder(sliderSurfTolerance, sliderSurfSharpness);
        QWidget::setTabOrder(sliderSurfSharpness, sliderCuttingPlane);

        retranslateUi(VolumeRenderingWidget);

        QMetaObject::connectSlotsByName(VolumeRenderingWidget);
    } // setupUi

    void retranslateUi(QWidget *VolumeRenderingWidget)
    {
        VolumeRenderingWidget->setWindowTitle(QApplication::translate("VolumeRenderingWidget", "Volume Rendering", 0));
        groupBox->setTitle(QApplication::translate("VolumeRenderingWidget", "Rendering Mode", 0));
#ifndef QT_NO_TOOLTIP
        radioButtonMIP->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects MIP rendering mode.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButtonMIP->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects MIP rendering mode.", 0));
#endif // QT_NO_STATUSTIP
        radioButtonMIP->setText(QApplication::translate("VolumeRenderingWidget", "MIP", 0));
#ifndef QT_NO_TOOLTIP
        radioButtonShaded->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects Shaded rendering mode.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButtonShaded->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects Shaded rendering mode.", 0));
#endif // QT_NO_STATUSTIP
        radioButtonShaded->setText(QApplication::translate("VolumeRenderingWidget", "Shaded", 0));
#ifndef QT_NO_TOOLTIP
        radioButtonXRay->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects X-Ray rendering mode.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButtonXRay->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects X-Ray rendering mode.", 0));
#endif // QT_NO_STATUSTIP
        radioButtonXRay->setText(QApplication::translate("VolumeRenderingWidget", "X-Ray", 0));
#ifndef QT_NO_TOOLTIP
        radioButtonSurface->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects Surface rendering mode.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButtonSurface->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects Surface rendering mode.", 0));
#endif // QT_NO_STATUSTIP
        radioButtonSurface->setText(QApplication::translate("VolumeRenderingWidget", "Surface", 0));
#ifndef QT_NO_TOOLTIP
        radioButtonCustom->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects rendering mode controlled by plugins.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        radioButtonCustom->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects rendering mode controlled by plugins.", 0));
#endif // QT_NO_STATUSTIP
        radioButtonCustom->setText(QApplication::translate("VolumeRenderingWidget", "Custom", 0));
        groupBox_2->setTitle(QApplication::translate("VolumeRenderingWidget", "Coloring Lookup Tables", 0));
        comboBoxColoring->clear();
        comboBoxColoring->insertItems(0, QStringList()
         << QApplication::translate("VolumeRenderingWidget", "Bone surface", 0)
         << QApplication::translate("VolumeRenderingWidget", "Skin surface", 0)
        );
#ifndef QT_NO_TOOLTIP
        comboBoxColoring->setToolTip(QApplication::translate("VolumeRenderingWidget", "Selects look-up table.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        comboBoxColoring->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Selects look-up table.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_3->setTitle(QApplication::translate("VolumeRenderingWidget", "Rendering Quality", 0));
#ifndef QT_NO_TOOLTIP
        sliderRenderingQuality->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts quality of volume rendering.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderRenderingQuality->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts quality of volume rendering.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_4->setTitle(QApplication::translate("VolumeRenderingWidget", "Output Adjustment", 0));
        labelWindShift->setText(QApplication::translate("VolumeRenderingWidget", "Wind. Shift", 0));
#ifndef QT_NO_TOOLTIP
        sliderWindShift->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts shift of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderWindShift->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts shift of density window.", 0));
#endif // QT_NO_STATUSTIP
        labelWindWidth->setText(QApplication::translate("VolumeRenderingWidget", "Wind. Width", 0));
#ifndef QT_NO_TOOLTIP
        sliderWindWidth->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts width of density window.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderWindWidth->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts width of density window.", 0));
#endif // QT_NO_STATUSTIP
        labelBrightness->setText(QApplication::translate("VolumeRenderingWidget", "Brightness", 0));
#ifndef QT_NO_TOOLTIP
        sliderBrightness->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts brightness of volume rendering.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderBrightness->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts brightness of volume rendering.", 0));
#endif // QT_NO_STATUSTIP
        labelContrast->setText(QApplication::translate("VolumeRenderingWidget", "Contrast", 0));
#ifndef QT_NO_TOOLTIP
        sliderContrast->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts contrast of volume rendering.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderContrast->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts contrast of volume rendering.", 0));
#endif // QT_NO_STATUSTIP
        labelSurfTolerance->setText(QApplication::translate("VolumeRenderingWidget", "Surf. Tolerance", 0));
#ifndef QT_NO_TOOLTIP
        sliderSurfTolerance->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts surface tolerance.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderSurfTolerance->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts surface tolerance.", 0));
#endif // QT_NO_STATUSTIP
        labelSurfSharpness->setText(QApplication::translate("VolumeRenderingWidget", "Surf. Sharpness", 0));
#ifndef QT_NO_TOOLTIP
        sliderSurfSharpness->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjusts surface sharpness.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderSurfSharpness->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjusts surface sharpness.", 0));
#endif // QT_NO_STATUSTIP
        groupBox_5->setTitle(QApplication::translate("VolumeRenderingWidget", "Cutting Plane", 0));
#ifndef QT_NO_TOOLTIP
        sliderCuttingPlane->setToolTip(QApplication::translate("VolumeRenderingWidget", "Adjust cutting plane position.", 0));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        sliderCuttingPlane->setStatusTip(QApplication::translate("VolumeRenderingWidget", "Adjust cutting plane position.", 0));
#endif // QT_NO_STATUSTIP
    } // retranslateUi

};

namespace Ui {
    class VolumeRenderingWidget: public Ui_VolumeRenderingWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VOLUMERENDERINGWIDGET_H
