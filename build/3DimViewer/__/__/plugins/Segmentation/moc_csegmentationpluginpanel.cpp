/****************************************************************************
** Meta object code from reading C++ file 'csegmentationpluginpanel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../plugins/Segmentation/csegmentationpluginpanel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'csegmentationpluginpanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CSegmentationPluginPanel_t {
    QByteArrayData data[10];
    char stringdata0[161];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CSegmentationPluginPanel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CSegmentationPluginPanel_t qt_meta_stringdata_CSegmentationPluginPanel = {
    {
QT_MOC_LITERAL(0, 0, 24), // "CSegmentationPluginPanel"
QT_MOC_LITERAL(1, 25, 30), // "on_pushButtonPickValue_clicked"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 29), // "on_pushButtonSegmente_clicked"
QT_MOC_LITERAL(4, 87, 16), // "testColorisation"
QT_MOC_LITERAL(5, 104, 11), // "calculBorne"
QT_MOC_LITERAL(6, 116, 13), // "majCoeffMulti"
QT_MOC_LITERAL(7, 130, 5), // "value"
QT_MOC_LITERAL(8, 136, 14), // "majNbIteration"
QT_MOC_LITERAL(9, 151, 9) // "majRadius"

    },
    "CSegmentationPluginPanel\0"
    "on_pushButtonPickValue_clicked\0\0"
    "on_pushButtonSegmente_clicked\0"
    "testColorisation\0calculBorne\0majCoeffMulti\0"
    "value\0majNbIteration\0majRadius"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CSegmentationPluginPanel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x08 /* Private */,
       3,    0,   50,    2, 0x08 /* Private */,
       4,    0,   51,    2, 0x08 /* Private */,
       5,    0,   52,    2, 0x08 /* Private */,
       6,    1,   53,    2, 0x08 /* Private */,
       8,    1,   56,    2, 0x08 /* Private */,
       9,    1,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    7,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,

       0        // eod
};

void CSegmentationPluginPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CSegmentationPluginPanel *_t = static_cast<CSegmentationPluginPanel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButtonPickValue_clicked(); break;
        case 1: _t->on_pushButtonSegmente_clicked(); break;
        case 2: _t->testColorisation(); break;
        case 3: _t->calculBorne(); break;
        case 4: _t->majCoeffMulti((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 5: _t->majNbIteration((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->majRadius((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject CSegmentationPluginPanel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CSegmentationPluginPanel.data,
      qt_meta_data_CSegmentationPluginPanel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CSegmentationPluginPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CSegmentationPluginPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CSegmentationPluginPanel.stringdata0))
        return static_cast<void*>(const_cast< CSegmentationPluginPanel*>(this));
    if (!strcmp(_clname, "CAppBindings"))
        return static_cast< CAppBindings*>(const_cast< CSegmentationPluginPanel*>(this));
    return QWidget::qt_metacast(_clname);
}

int CSegmentationPluginPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
