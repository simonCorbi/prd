/****************************************************************************
** Meta object code from reading C++ file 'CMenuEx.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../../../../../../include/3dim/qtgui/controls/CMenuEx.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CMenuEx.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CMenuBarEx_t {
    QByteArrayData data[1];
    char stringdata0[11];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CMenuBarEx_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CMenuBarEx_t qt_meta_stringdata_CMenuBarEx = {
    {
QT_MOC_LITERAL(0, 0, 10) // "CMenuBarEx"

    },
    "CMenuBarEx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CMenuBarEx[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void CMenuBarEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject CMenuBarEx::staticMetaObject = {
    { &QMenuBar::staticMetaObject, qt_meta_stringdata_CMenuBarEx.data,
      qt_meta_data_CMenuBarEx,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CMenuBarEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CMenuBarEx::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CMenuBarEx.stringdata0))
        return static_cast<void*>(const_cast< CMenuBarEx*>(this));
    if (!strcmp(_clname, "CMenuHighlighter"))
        return static_cast< CMenuHighlighter*>(const_cast< CMenuBarEx*>(this));
    return QMenuBar::qt_metacast(_clname);
}

int CMenuBarEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenuBar::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_CMenuEx_t {
    QByteArrayData data[1];
    char stringdata0[8];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CMenuEx_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CMenuEx_t qt_meta_stringdata_CMenuEx = {
    {
QT_MOC_LITERAL(0, 0, 7) // "CMenuEx"

    },
    "CMenuEx"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CMenuEx[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void CMenuEx::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject CMenuEx::staticMetaObject = {
    { &QMenu::staticMetaObject, qt_meta_stringdata_CMenuEx.data,
      qt_meta_data_CMenuEx,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CMenuEx::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CMenuEx::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CMenuEx.stringdata0))
        return static_cast<void*>(const_cast< CMenuEx*>(this));
    if (!strcmp(_clname, "CMenuHighlighter"))
        return static_cast< CMenuHighlighter*>(const_cast< CMenuEx*>(this));
    return QMenu::qt_metacast(_clname);
}

int CMenuEx::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMenu::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
