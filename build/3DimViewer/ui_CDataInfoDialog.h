/********************************************************************************
** Form generated from reading UI file 'CDataInfoDialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CDATAINFODIALOG_H
#define UI_CDATAINFODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_CDataInfoDialog
{
public:
    QGridLayout *gridLayout;
    QTableWidget *tableWidget;

    void setupUi(QDialog *CDataInfoDialog)
    {
        if (CDataInfoDialog->objectName().isEmpty())
            CDataInfoDialog->setObjectName(QStringLiteral("CDataInfoDialog"));
        CDataInfoDialog->resize(560, 349);
        CDataInfoDialog->setMinimumSize(QSize(400, 300));
        CDataInfoDialog->setSizeGripEnabled(true);
        gridLayout = new QGridLayout(CDataInfoDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tableWidget = new QTableWidget(CDataInfoDialog);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->setColumnCount(2);
        tableWidget->horizontalHeader()->setDefaultSectionSize(200);
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setDefaultSectionSize(24);

        gridLayout->addWidget(tableWidget, 0, 0, 1, 1);


        retranslateUi(CDataInfoDialog);

        QMetaObject::connectSlotsByName(CDataInfoDialog);
    } // setupUi

    void retranslateUi(QDialog *CDataInfoDialog)
    {
        CDataInfoDialog->setWindowTitle(QApplication::translate("CDataInfoDialog", "Data Information", 0));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("CDataInfoDialog", "Parameter", 0));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("CDataInfoDialog", "Value", 0));
    } // retranslateUi

};

namespace Ui {
    class CDataInfoDialog: public Ui_CDataInfoDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CDATAINFODIALOG_H
