////////////////////////////////////////////////////////////
// $Id: mctris.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCTRIS_H
#define MCTRIS_H

////////////////////////////////////////////////////////////
// include

#include <MDSTk/Module/mdsSerializer.h>

#include "mcedges.h"
#include "mctri.h"


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

/** @todo Udelat iteratory pro prochazeni kontejneru entit (vertices, edges, tris, tetras). */

////////////////////////////////////////////////////////////
/*!
 * Container of triangles (polygonal mesh).
 */
class VCTL_EXPORT MCTriS : public MCEntitieS<MCTri>, public mds::mod::CSerializable
{
private:
    //! Pointer to a container of vertices.
    MCVerticeS * vertices;

    //! Container of edges in the mesh (may not be used).
    MCEdgeS edges;

public:
    //! Standard method getEntityName().
    MDS_ENTITY_NAME("MCTriS");

    //! Standard method getEntityCompression().
    MDS_ENTITY_COMPRESSION(mds::mod::CC_RAW);

    enum STLState
    {
      STATE_INIT,
      STATE_SOLID,
      STATE_INSOLID,
      STATE_FACET,
      STATE_NORMAL,
      STATE_INFACET,
      STATE_OUTER,
      STATE_INLOOP,
      STATE_VERTEX,
      STATE_ENDSOLID,
      STATE_FINISH
    };

    enum STLTokenType
    {
      TOKEN_UNKNOWN,
      TOKEN_ERROR,
      TOKEN_IDENTIFIER,
      TOKEN_NUMBER,
      TOKEN_SOLID,
      TOKEN_ENDSOLID,
      TOKEN_FACET,
      TOKEN_ENDFACET,
      TOKEN_LOOP,
      TOKEN_ENDLOOP,
      TOKEN_OUTER,
      TOKEN_VERTEX,
      TOKEN_NORMAL
    };

    struct STLToken
    {
      STLTokenType type;
      mds::tSize begin;
      mds::tSize end;

      STLToken():
        type(TOKEN_UNKNOWN), begin(-1), end(0)
      {
        // nothing
      }
    };

public:
    //! Default constructor.
    MCTriS() { vertices = edges.GetVerticeS(); }

    //! Destructor.
    ~MCTriS() {}


    //! Serializes the triangular mesh.
    template <class S>
    void serialize(mds::mod::CChannelSerializer<S>& Writer)
    {
        // serialize vertices of the mesh first
        vertices->serialize(Writer);

        // Begin of data serialization block
        Writer.beginWrite(*this);

        MCTri * aktual = GetFirst();        // actual entity pointer

        // write number of triangles
        Writer.write(MCList<MCTri>::list_node_number);

        // entity cycle
        while( aktual )
        {
            // save actual entity
            aktual->SerializeEntity(Writer);

            // get next actual entity pointer
            aktual = aktual->GetNext();
        }

        // End of the block
        Writer.endWrite(*this);
    }

    //! Deserializes the triangular mesh.
    template <class S>
    void deserialize(mds::mod::CChannelSerializer<S>& Reader)
    {
        // clear the container
        ClearAll();

        // deserialize vertices of the mesh first
        vertices->deserialize(Reader);

        std::vector<MCVertex *> index_array;

        // make an ordered array of vertices
        vertices->MakeIndexVector(index_array);

        // Begin of data deserialization block
        Reader.beginRead(*this);

        // read the number of entities
        int pocet = 0;
        Reader.read(pocet);

        // entity cycle
        MCTri work_tri;
        for( int i = 0; i < pocet; ++i )
        {
            // read new entity
            work_tri.DeserializeEntity(Reader, index_array);

            // including read tri into container structures
            MCTri *new_tri = New(work_tri.GetVertex(0), work_tri.GetVertex(1), work_tri.GetVertex(2));
            assert(new_tri);
            new_tri->SetEntityAttributes(&work_tri);
        }

        // End of the block
        Reader.endRead(*this);
    }


    //! Reads polygonal mesh in the binary STL format from a given channel.
    //! @return  false on failure.
    bool LoadSTL(mds::mod::CChannel& Channel);

    //! Writes polygonal mesh to a given channel in the binary STL format.
    //! @return  false on failure.
    bool SaveSTL(mds::mod::CChannel& Channel);

    //! Writes the triangular mesh to a given channel in VRML format.
    //! @return  false on failure.
    bool SaveVRML(mds::mod::CChannel& Channel);


    //! Creates a new triangle.
    //! - Checks existence of the triangle!
    //! - Returns pointer to the new triangle or a previously created one.
    MCTri * New(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2);

    //! Removes triangle from the mesh.
    //! - Deregistration of the triangle from its vertices.
    //! - Removes the triangle from the sorting queue.
    void Erase( MCTri * del_ent);

    //! Deregistration of all triangles in the mesh from vertices.
    void DeRegistrationAll();


    //! Returns all triangles sharing a given edge.
    void GetTriEdge( MCVertex * _u0, MCVertex * _u1, std::vector<MCTri *> & tri_pole );

    //! Returns the number of triangles sharing a given edge.
    int GetTriEdgeNumber( MCVertex * _u0, MCVertex * _u1);

    //! Checks if a specified edge exists in the mesh.
    bool TestEdgeExistence( MCVertex * _u0, MCVertex * _u1 );

    //! Checks if a specified triangle exists in the mesh.
    //! - Returns pointer to the found triangle, NULL otherwise.
    MCTri * TestExistence(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2);

    //! Checks the triangular mesh is a closed manifold object.
    bool TestManifold();


    //! Returns pointer to the container of vertices.
    MCVerticeS * GetVerticeS()                           { return vertices; }

    //! Returns pointer to the container of edges.
    MCEdgeS * GetEdgeS()                                 { return (&edges); };


    //! Prepares the container of edges.
    void MakeAllTrisEdges();

    //! Adds all edges of a given triangle to the container of edges.
    void MakeTriEdges(MCTri * _tri);

    //! Erases the container of edges.
    void EraseEdgesNoTris();

    //! Clears all tri currently stored in the container.
    virtual void ClearAll();

    //! Flip normals of all tris of the tris mesh
    void FlipTrisNormal();

protected:

    /** Read STL token form ASCII STL format from buffer \p buffer starting at
    position \p bufferStart and finishing at position \p bufferEnd.
    \p bufferStart is then updated to point behind last character of the token. */
    STLToken getSTLToken(const char * buffer, mds::tSize & bufferStart,
      mds::tSize buffer_end) const;

    /** Checks if word in string buffer \p buffer starting at position
    \p wordStart and ending at position \p wordEnd is a floating point
    number. */
    bool isFloat(const char * buffer, const mds::tSize wordStart,
      const mds::tSize wordEnd) const;
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
