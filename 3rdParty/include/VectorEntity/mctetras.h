////////////////////////////////////////////////////////////
// $Id: mctetras.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCTETRAS_H
#define MCTETRAS_H

////////////////////////////////////////////////////////////
// includes

// MDSTk
#include <MDSTk/Module/mdsSerializer.h>

#include "mcvertices.h"
#include "mcedges.h"
#include "mctris.h"
#include "mctetra.h"

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Container of tetrahedra (tetrahedra mesh).
 * - Derived from the general container MCEntitieS.
 */
class VCTL_EXPORT MCTetraS : public MCEntitieS<MCTetra>, public mds::mod::CSerializable
{
private:
    //! Container of all vertices.
    MCVerticeS * vertices;

    //! Container of all edges (may not be used).
    MCEdgeS * edges;

    //! Container of all triangles.
    MCTriS tris;

    //! Actual value flag used for signing the tetrahedra mesh.
    unsigned int actual_value_flag;

public:
    //! Standard method getEntityName().
    MDS_ENTITY_NAME("MCTetraS");

    //! Standard method getEntityCompression().
    MDS_ENTITY_COMPRESSION(mds::mod::CC_RAW);

public:
    //! Default constructor.
    MCTetraS() { vertices = tris.GetVerticeS(); edges = tris.GetEdgeS(); actual_value_flag = 0; }

    //! Destructor.
    ~MCTetraS() {}


    //! Serializes the tetrahedral mesh.
    template <class S>
    void serialize(mds::mod::CChannelSerializer<S>& Writer)
    {
        // serialize vertices of the mesh
        vertices->serialize(Writer);

        // Begin of data serialization block
        Writer.beginWrite(*this);

        MCTetra * aktual =  GetFirst();

        // write entity number into channel
        Writer.write(MCList<MCTetra>::list_node_number);

        // entity cycle
        while( aktual )
        {
            // save actual entity
            aktual->SerializeEntity(Writer);

            // get next actual entity pointer
            aktual = aktual->GetNext();
        }

        // End of the block
        Writer.endWrite(*this);
    }

    //! Deserializes the tetrahedral mesh.
    template <class S>
    void deserialize(mds::mod::CChannelSerializer<S>& Reader)
    {
        // clear container
        ClearAll();

        // deserialize vertices of the mesh
        vertices->deserialize(Reader);

        std::vector<MCVertex *> index_array;

        // make an ordered array of tetrahedra
        vertices->MakeIndexVector(index_array);

        // Begin of data deserialization block
        Reader.beginRead(*this);

        // read entity number from channel
        int pocet = 0;
        Reader.read(pocet);

        // entity cycle
        MCTetra work_tetra;
        for( int i = 0; i < pocet; ++i )
        {
            // read new entity
            work_tetra.DeserializeEntity(Reader, index_array);

            // including deserialized tetrahedron into container structures
            MCTetra *new_tetra = New(work_tetra.GetVertex(0), work_tetra.GetVertex(1), work_tetra.GetVertex(2), work_tetra.GetVertex(3));
            assert(new_tetra);
            new_tetra->SetEntityAttributes(&work_tetra);
        }

        // End of the block
        Reader.endRead(*this);
    }


    //! Returns value of the actual value flag.
    unsigned int GetActualValueFlag() { return actual_value_flag; }

    //! Sets value of the actual value flag.
    void SetActualValueFlag(unsigned int _value) { actual_value_flag = _value; }


    //! Creates a new tetrahedron.
    //! - Checks existence of the tetrahedra!
    //! - Returns pointer to the new tetrahedra or a previously created one.
    MCTetra * New(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2, MCVertex * _u3);

    //! Creates a new tetrahedron.
    //! - The method doesn't check existence of the tetrahedra!
    //! - Returns pointer to the newly created tetrahedra.
    MCTetra * New2(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2, MCVertex * _u3);

    //! Removes tetrahedra from the mesh.
    //! - Deregistration of the terahedra from its vertices.
    //! - Removes the tetra from the sorting queue.
    void Erase( MCTetra * del_ent);

    //! Deregisters all tetrahedra.
    void DeRegistrationAll();


    //! Returns all tetrahedra sharing a given edge.
    void GetTetraEdge( MCVertex * _u0, MCVertex * _u1, std::vector<MCTetra *> & tetra_pole );

    //! Returns the number of tetrahedra sharing a given edge.
    int GetTetraEdgeNumber( MCVertex * _u0, MCVertex * _u1 );

    //! Checks if a given edge exists in the mesh.
    bool TestEdgeExistence( MCVertex * _u0, MCVertex * _u1 );

    //! Checks if a given triangle exists in the mesh.
    bool TestTriExistence( MCVertex * _u0, MCVertex * _u1, MCVertex * _u2 );

    //! Checks if a specified tetrahedron already exists in the mesh.
    //! - Returns pointer to the found tetra, NULL otherwise.
    MCTetra * TestExistence(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2, MCVertex * _u3);


    //! Returns pointer to the container of vertices.
    MCVerticeS * GetVerticeS()                           { return vertices; }

    //! Returns pointer to the container of edges.
    MCEdgeS * GetEdgeS()                                 { return edges; }

    //! Returns pointer to the container of triangles.
    MCTriS * GetTriS()                                   { return &tris; }


    //! Creates container of edges for all tetrahedra in the mesh.
    void MakeAllTetrasEdges();

    //! Adds all edges of a given tetrahedra to the container of edges.
    void MakeTetraEdges(MCTetra * _tetra);

    //! Removes all unused vertices from the mesh.
    void EraseVerticesNoTetras();

    //! Erases the container of edges.
    void EraseEdgesNoTetras();
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
