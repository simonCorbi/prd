////////////////////////////////////////////////////////////
// $Id: mcvector3d.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCVECTOR3D_H
#define MCVECTOR3D_H

////////////////////////////////////////////////////////////
// include files

#include "mcpoint3d.h"

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Class encapsulates a vector in 3D space as well as some
 * basic arithmetic.
 * - Just for backward compatibility!
 */
class VCTL_EXPORT MCVector3D : public MCCoordinate3D<double>
{
public:
    //! Base class.
    typedef MCCoordinate3D<double> tCoordinates;

public:
    //! Default constructor.
    //! - Initializes all coordinates to zero.
    MCVector3D() {}

    //! Constructor that initializes vector cordinates.
    MCVector3D(const MCVector3D& v) : tCoordinates(v) {}

    //! Constructor that initializes vector cordinates.
    MCVector3D(MCVector3D * v) : tCoordinates(*v) {}

    //! Constructor that initializes vector cordinates.
    MCVector3D(double * _x, double * _y, double * _z) : tCoordinates(*_x, *_y, *_z) {}

    //! Constructor that initializes vector cordinates.
    MCVector3D(const double& _x, const double& _y, const double& _z) : tCoordinates(_x, _y, _z) {}

    //! Constructor that initializes vector cordinates.
    //! @param p1 - starting point.
    //! @param p2 - end point.
    MCVector3D(MCPoint3D * p1, MCPoint3D * p2) : tCoordinates(p2->x() - p1->x(), p2->y() - p1->y(), p2->z() - p1->z()) {}

    //! Destructor.
    ~MCVector3D() {}


    //! Initializes the vector.
    //! @param p1 - starting point.
    //! @param p2 - end point.
    void Make(const MCPoint3D & p1, const MCPoint3D & p2) { x() = p2.x() - p1.x(); y() = p2.y() - p1.y(); z() = p2.z() - p1.z(); }

    //! Vector addition.
    MCVector3D operator + (const MCVector3D & v) { return MCVector3D( x() + v.x(), y() + v.y() , z() + v.z() ); }

    //! Vector subtraction.
    MCVector3D operator - (const MCVector3D & v) { return MCVector3D( x() - v.x(), y() - v.y(), z() - v.z() ); }

    //! Makes a counter vector having opossite direction.
    MCVector3D operator - () { return MCVector3D(-x(), -y(), -z()); }


    //! Vector addition.
    void operator += (const MCVector3D & v) { x() += v.x(); y() += v.y(); z() += v.z(); }

    //! Vector subtraction.
    void operator -= (const MCVector3D & v) { x() -= v.x(); y() -= v.y(); z() -= v.y(); }

    //! Multiplies the vector by a given number.
    void operator *= (double cislo) { x() *= cislo; y() *= cislo; z() *= cislo; }


    //! Calculates vector product.
    MCVector3D operator % (const MCVector3D & v) const
    {
        MCVector3D  _v(y() * v.z() - z() * v.y(), z() * v.x() - x() * v.z(), x() * v.y() - y() * v.x());
        return _v;
    }

    //! Calculates vector product.
    MCVector3D& VectorProduct(const MCVector3D & u, const MCVector3D & v)
    {
        x() = u.y() * v.z() - u.z() * v.y();
        y() = u.z() * v.x() - u.x() * v.z();
        z() = u.x() * v.y() - u.y() * v.x();
        return *this;
    }


    //! Calculates dot product.
    double operator * (const MCVector3D & v) const { return ( x() * v.x() + y() * v.y() + z() * v.z() ); }

    //! Calculates dot product.
    static double DotProduct(const MCVector3D & u, const MCVector3D & v) { return ( u.x() * v.x() + u.y() * v.y() + u.z() * v.z() ); }

    //! Multiplies the vector coordinates by a given number.
    MCVector3D operator *(double cislo) const
    {
        return MCVector3D( x()*cislo, y()*cislo, z()*cislo );
    }

    //! Divides the vector coordinates by a given number.
    MCVector3D operator / (double cislo) const
    {
        return MCVector3D( x()/cislo, y()/cislo, z()/cislo );
    }

    //! Length of the vector.
    double Length() const { return sqrt(x()*x() + y()*y() + z()*z()); }

    //! Square root of the vector length.
    double SquareLength() const { return (x()*x() + y()*y() + z()*z()); }

    //! Vector normalization so that the length will be equal to one.
    MCVector3D & Normalization()
    {
        double inv = 0.0, d = Length();
        if( d > 0 )
        {
            inv = 1.0 / d;
        }

        x() *= inv;
        y() *= inv;
        z() *= inv;

        return *this;
    }

    //! Calculates normal of a plane defined by three points.
    static MCVector3D & MakeNormal( MCPoint3D * p0, MCPoint3D * p1, MCPoint3D * p2, MCVector3D & normala )
    {
        MCVector3D v01(p0, p1), v02(p0, p2);     // pomocne vektory

        // vypocet normaly, vektorovy soucin
        normala.VectorProduct(v01, v02);
        // normalizace normaly
        normala.Normalization();

        // vraceni reference vysledneho vektoru
        return normala;
    }

    //! Calculate angle between two vectors, not signed, in range <0, Pi>
    static double VectorAngle( const MCVector3D & u, const MCVector3D & v )
    {
        // calculate vectors length
        double      lu = u.Length();
        double      lv = v.Length();

        // calculatce vectors angle, test existence
        if ( (lu != 0.0) && (lv != 0.0) )
            return acos( DotProduct(u, v) / (lu * lv) );
        else
            return 0.0;
    }

    //! Calculate angle between two vectors, for rotation purpose, signed, in range <0, Pi>
    static double VectorAngleRotation( const MCVector3D & u, const MCVector3D & v, const MCVector3D & axis )
    {
        // calculate vectors length
        double      lu = u.Length();
        double      lv = v.Length();

        // calculatce vectors angle, test existence
        if ( (lu != 0.0) && (lv != 0.0) )
        {
            MCVector3D      normal;             // given vectors normal

            // calculate vectors normal
            normal.VectorProduct(u, v);
            // determine angle sign
            double sign = (DotProduct(normal, axis) < 0) ? -1 : 1; 
            // return result angle
            return acos( DotProduct(u, v) / (lu * lv) ) * sign;
        }
        else
            return 0.0;
    }

};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
