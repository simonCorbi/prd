////////////////////////////////////////////////////////////
// $Id: mclist.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCLIST_H
#define MCLIST_H

////////////////////////////////////////////////////////////
// include

#include "mclistnode.h"

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * A simple container of objects derived from MCListNode
 * implemented as a doubly-linked list.
 */
template <typename NODE_TYPE>
class MCList
{
public:
    //! Node type.
    typedef NODE_TYPE MT_NODE;

protected:
    //! Number of nodes in the list.
    int list_node_number;

    //! Pointer to the first node.
    NODE_TYPE * first_node;

    //! Pointer to the last node in the list.
    NODE_TYPE * last_node;

public:
    //! Default constructor.
    MCList() : list_node_number(0), first_node(NULL), last_node(NULL) {}

    //! An empty destructor!
    ~MCList() {}

    //! Returns pointer to the first node in the list,
    //! NULL in case of the empty list.
    NODE_TYPE * GetFirst() { return first_node; }

    //! Returns pointer to the first node in the list,
    //! NULL in case of the empty list.
    NODE_TYPE * GetLast() { return last_node; }

    //! Returns the number of nodes in the list.
    int GetNumber() const { return list_node_number; }

protected:
    //! Clears the list of nodes.
    //! - The method doesn't free any memory.
    void ClearAllNode() { first_node = last_node = NULL; list_node_number = 0; }

    //! Adds a new node at the end of the list.
    void AddNode( NODE_TYPE * new_node )
    {
        // test existence daneho ukazatele
        assert(new_node != NULL);

        // pripojeni noveho uzlu do retezu
        if (list_node_number == 0)
        {
            // nastaveni pridavaneho uzlu jako prvniho a posledniho
            last_node = first_node = new_node;
            // vynulovani ukazatelu na predchozi a nasledujici pro novy uzel
            new_node->SetPrev(NULL);
            new_node->SetNext(NULL);
        }
        else
        {
            // zacleneni pridavaneho uzlu do retezu na konec
            last_node->SetNext(new_node);
            new_node->SetPrev(last_node);
            last_node = new_node;
            // vynulovani ukazatelu na nasledujici pro novy uzel
            new_node->SetNext(NULL);
        }

        // inkrementace poctu
        list_node_number++;
    }

    //! Removes a given node from the list.
    void EraseNode( NODE_TYPE * erase_node )
    {
        // test existence daneho ukazatele
        assert(erase_node != NULL);

        // test nuloveho poctu prvku retezu
        if (list_node_number != 0)
        {
            // test, neni-li mazany uzel prvnim uzlem
            if (erase_node == first_node)
            {
                // nastaveni noveho prvniho uzlu
                first_node = erase_node->GetNext();
                // nulovani predchudce noveho prvniho uzlu, pokud existuje
                if (first_node != NULL)
                    first_node->SetPrev(NULL);
            }
            // test, neni-li mazany uzel poslednim uzlem
            else if (erase_node == last_node)
            {
                // nastaveni noveho posledniho uzlu
                last_node = erase_node->GetPrev();
                // nulovani nasledovnika noveho posledniho uzlu, pokud existuje
                if (last_node != NULL)
                    last_node->SetNext(NULL);
            }
            else // odstranovany uzel je uprostred retezu
            {
                // premostit predchoziho na nasledujiciho a naopak pro vyhazovany uzel
                erase_node->GetPrev()->SetNext(erase_node->GetNext());
                erase_node->GetNext()->SetPrev(erase_node->GetPrev());
            }

            // vynulovani ukazatelu na sousedy v odstranovanem uzlu
            erase_node->SetPrev(NULL);
            erase_node->SetNext(NULL);

            // dekrementace poctu node
            list_node_number--;
        }
    }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
