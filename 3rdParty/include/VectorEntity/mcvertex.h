////////////////////////////////////////////////////////////
// $Id: mcvertex.h 1869 2010-09-02 22:54:56Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCVERTEX_H
#define MCVERTEX_H

////////////////////////////////////////////////////////////
// include

#include "mcentity.h"

// STL
#include <sstream>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * A vertex representation  used by polygonal meshes.
 * - Derived from MCPoint3D and MCEntity classes.
 */
class VCTL_EXPORT MCVertex : public MCEntity<MCVertex>, public MCPoint3D
{
protected:
    //! Base type.
    typedef MCEntity<MCVertex> tBase;

    //! Node index (used during saving).
    unsigned int index;

    //! Vertex coordinates hash code.
    unsigned int m_hash_code;

    //! Pointer to a first edge sharing the vertex.
    MCEdge * edge_list;

    //! Pointer to a first triangle sharing the vertex.
    MCTri * tri_list;

    //! Pointer to a first tetrahedron sharing the vertex.
    MCTetra * tetra_list;

public:
    //! Default constructor.
    MCVertex() : index(0), m_hash_code(0), edge_list(NULL), tri_list(NULL), tetra_list(NULL) {}

    //! Copy constructor.
    MCVertex(const MCPoint3D & _point) : MCPoint3D(_point), index(0), m_hash_code(0), edge_list(NULL), tri_list(NULL), tetra_list(NULL) {}

    //! Just another constructor.
    MCVertex(const double& _x, const double& _y, const double& _z) : MCPoint3D(_x, _y, _z), index(0), edge_list(NULL), tri_list(NULL), tetra_list(NULL) {}

    //! Destructor.
    ~MCVertex() {}


    //! Writes the vertex data.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        // saving vertex coordinates
        Writer.write(this->x());
        Writer.write(this->y());
        Writer.write(this->z());

        // serialize MCEntity data
        tBase::SerializeEntity(Writer);
    }

    //! Reads the Vertex data.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader)
    {
        // loading vertex coordinates
        Reader.read(this->x());
        Reader.read(this->y());
        Reader.read(this->z());

        // deserialize MCEntity data
        tBase::DeserializeEntity(Reader);
    }


    //! Sets the vertex index.
    void SetIndex( unsigned int _index ) { index = _index; }

    //! Returns the vertex index;
    unsigned int GetIndex() const { return index; }


    //! Sets pointer to an already registered edge linked to the vertex.
    void SetRegisteredEdge(MCEdge * _edge) { edge_list = _edge; }

    //! Returns pointer to a first registered edge.
    MCEdge * GetRegisteredEdge() { return edge_list; }
    const MCEdge * GetRegisteredEdge() const { return edge_list; }

    //! Sets pointer to an already registered triangle linked to the vertex.
    void SetRegisteredTri(MCTri * _tri) { tri_list = _tri; }

    //! Returns pointer to a registered triangle.
    MCTri * GetRegisteredTri() { return tri_list; }
    const MCTri * GetRegisteredTri() const { return tri_list; }

    //! Sets pointer to a registered tetrahedron already linked to the vertex.
    void SetRegisteredTetra(MCTetra * _tetra) { tetra_list = _tetra; }

    //! Returns pointer to a registered tetrahedron.
    MCTetra * GetRegisteredTetra() { return tetra_list; }
    const MCTetra * GetRegisteredTetra() const { return tetra_list; }

    //! Returns vector of all linked edges.
    void GetRegisteredEdgeList(std::vector<MCEdge *> & _edge_list);

    //! Returns vector of all linked triangles.
    void GetRegisteredTriList(std::vector<MCTri *> & _tri_list);

    //! Returns vector of all linked tetrahedra.
    void GetRegisteredTetraList(std::vector<MCTetra *> & _tetra_list);

    //! Returns vector of triangles registered for this vertex,
    //! excluding those including a given exclude vertex.
    void GetRegisteredTriListExclude(std::vector<MCTri *> & _tri_list, MCVertex * exclude_vertex);


    //! Checks identity of two vertices.
    bool TestIdentity( MCVertex * _test ) const
    {
        assert(_test);
        #ifndef NO_VERTEX_IDENTITY
            return ((mds::math::getAbs(x() - _test->GetX()) < DOUBLE_MIN)
                    && (mds::math::getAbs(y() - _test->GetY()) < DOUBLE_MIN)
                    && (mds::math::getAbs(z() - _test->GetZ()) < DOUBLE_MIN));
        #else
            return false;
        #endif
    }

    //! Returns hash code of vertex coordinates.
    unsigned int GetHashCode() const { return m_hash_code; }

    //! Sets the hash code equal to a given vertex.
    void SetHashCode(MCVertex & _vertex) { m_hash_code = _vertex.m_hash_code; }

    //! Calculates hash code of vertex coordinates.
    void MakeHashCode()
    {
        // zaokrouhleni souradnic vrcholu a ulozeni vysledku do pracovniho pole pro vypocet hash kodu
        double double_array[3];
        double_array[0] = floor(x() * DOUBLE_MAX + 0.5);
        double_array[1] = floor(y() * DOUBLE_MAX + 0.5);
        double_array[2] = floor(z() * DOUBLE_MAX + 0.5);

        // vypocet hash kodu a vraceni vysledku
        m_hash_code = MakeStringHashCode(reinterpret_cast<char *>(double_array), sizeof(double)*3);
    }

    //! Returns text description of the vertex.
    std::string ToString() const
    {
        std::ostringstream text_buff;
        text_buff << "Vertex " << this << " : " << x() << " / " << y() << " / " << z();
        return text_buff.str();
    }
};


} // namespace vctl

#endif // MCVERTEX_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

