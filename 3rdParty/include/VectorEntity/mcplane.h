/////////////////////////////////////////////////////////////
// $Id: mcplane.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCPLANE_H
#define MCPLANE_H

////////////////////////////////////////////////////////////
// include

#include "mcvertex.h"
#include "mcedge.h"

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Plane defined by its equation coefficients.
 * - The coefficients of the equation (A*x + B*y + C*z + D = 0) need not to be normalized.
 * - The coefficients A, B, and C cannot be equal to zero at all.
*/
class VCTL_EXPORT MCPlane: public mds::base::CSmallObject<>
{
public:
	//! Relative position of an edge and a plane.
    enum MCRelativePosition
    {
        //! The edge intersects the plane.
        INTERSECTS = 0,

        //! First endpoint of the edge lies in the plane.
        FIRST_VERTEX_IN_PLANE,

        //! Second endpoint of the edge lies in the plane.
        SECOND_VERTEX_IN_PLANE,

        //! Both endpoints of the edge lie in the plane.
        BOTH_IN_PLANE,

        //! The edge does not intersect the plane.
        OUT_OF_PLANE
    };

	//! Types of coordinate planes.
    enum MCCoordinatePlaneType
    {
        //! normal vector (0,0,1).
        XY_PLANE = 0,

        //! normal vector (0,1,0).
        XZ_PLANE,

        //! normal vector (1,0,0).
        YZ_PLANE
    };

public:
	//! Default constructor.
    //! - Makes a XY coordinate plane
    MCPlane() : _A(0.0), _B(0.0), _C(1.0), _D(0.0) {}

	//! A constructor with plane equation coefficients.
    MCPlane(double A, double B, double C, double D) : _A(A), _B(B), _C(C), _D(D) { NormalizeVector(); }

	//! A constructor with a normal vector of the plane and a point lying in the plane.
    MCPlane(MCVertex * vertex, MCVector3D * normal)
    {
        _A = normal->GetX();
        _B = normal->GetY();
        _C = normal->GetZ();
        _D = -(_A * vertex->GetX() + _B * vertex->GetY() + _C * vertex->GetZ());
        NormalizeVector();
    }

	//! A constructor with three points lying in the plane.
    MCPlane(MCVertex * v1, MCVertex * v2, MCVertex * v3)
    {
        MCVector3D vec1(v1, v2);
        MCVector3D vec2(v1, v3);
        MCVector3D normal = vec1 % vec2;
        _A = normal.GetX();
        _B = normal.GetY();
        _C = normal.GetZ();
        _D = -(_A * v1->GetX() + _B * v1->GetY() + _C * v1->GetZ());
        NormalizeVector();
    }

	//! A constructor of a coordinate plane.
    MCPlane(MCCoordinatePlaneType cplane) : _A(0.0), _B(0.0), _C(0.0), _D(0.0)
    {
        switch(cplane)
        {
            case XY_PLANE: _C = 1.0; break;
            case XZ_PLANE: _B = 1.0; break;
            case YZ_PLANE: _A = 1.0; break;
        }
    }

	//! A constructor of a coordinate plane.
    //! - Vertex lies in the plane.
    MCPlane(MCCoordinatePlaneType cplane, MCVertex * vertex) : _A(0.0), _B(0.0), _C(0.0)
    {
        switch(cplane)
        {
            case XY_PLANE: _C = 1.0; _D = -vertex->GetZ(); break;
            case XZ_PLANE: _B = 1.0; _D = -vertex->GetY(); break;
            case YZ_PLANE: _A = 1.0; _D = -vertex->GetX(); break;
        }
    }

	//! A constructor of a coordinate plane.
    //! - The plane is in the distance from coordinate origin.
	MCPlane(MCCoordinatePlaneType cplane, double distance)
        : _A(0.0), _B(0.0), _C(0.0), _D(distance)
    {
        switch(cplane)
        {
            case XY_PLANE: _C = 1.0; break;
            case XZ_PLANE: _B = 1.0; break;
            case YZ_PLANE: _A = 1.0; break;
        }
    }

	//! Destructor.
    ~MCPlane() {}

	//! Sets the coefficient.
    void SetA(double A) { _A = A; }
	void SetB(double B) { _B = B; }
	void SetC(double C) { _C = C; }
	void SetD(double D) { _D = D; }

	//! Returns the coefficient.
    double GetA() const { return _A; }
	double GetB() const { return _B; }
	double GetC() const { return _C; }
	double GetD() const { return _D; }

	//! Returns the normal vector of a plane.
    MCVector3D GetNormal() { return MCVector3D(_A, _B, _C); }

	//! Moves the plane into a point.
    void MoveTo(MCVertex * vertex)
    {
        _D = -(_A * vertex->GetX() + _B * vertex->GetY() + _C * vertex->GetZ());
    }

    //! Moves the plane by a \a distance.
	void MoveBy(double distance) { _D += distance; }

	//! Returns relative position of an edge and the plane.
	//! - First and second endpoint of an edge.
    //! - Reference to an intersection vertex whose coordinates are set
    //!   if the edge intersects the plane, otherwise the coordinates are not set
    //!   (the function returns BOTH_IN_PLANE or OUT_OF_PLANE).
    MCRelativePosition Position(MCEdge * edge, MCVertex & intersection)
    {
        return Position(edge->GetVertex(0), edge->GetVertex(1), intersection);
    }

    //! Returns relative position of an edge and the plane.
    //! - First and second endpoint of an edge.
	//! - Reference to an intersection vertex whose coordinates are set
    //!   if the edge intersects the plane, otherwise the coordinates are not set
    //!   (the function returns BOTH_IN_PLANE or OUT_OF_PLANE).
    MCRelativePosition Position(MCVertex * v1, MCVertex * v2, MCVertex & intersection);

    //! Tests identity of two planes.
    bool TestIdentity( MCPlane * test_plane )
    {
        return ((mds::math::getAbs(_A - test_plane->GetA()) < DOUBLE_MIN)
              && (mds::math::getAbs(_B - test_plane->GetB()) < DOUBLE_MIN)
              && (mds::math::getAbs(_C - test_plane->GetC()) < DOUBLE_MIN)
              && (mds::math::getAbs(_D - test_plane->GetD()) < DOUBLE_MIN));
    }

    //! Substitutes a point into the plane equation.
    //! - If the normal vector is normalised it returns the distance
    //!   of the point from the plane.
    //! - Returns result of the equation\n
    //!   A*x + B*y + C*z + D \n
    //!   where x, y and z are coordinates of the vertex.
    double ToPlaneEquation(MCVertex * v)
    {
        return _A * v->GetX() + _B * v->GetY() + _C * v->GetZ() + _D;
    }

    //! Normalizes the normal vector of the plane.
    void NormalizeVector()
    {
        IsValid();
        double d = sqrt(_A * _A + _B * _B + _C * _C);
        if( d > 0 )
        {
            double inv = 1.0 / d;
            _A *= inv;
            _B *= inv;
            _C *= inv;
            _D *= inv;
        }
    }

protected:
	//! Equation coefficient.
    double _A, _B, _C, _D;

	//! Checks that at least one coefficient is non-zero.
    void IsValid()
    {
        assert(_A != 0.0 || _B != 0.0 || _C != 0.0);
    }

private:
    // ???
	MCVector3D edgeVector;
};

} // namespace

#endif
