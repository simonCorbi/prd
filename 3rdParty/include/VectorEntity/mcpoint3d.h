////////////////////////////////////////////////////////////
// $Id: mcpoint3d.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCPOINT3D_H
#define MCPOINT3D_H

////////////////////////////////////////////////////////////
// include files

// MDSTk
#include <MDSTk/Base/mdsSmallObject.h>

#include "vctlExport.h"
#include "mccoordinate3d.h"

// STL
#include <cmath>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Class encapsulates a point in 3D space.
 * - Just for backward compatibility!
 */
class VCTL_EXPORT MCPoint3D : public MCCoordinate3D<double>, public mds::base::CSmallObject<>
{
public:
    //! Base class.
    typedef MCCoordinate3D<double> tCoordinates;

public:
    //! Default constructor.
    //! - Initializes all coordinates to zero.
    MCPoint3D() {}

    //! Constructor that initializes point coordinates.
    MCPoint3D(const double& _x, const double& _y, const double& _z) : tCoordinates(_x, _y, _z) {}

    //! Constructor that initializes point coordinates.
    MCPoint3D(MCPoint3D * _point) : tCoordinates(*_point) { assert(_point); }

    //! Constructor that initializes point coordinates.
    MCPoint3D(const MCPoint3D& _point)
        : tCoordinates(_point)
        , mds::base::CSmallObject<>()
    {}

    //! Destructor.
    ~MCPoint3D() {}

    //! Assignment operator.
    MCPoint3D& operator =(const MCPoint3D& p)
    {
        this->x() = p.x();
        this->y() = p.y();
        this->z() = p.z();
        return *this;
    }


    //! Initializes coordinates of a given point.
    void GetPoint3D( MCPoint3D & _point ) const { _point.x() = x(); _point.y() = y(); _point.z() = z(); }

    //! Copies coordinates from a given point.
    void SetPoint3D( const MCPoint3D & _point ) { x() = _point.x(); y() = _point.y(); z() = _point.z(); }

    //! Returns sum of coordinates.
    double GetSumXYZ() const { return (x() + y() + z()); }

    //! Returns coordinates product.
    double GetMultXYZ() const { return (x() * y() * z()); }

    //! Returns Euclidean distance between this and a given point.
    double Distance( const MCPoint3D & b2 ) const
    {
        double dx = x() - b2.x();
        double dy = y() - b2.y();
        double dz = z() - b2.z();
        return (sqrt(dx * dx + dy * dy + dz * dz));
    }

    //! Returns distance between two points.
    static double Distance( const MCPoint3D & b1, const MCPoint3D & b2 ) { return b1.Distance(b2); }

    //! Returns square root of the distance between this and a given point.
    double SquareDistance( const MCPoint3D & b2 ) const
    {
        double dx = x() - b2.x();
        double dy = y() - b2.y();
        double dz = z() - b2.z();
        return (dx * dx + dy * dy + dz * dz);
    }

    //! Returns distance square root.
    static double SquareDistance( const MCPoint3D & b1, const MCPoint3D & b2 )
    {
        return b1.SquareDistance(b2);
    }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
