////////////////////////////////////////////////////////////
// $Id: mcedge.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCEDGE_H
#define MCEDGE_H

////////////////////////////////////////////////////////////
// include

#include "mcvertex.h"

// STL
#include <sstream>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Edge of a polygonal mesh.
 * - Derived from the MCEntity class.
 */
class VCTL_EXPORT MCEdge : public MCEntity<MCEdge>, public mds::base::CSmallObject<>
{
protected:
    //! Base class.
    typedef MCEntity<MCEdge> tBase;

    //! End-points of the edge.
    MCVertex * u[2];

    //! Pointers to edges sharing the same vertex.
    MCEdge * s[2];

public:
    //! Default constructor.
    MCEdge() { u[0] = u[1] = NULL; s[0] = s[1] = NULL; }

    //! Copy constructor.
    //! - Initializes pointers to vertices only!
    MCEdge(const MCEdge & _hrana)
        : mds::base::CSmallObject<>()
    {
        _hrana.GetVerticeS(u);
        s[0] = s[1] = NULL;
    }

    //! Constructor.
    //! - Initializes pointers to vertices only!
    MCEdge(MCEdge * _hrana) { assert(_hrana); _hrana->GetVerticeS(u); s[0] = s[1] = NULL; }

    //! Constructor.
    //! - Initializes pointers to vertices only!
    MCEdge(MCVertex * _u0, MCVertex * _u1)
    {
        assert(_u0 && _u1);
        u[0] = _u0; u[1] = _u1;
        s[0] = NULL; s[1] = NULL;
    }

    //! Constructor.
    //! - Initializes pointers to vertices only!
    MCEdge(MCVertex * _u[2])
    {
        assert(_u[0] && _u[1]);
        u[0] = _u[0]; u[1] = _u[1];
        s[0] = s[1] = NULL;
    }

    //! Empty destructor.
    ~MCEdge() {}


    //! Writes the edge data.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        assert(u[0] && u[1]);

        // write vertex indexes
        Writer.write(u[0]->GetIndex());
        Writer.write(u[1]->GetIndex());

        // serialize MCEntity data
        tBase::SerializeEntity(Writer);
    }

    //! Reads the edge data.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader, std::vector<MCVertex *> & index_array)
    {
        // read vertex indexes
        int index0, index1;
        Reader.read(index0);
        Reader.read(index1);

        assert(index0 < int(index_array.size()) && index0 >= 0);
        assert(index1 < int(index_array.size()) && index1 >= 0);

        // translation of indexes to pointers
        u[0] = index_array[index0];
        u[1] = index_array[index1];

        // deserialize MCEntity data
        tBase::DeserializeEntity(Reader);
    }


    //! Sets pointer to one of the edge vertices.
    //! @param index - index of the vertex (0 or 1).
    void SetVertex( int index, MCVertex * new_uzel )
    {
        assert((index >= 0) && (index < 2) && new_uzel);
        u[index] = new_uzel;
    }

    //! Sets the both edge end-points.
    void SetVerticeS( MCVertex * _u0, MCVertex * _u1 )      { assert(_u0 && _u1); u[0] = _u0; u[1] = _u1; }

    //! Sets both edge end-points.
    void SetVerticeS( MCVertex * _u[2] )                    { assert(_u[0] && _u[1]); u[0] = _u[0]; u[1] = _u[1]; }

    //! Returns pointer to a subscripted vertex.
    MCVertex * GetVertex( int index )                       { assert((index >= 0) && (index < 2)); return u[index]; }

    //! Returns pointers to edge vertices.
    void GetVerticeS( MCVertex ** _u0, MCVertex ** _u1 ) const
    {
        assert(_u0 && _u1);
        *_u0 = u[0]; *_u1 = u[1];
    }

    //! Returns pointers to edge vertices.
    void GetVerticeS( MCVertex * _u[2] ) const              { _u[0] = u[0]; _u[1] = u[1]; }

    //! Exchanges pointer to edge vertex.
    //!  @param old_uzel - pointer to the original vertex.
    //!  @param new_uzel - pointer to a new vertex.
    void ChangeVertex(MCVertex * old_uzel, MCVertex * new_uzel)
    {
        int old_index = IsVertex(old_uzel);
        assert(old_index != -1);
        assert(new_uzel);
        u[old_index] = new_uzel;
    }


    //! Checks if a given vertex belongs to the edge.
    //! @return index of the vertex within the edge, or -1 otherwise.
    int IsVertex( MCVertex * test_uzel ) const             { assert(test_uzel); return ( (test_uzel == u[0]) ? 0 : ((test_uzel == u[1]) ? 1 : -1) ); }

    //! Checks if a given vertex belongs to the edge.
    bool IsVertexBool( MCVertex * test_uzel ) const        { assert(test_uzel); return ((test_uzel == u[0]) || (test_uzel == u[1])); }

    //! Returns pointer to the opossite vertex of the edge.
    //! @param _uzel - pointer to a one of the edge vertices.
    //! @return NULL on failure.
    MCVertex * GetRestVertex( MCVertex * _uzel )           { assert(_uzel); return ((u[0] == _uzel) ? u[1] : ((u[1] == _uzel) ? u[0] : NULL)); }

    //! Returns geometrical center of the edge.
    void GetCenter( MCPoint3D & _stred )                   { _stred.SetXYZ( (u[0]->GetX()+u[1]->GetX())*0.5, (u[0]->GetY()+u[1]->GetY())*0.5, (u[0]->GetZ()+u[1]->GetZ())*0.5 ); }

    //! Returns length of the edge.
    double GetLength()                                     { return u[0]->Distance(*u[1]); }

    //! Divides the edge with respect to a specified ratio <0,1>.
    //! - Returns coordinates of calculated point.
    void GetEdgePoint(MCPoint3D & bod_h, double pomer)
    {
        bod_h.SetX(u[0]->GetX() + pomer*(u[1]->GetX() - u[0]->GetX()));
        bod_h.SetY(u[0]->GetY() + pomer*(u[1]->GetY() - u[0]->GetY()));
        bod_h.SetZ(u[0]->GetZ() + pomer*(u[1]->GetZ() - u[0]->GetZ()));
    }


    //! Registers the edge into its vertices.
    void Registration()
    {
        s[0] = u[0]->GetRegisteredEdge();
        s[1] = u[1]->GetRegisteredEdge();
        u[0]->SetRegisteredEdge(this);
        u[1]->SetRegisteredEdge(this);
    }

    //! Deregistration of the edge in its vertices.
    void DeRegistration();


    //! Checks identity of two edges.
    bool TestIdentity( MCEdge * test_edge ) const
    {
        assert(test_edge);
        return (IsVertexBool(test_edge->GetVertex(0)) && IsVertexBool(test_edge->GetVertex(1)));
    }

    //! Returns hash code of the edge.
    //! - Not yet implemented!
    //! @todo Doplnit implementaci generovani hash kodu.
    unsigned int GetHashCode() const                       { return 0; }

    //! Returns text description of the edge.
    virtual std::string ToString() const
    {
        std::ostringstream text_buff;
        text_buff << "Edge " << this << " : " << u[0] << " / " << u[1];
        return text_buff.str();
    }

    //! Returns pointer to a first edge registered in a specified vertex.
    //! - NULL on failure.
    MCEdge * GetVertexEdge( MCVertex * _uzel )             { int index = IsVertex(_uzel); return((index != -1) ? s[index] : NULL); }

    //! Sets an edge registered in a specified vertex.
    void SetVertexEdge(MCVertex * _uzel, MCEdge * _hrana)  { int index = IsVertex(_uzel); assert(index != -1); s[index] = _hrana; }
};


} // namespace vctl

#endif // MCEDGE_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

