////////////////////////////////////////////////////////////
// $Id: mctri.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCTRI_H
#define MCTRI_H

////////////////////////////////////////////////////////////
// include

#include "mcvertex.h"

// STL
#include <sstream>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Triangle in 3D space, component of a polygonal mesh.
 * - Derived from MCEntity class.
 */
class VCTL_EXPORT MCTri : public MCEntity<MCTri>, public mds::base::CSmallObject<>
{
protected:
    //! Base class.
    typedef MCEntity<MCTri> tBase;

    //! Pointers to all triangle vertices.
    MCVertex * u[3];

    //! Pointers to triangles sharing the corresponding vertex.
    MCTri * s[3];

public:
    //! Default constructor.
    //! - Initializes all pointers to NULL!
    MCTri() { u[0] = u[1] = u[2] = NULL; s[0] = s[1] = s[2] = NULL; }

    //! Copy constructor.
    //! - Initializes only pointers to triangle vertices.
    MCTri(const MCTri& _tri) : mds::base::CSmallObject<>()
    {
        u[0] = _tri.u[0];
        u[1] = _tri.u[1];
        u[2] = _tri.u[2];
        s[0] = s[1] = s[2] = NULL;
    }

    //! Constructor.
    //! - Initializes only pointers to triangle vertices.
    MCTri(MCTri * _tri)
    {
        assert(_tri);
        u[0] = _tri->u[0];
        u[1] = _tri->u[1];
        u[2] = _tri->u[2];
        s[0] = s[1] = s[2] = NULL;
    }

    //! Constructor.
    //! - Initializes only pointers to triangle vertices.
    MCTri(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2)
    {
        assert(_u0 && _u1 && _u2);
        u[0] = _u0;
        u[1] = _u1;
        u[2] = _u2;
        s[0] = s[1] = s[2] = NULL;
    }

    //! Constructor.
    //! - Initializes only pointers to triangle vertices.
    MCTri(MCVertex * _u[3])
    {
        assert(_u[0] && _u[1] && _u[2]);
        u[0] = _u[0];
        u[1] = _u[1];
        u[2] = _u[2];
        s[0] = s[1] = s[2] = NULL;
    }

    //! Empty destructor.
    virtual ~MCTri() {}


    //! Loads triangle (indexes of vertices) from a given stream.
    //! - An array of vertices must be loaded previously.
    void Load(std::ifstream & proud, std::vector<MCVertex *> & _pole);

    //! Saves triangle (indexes of vertices) into a stream.
    //! - An index must be assigned to each vertex.
    void Save(std::ofstream & proud);


    //! Serializes the triangle.
    //! - An index must be assigned to each vertex.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        assert(u[0] && u[1] && u[2]);

        // write vertex indexes into
        Writer.write(u[0]->GetIndex());
        Writer.write(u[1]->GetIndex());
        Writer.write(u[2]->GetIndex());

        // serialize MCEntity data
        MCEntity<MCTri>::SerializeEntity(Writer);
    }

    //! Deserializes the triangle.
    //! - An array of vertices must be loaded previously.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader, std::vector<MCVertex *> & index_array)
    {
        // read vertices index from given channel
        int index0, index1, index2;
        Reader.read(index0);
        Reader.read(index1);
        Reader.read(index2);

        assert((index0 < (int)index_array.size()) && (index0 >= 0));
        assert((index1 < (int)index_array.size()) && (index1 >= 0));
        assert((index2 < (int)index_array.size()) && (index2 >= 0));

        // translation from vertices index on their pointers
        u[0] = index_array[index0];
        u[1] = index_array[index1];
        u[2] = index_array[index2];

        // deserialize MCEntity data
        tBase::DeserializeEntity(Reader);
    }


    //! Sets pointer to one of the vertices.
    void SetVertex( int index, MCVertex * new_uzel )       { assert((index >= 0) && (index < 3) && new_uzel); u[index] = new_uzel; }

    //! Sets pointers to all triangle vertices.
    void SetVerticeS( MCVertex * _u0, MCVertex * _u1, MCVertex * _u2 ) { assert(_u0 && _u1 && _u2); u[0] = _u0; u[1] = _u1; u[2] = _u2; }

    //! Sets pointers to all triangle vertices.
    void SetVerticeS( MCVertex * _u[3] )                    { assert(_u[0] && _u[1] && _u[2]); u[0] = _u[0]; u[1] = _u[1]; u[2] = _u[2]; }

    //! Return pointer to a specified vertex.
    MCVertex * GetVertex( int index )                       { assert((index >= 0) && (index < 3)); return u[index]; }

    //! Returns pointer to all triangle vertices.
    void GetVerticeS( MCVertex ** _u0, MCVertex ** _u1, MCVertex ** _u2 ) const
    {
        assert(_u0 && _u1 && _u2);
        *_u0 = u[0]; *_u1 = u[1]; *_u2 = u[2];
    }

    //! Returns pointer to all triangle vertices.
    void GetVerticeS( MCVertex * _u[3] ) const              { _u[0] = u[0]; _u[1] = u[1]; _u[2] = u[2]; }


    //! Exchanges one of the triangle vertices.
    //! - This method doesn't changes registration of tringles in vertices.
    //!   You must do the registration on your own.
    void ChangeVertex(MCVertex * old_uzel, MCVertex * new_uzel)
    {
        int old_index = IsVertex(old_uzel);
        assert(old_index != -1);
        assert(new_uzel);
        u[old_index] = new_uzel;
    }

    //! Checks if a given vertex belongs to the triangle.
    //! @return index of the vertex within the triangle, or -1 otherwise.
    int IsVertex( MCVertex * test_uzel ) const              { assert(test_uzel); return ((test_uzel == u[0]) ? 0 : ((test_uzel == u[1]) ? 1 : ((test_uzel == u[2]) ? 2 : -1)) ); }

    //! Checks if a given vertex belongs to the triangle.
    bool IsVertexBool( MCVertex * test_uzel ) const        { assert(test_uzel); return ((test_uzel == u[0]) || (test_uzel == u[1]) || (test_uzel == u[2])); }


    //! Returns a remaining vertex, or NULL.
    MCVertex * GetRestVertex(MCVertex * _u0, MCVertex * _u1);

    //! Returns two remaining vertices.
    void GetRestEdge(MCVertex * _uzel, MCVertex ** _u0, MCVertex ** _u1);


    //! Returns length and vertices of the longest edge.
    double GetMaxEdge( MCVertex ** _u0, MCVertex ** _u1 );

    //! Returns pointer to a longest edge, or NULL on failure.
    double GetMaxEdge( MCEdge ** max_hrana );

    //! Returns index of the longest edge.
    int GetMaxEdge();

    //! Returns length of the longest edge.
    double GetMaxEdgeLength();

    //! Returns length of the shortest edge.
    double GetMinEdgeLength();


    //! Returns all triangle edges, or NULL if no edges exist.
    void GetEdges( MCEdge * hrany[3] );

    //! Returns a specified triangle edge, or NULL if no such edge exists.
    MCEdge * GetEdge( int hrana );

    //! Returns pointers to all triangle vertices.
    void GetEdge(int index, MCVertex ** _u0, MCVertex ** _u1);

    //! Returns perimeter of the triangle.
    double GetCircuit();

    //! Returns area of the triangle.
    double GetArea();

    //! Returns coordinates of the triangle's center.
    void GetCenter(MCPoint3D & _center);


    //! Returns normal of the triangle normalized so that its length is equal to one.
    MCVector3D GetNormal();

    //! Returns normal of the triangle normalized so that its length is equal to one.
    void GetNormal(MCVector3D & normala);

    //! Returns parameters of the triangle's plane.
    void GetPlane( double & A, double & B, double & C, double & D );

    //! Returns quality of the triangle.
    double GetQuality()
    {
        double plocha = GetArea();
        if( plocha == 0 ) return 1000000;
        return ((SQRT_3 * GetMaxEdgeLength() * GetCircuit()) / (12.0 * plocha));
    }


    //! Returns pointers to neighbouring triangles.
    void GetNeighbours( MCTri * _pole[3] );

    //! Returns pointer to a specific triangle.
    MCTri * GetNeighbour( int _index );


    //! Changes orientation of the triangle.
    void InverseOrientation();

    //! Compares orientation of two triangles.
    //! - 1 - same orientation of triangles.
    //! - 0 - perpendicular triangles.
    //! - -1 - reverse orientation.
    int CompareTriOrientation(MCTri * test_tri);


    //! Registers the triangle into its vertices.
    void Registration()
    {
        s[0] = u[0]->GetRegisteredTri();
        s[1] = u[1]->GetRegisteredTri();
        s[2] = u[2]->GetRegisteredTri();
        u[0]->SetRegisteredTri(this);
        u[1]->SetRegisteredTri(this);
        u[2]->SetRegisteredTri(this);
    }

    //! Deregistration of triangle from vertices.
    void DeRegistration();


    //! Checks of two triangles are equal.
    bool TestIdentity( MCTri * test_tri ) const
    {
        assert(test_tri);
        return (test_tri->IsVertexBool(u[0]) && test_tri->IsVertexBool(u[1]) && test_tri->IsVertexBool(u[2]));
    }

    //! Returns triangle hash code.
    //! - Not yet implemented!
    //! @todo Doplnit implementaci generovani hash kodu.
    unsigned int GetHashCode() const                        { return 0; }

    //! Returns text description of the triangle.
    virtual std::string ToString() const
    {
        std::ostringstream text_buff;
        text_buff << "Tri " << this << " : " << u[0] << " / " << u[1] << " / " << u[2];
        return text_buff.str();
    }

    //! Returns a registered triangle in a given vertex.
    //! - NULL on failure.
    MCTri * GetVertexTri( MCVertex * _uzel )
    {
        int index = IsVertex(_uzel);
        return ((index != -1) ? s[index] : NULL);
    }

    //! Sets pointer to a triangle registered in a given vertex.
    void SetVertexTri(MCVertex * _uzel, MCTri * _tri)
    {
        int index = IsVertex(_uzel);
        assert(index != -1);
        s[index] = _tri;
    }
};

} // namespace vctl

#endif // MCTRI_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

