//==============================================================================
/*! \file
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2010/08/29                          \n
 *
 * $Id: vctlExport.h 1869 2010-09-02 22:54:56Z spanel $
 *
 * Description:
 * - Dynamic library export settings.
 */

#ifndef VCTL_Export_H
#define VCTL_Export_H


//=============================================================================
/*
 * Setup export settings.
 * - MDS_LIBRARY_STATIC ~ using static library
 * - MDS_LIBRARY_SHARED + MDS_MAKING_VCTL_LIBRARY ~ making dynamic library
 * - MDS_LIBRARY_SHARED ~ using dynamic library
 */

#ifdef _WIN32
#   if defined( MDS_LIBRARY_SHARED )
#       ifdef MDS_MAKING_VCTL_LIBRARY
#           define VCTL_EXPORT __declspec(dllexport)
#       else
#           define VCTL_EXPORT __declspec(dllimport)
#       endif
#   else
#       define VCTL_EXPORT
#   endif
#else
#   define VCTL_EXPORT
#endif


#endif // VCTL_Export_H

