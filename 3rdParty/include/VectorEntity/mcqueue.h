////////////////////////////////////////////////////////////
// $Id: mcqueue.h 2065 2012-02-02 23:29:38Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCQUEUE_H
#define MCQUEUE_H

////////////////////////////////////////////////////////////
// include

#include <MDSTk/Base/mdsSetup.h>

// STL
#include <map>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Sorting queue of entities.
 * - Encapsulates STL multimap.
 * - VALUE_TYPE must be derived from MCEntity.
 */
template <typename KEY_TYPE, typename VALUE_TYPE>
class MCQueue
{
public:
    //! Tree implementation of the queue.
    typedef std::multimap<KEY_TYPE, VALUE_TYPE *> MT_TYPE;

    //! Reverse iterator (see STL).
    typedef typename MT_TYPE::reverse_iterator MT_RITER;

    //! Iterator (see STL).
    typedef typename MT_TYPE::iterator MT_ITER;

private:
    //! Tree representing the queue.
    MT_TYPE strom;

public:
    //! Default constructor.
    MCQueue() {}

    //! Destructor
    ~MCQueue() {}

    //! Inserts a new entity to the queue.
    void Sort( KEY_TYPE klic, VALUE_TYPE * entita )
    {
        strom.insert(std::make_pair(klic, entita));
    }

    //! Removes a given entity from the queue.
    void UnSort( KEY_TYPE klic, VALUE_TYPE * entita )
    {
        MT_ITER it;

        // test, jestli je strom prazdny
        if( !strom.empty() )
        {
            // nalezeni iteratoru pro mazanou entitu
            if( (it = GetIterPtr( klic, entita )) == strom.end() )
            {
                throw std::logic_error("Chyba mazani entity podle klice z fronty, neni ve fronte");
            }
            // zruseni entity z fronty
            strom.erase(it);
        }
    }

    //! Returns pointer to an entity having the minimal key,
    //! or NULL if the queue is empty.
    VALUE_TYPE * GetMin()
    {
        MT_ITER it = strom.begin();       // iterator prvni polozky fronty

        // kontrola prazdne fronty
        if( it != strom.end() )
        {
            // kontrola existence ukazatele na prvni polozku
            assert(it->second != NULL);
            // vraci prvni entitu fronty, s nejmensim klicem
            return (it->second);
        }

        return NULL;
    }

    //! Give pointers to entities having the first two minimal key,
    //! or NULL if the queue is empty.
    void GetMinFirstTwo(VALUE_TYPE ** first_min, VALUE_TYPE ** second_min)
    {
        MT_ITER it = strom.begin();       // iterator prvni polozky fronty

        // init return values
        *first_min = NULL;
        *second_min = NULL;
        
        // test tree end
        if (it != strom.end())
        {
            // save first min value
            assert(it->second != NULL);
            *first_min = it->second;

            // move iterator on next min entity
            it++;

            // test tree end
            if( it != strom.end() )
            {
                // save second min value
                assert(it->second != NULL);
                *second_min = it->second;
            }
        }
    }

    //! Returns pointer to an entity having the maximal key,
    //! or NULL if the queue is empty.
    VALUE_TYPE * GetMax()
    {
        MT_RITER it = strom.rbegin();  // iterator na posledni polozku

        // kontrola rezimu trideni a prazdne fronty
        if( it != strom.rend() )
        {
            // kontrola existence ukazatele na posledni polozku
            assert(it->second != NULL);
            // vraci posledni entitu fronty, s nejvetsim klicem
            return (it->second);
        }

        return NULL;
    }

    //! Returns iterator to an entity having the minimal key value.
    MT_ITER GetMinIter() { return strom.begin(); }

    //! Returns iterator after the last element in the queue.
    MT_ITER GeEndIter() { return strom.end(); }

    //! Returns iterator to an entity having the maximal key value.
    MT_RITER GetMaxIter() { return strom.rbegin(); }

    //! Returns true if a given iterator points after the last item in the queue.
    bool TestEndIter(MT_ITER _it) { return (_it == strom.end()); }

    //! Returns true if a given iterator points after the last item in the queue.
    bool TestEndIter(MT_RITER _it) { return (_it == strom.rend()); }


    //! Clears the queue.
    //! - No objects are deleted!
    void Clear() { strom.clear(); }

    //! Checks if there is an identical entity in the queue.
    //! - Returns NULL if no such entity was found.
    VALUE_TYPE * Exist( KEY_TYPE klic, VALUE_TYPE * entita )
    {
        MT_ITER it;           // iterator na totoznou polozku

        // hledani iteratoru dane entity podle totoznosti
        if( (it = GetIterIdentity(klic, entita)) != strom.end() )
        {
            // kontrola existence ukazatele na polozku
            assert(it->second != NULL);
            // vraceni ukazatele na nalezenou polozku
            return it->second;
        }

        return NULL;
    }

private:
    //! Returns iterator to an entity identical with the given one.
    //! - See the MCEntity::TestIdentity() method.
    MT_ITER GetIterIdentity( KEY_TYPE klic, VALUE_TYPE * entita )
    {
        // rozsah iteratoru pro dany klic
        std::pair<MT_ITER, MT_ITER> rozsah_it;

        // ziskani rozsahu it pro dany klic
        rozsah_it = strom.equal_range(klic);

        // cyklus iteratoru pro dany klic
        for( MT_ITER it = rozsah_it.first; it != rozsah_it.second; ++it )
        {
            // kontrola existence ukazatele na polozku
            assert(it->second != NULL);
            // kontrola nalezeneho iter, jestli obsahuje totoznou entitu
            if( it->second->TestIdentity(entita) )
                return it;      // nalezeny iter obsahuje hledanou totoznou entitu
        }

        // vraceni koncoveho iter, jako signal neuspesnosti hledani
        return strom.end();
    };

    //! Returns iterator to an entity in the queue comparing entity pointers.
    MT_ITER GetIterPtr( KEY_TYPE klic, VALUE_TYPE * entita )
    {
        // rozsah iteratoru pro dany klic
        std::pair<MT_ITER, MT_ITER> rozsah_it;

        // ziskani rozsahu it pro dany klic
        rozsah_it = strom.equal_range(klic);

        // cyklus iteratoru pro dany klic
        for( MT_ITER it = rozsah_it.first; it != rozsah_it.second; ++it )
        {
            // kontrola nalezeneho iter, jestli obsahuje entita
            if( it->second == entita )
                return it;      // nalezeny iter obsahuje hledanou entitu
        }

        // vraceni koncoveho iter, jako signal neuspesnosti hledani
        return strom.end();
    }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
