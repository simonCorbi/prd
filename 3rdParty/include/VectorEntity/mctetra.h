////////////////////////////////////////////////////////////
// $Id: mctetra.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCTETRA_H
#define MCTETRA_H

////////////////////////////////////////////////////////////
// include

#include "mcvertex.h"

// STL
#include <sstream>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Class representing a tetrahedron.
 * - Derived from MCEntity class.
 */
class VCTL_EXPORT MCTetra: public MCEntity<MCTetra>, public mds::base::CSmallObject<>
{
protected:
    //! Base class.
    typedef MCEntity<MCTetra> tBase;

    //! Pointers to all tetrahedron vertices.
    MCVertex * u[4];

    //! Pointers to tetrahedra sharing the same vertices.
    MCTetra * s[4];

    //! Direct pointers to neighbouring tetrahedra.
    MCTetra * n[4];

    //! Value flag for signing the tetrahedron.
    unsigned int value_flag;

public:
    //! Default constructor.
    //! - Initializes all pointers to NULL.
    MCTetra() : value_flag(0)
    {
        u[0] = u[1] = u[2] = u[3] = NULL;
        s[0] = s[1] = s[2] = s[3] = NULL;
        n[0] = n[1] = n[2] = n[3] = NULL;
    }

    //! Copy constructor.
    //! - Initializes pointers to tetrahedron vertices only.
    MCTetra(const MCTetra& _tet)
        : mds::base::CSmallObject<>()
        , value_flag(0)
   {
        _tet.GetVerticeS(u);
        s[0] = s[1] = s[2] = s[3] = NULL;
        n[0] = n[1] = n[2] = n[3] = NULL;
    }

    //! Constructor.
    //! - Initializes pointers to tetrahedron vertices only.
    MCTetra(MCTetra * _tet) : value_flag(0)
    {
        assert(_tet);
        _tet->GetVerticeS(u);
        s[0] = s[1] = s[2] = s[3] = NULL;
        n[0] = n[1] = n[2] = n[3] = NULL;
    }

    //! Constructor.
    //! - Initializes pointers to tetrahedron vertices only.
    MCTetra(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2, MCVertex * _u3) : value_flag(0)
    {
        SetVerticeS(_u0, _u1, _u2, _u3);
        s[0] = s[1] = s[2] = s[3] = NULL;
        n[0] = n[1] = n[2] = n[3] = NULL;
    }

    //! Constructor.
    //! - Initializes pointers to tetrahedron vertices only.
    MCTetra(MCVertex * _u[4]) : value_flag(0)
    {
        assert(_u[0] && _u[1] && _u[2] && _u[3]);
        u[0] = _u[0];
        u[1] = _u[1];
        u[2] = _u[2];
        u[3] = _u[3];
        s[0] = s[1] = s[2] = s[3] = NULL;
        n[0] = n[1] = n[2] = n[3] = NULL;
    }

    //! Virtual destructor.
    virtual ~MCTetra() {}


    //! Returns the value flag.
    unsigned int GetValueFlag()                                           { return value_flag; }

    //! Sets the value flag.
    void SetValueFlag(unsigned int _value)                                { value_flag = _value; }


    //! Writes the tetrahedron data.
    //! - An unique index must be assigned to each vertex first.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        assert(u[0] && u[1] && u[2] && u[3]);

        // write indexes of vertices
        Writer.write(u[0]->GetIndex());
        Writer.write(u[1]->GetIndex());
        Writer.write(u[2]->GetIndex());
        Writer.write(u[3]->GetIndex());

        // serialize MCEntity data
        tBase::SerializeEntity(Writer);
    }

    //! Reads the tetra data.
    //! - An array of vertices must be loaded previously.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader, std::vector<MCVertex *> & index_array)
    {
        // read vertices index from given channel
        int index0, index1, index2, index3;
        Reader.read(index0);
        Reader.read(index1);
        Reader.read(index2);
        Reader.read(index3);

        assert(index0 < (int)index_array.size() && index0 >= 0);
        assert(index1 < (int)index_array.size() && index1 >= 0);
        assert(index2 < (int)index_array.size() && index2 >= 0);
        assert(index3 < (int)index_array.size() && index3 >= 0);

        // translation of index to pointer
        u[0] = index_array[index0];
        u[1] = index_array[index1];
        u[2] = index_array[index2];
        u[3] = index_array[index3];

        // deserialize MCEntity data
        tBase::DeserializeEntity(Reader);
    }


    //! Sets pointer to a specified vertex.
    void SetVertex( int index, MCVertex * new_uzel )       { assert((index >= 0) && (index < 4) && new_uzel); u[index] = new_uzel; }

    //! Sets pointers to all tetrahedron vertices.
    void SetVerticeS( MCVertex * _u0, MCVertex * _u1, MCVertex * _u2, MCVertex * _u3 )
    {
        assert(_u0 && _u1 && _u2 && _u3);
        u[0] = _u0; u[1] = _u1; u[2] = _u2; u[3] = _u3;
    }

    //! Sets pointers to all tetrahedron vertices.
    void SetVerticeS( MCVertex * _u[4] )
    {
        assert(_u[0] && _u[1] && _u[2] && _u[3]);
        u[0] = _u[0]; u[1] = _u[1]; u[2] = _u[2]; u[3] = _u[3];
    }

    //! Returns pointer to a specified vertex.
    MCVertex * GetVertex( int index )                      { assert((index >= 0) && (index < 4)); return u[index]; }

    //! Returns pointers to all tetrahedron vertices.
    void GetVerticeS( MCVertex ** _u0, MCVertex ** _u1, MCVertex ** _u2, MCVertex ** _u3 ) const
    {
        assert(_u0 && _u1 && _u2 && _u3);
        *_u0 = u[0]; *_u1 = u[1]; *_u2 = u[2]; *_u3 = u[3];
    }

    //! Returns pointers to all tetrahedron vertices.
    void GetVerticeS( MCVertex * _u[4] ) const             { _u[0] = u[0]; _u[1] = u[1]; _u[2] = u[2]; _u[3] = u[3]; }


    //! Returns pointer to the remaining vertex.
    MCVertex * GetRestVertex(MCVertex * _u0, MCVertex * _u1, MCVertex * _u2);

    //! Returns pointer to the vertex opposing a specified face.
    MCVertex * GetRestVertex( int index );


    //! Exchanges a specified vertex with a new one.
    //! - The method doesn't provide any registration/deregistration of tetrahedron in vertices.
    //!   You must do this on your own!
    void ChangeVertex(MCVertex * old_uzel, MCVertex * new_uzel)
    {
        int old_index = IsVertex(old_uzel);
        assert(old_index != -1);
        assert(new_uzel);
        u[old_index] = new_uzel;
    }


    //! Returns index of a specified vertex comparing pointers, or -1 if no such vertex was found.
    int IsVertex( MCVertex * test_uzel ) const
    {
        assert(test_uzel);
        return ((test_uzel == u[0]) ? 0 : ((test_uzel == u[1]) ? 1 : ((test_uzel == u[2]) ? 2: ((test_uzel == u[3]) ? 3 : -1))));
    }

    //! Checks if a given vertex belongs to the tetrahedra.
    bool IsVertexBool( MCVertex * test_uzel ) const
    {
        assert(test_uzel);
        return ((test_uzel == u[0]) || (test_uzel == u[1]) || (test_uzel == u[2]) || (test_uzel == u[3]));
    }

    //! Checks if a given triangle belongs to the tetrahedra.
    bool IsTriBool( MCVertex * _u0, MCVertex * _u1, MCVertex * _u2 )
    {
        return (IsVertexBool(_u0) && IsVertexBool(_u1) && IsVertexBool(_u2));
    }

    //! Checks if a given triangle belongs to the tetrahedra.
    bool IsTriBool( MCVertex * _u[3] )                     { return IsTriBool(_u[0], _u[1], _u[2]); }


    //! Returns langth of the longest edge.
    double GetMaxEdgeLength();

    //! Returns langth of the shortest edge.
    double GetMinEdgeLength();

    //! Returns the longest edge.
    void GetMaxEdge( MCVertex ** _u0, MCVertex ** _u1 );

    //! Returns the shortest edge.
    void GetMinEdge( MCVertex ** _u0, MCVertex ** _u1 );


    //! Returns pointers to all vertices of a specified tetrahedron face.
    void GetTri( int index, MCVertex ** _u0, MCVertex ** _u1, MCVertex ** _u2 ) const;

    //! Returns pointers to all vertices of a specified tetrahedron face.
    void GetTri( int index, MCVertex * _u[3] ) const { GetTri(index, _u, _u + 1, _u + 2); }

    //! Returns indexes of vertices of a tetrahedron face.
    void GetTri( int index, int * _u0, int * _u1, int * _u2 ) const;

    //! Returns indexes of vertices of a tetrahedron face.
    void GetTri( int index, int _u[3] ) const { GetTri(index, _u, _u + 1, _u + 2); }


    //! Returns pointers to vertices opposing a specified vertex.
    void GetRestTri( int index, MCVertex ** _u0, MCVertex ** _u1, MCVertex ** _u2 ) const;

    //! Returns pointers to vertices opposing a specified vertex.
    void GetRestTri( int index, MCVertex * _u[3] ) const { GetRestTri(index, _u, _u + 1, _u + 2); }


    //! Returns tetrahedron surface area.
    double GetArea();

    //! Returns tetrahedron volume.
    double GetVolume();

    //! Returns coordinates of tetrahedron center.
    void GetCenter(MCPoint3D & _center);

    //! Returns normal of a specified tetrahedron face.
    //! - The returned vector is normalized.
    MCVector3D GetNormal( int index ) const;

    //! Returns normal of a specified tetrahedron face.
    //! - The returned vector is not normalized.
    MCVector3D GetNormal2( int index ) const;

    //! Returns normal of a specified tetrahedron face.
    //! - The returned vector is normalized.
    void GetNormal( int index, MCVector3D & normal ) const;

    //! Returns normal of a specified tetrahedron face.
    //! - The returned vector is not normalized.
    void GetNormal2( int index, MCVector3D & normal ) const;


    //! Returns neighbouring tetrahedra.
    //! - Traversing all tetrahedra registered in the tetrahedron vertices.
    void GetNeighboursByVertices( MCTetra * _pole[4] );

    //! Returns a specified neighbouring tetrahedron.
    //! - Traversing all tetrahedra registered in the tetrahedron vertices.
    MCTetra * GetNeighbourByVertices( int _index );

    //! Returns neighbouring tetrahedra.
    //! - Directly via internal pointers.
    void GetNeighbours( MCTetra * _n[4] )    { _n[0] = n[0]; _n[1] = n[1]; _n[2] = n[2]; _n[3] = n[3]; }

    //! Returns pointers to neighbouring tetrahedra.
    //! - Directly via internal pointers.
    const MCTetra ** GetNeighbours() const   { return const_cast<const MCTetra **>(n); }

    //! Returns a specified neighbouring tetrahedron.
    //! - Directly via internal pointers.
    MCTetra * GetNeighbour( int _index )     { assert((_index >= 0) && (_index < 4)); return n[_index]; }


    //! Inverses orientation of the tetrahedron.
    void InverseOrientation();

    //! Registeres the tetrahedron in all its vertices.
    void Registration()
    {
        for (int i = 0; i < 4; i++)
        {
            s[i] = u[i]->GetRegisteredTetra();
            u[i]->SetRegisteredTetra(this);
        }
        NeighboursRegistration();
    }

    //! Deregisteres the tetrahedron from its vertices.
    void DeRegistration();


    //! Checks identity of two tetrahedra.
    bool TestIdentity( MCTetra * test_tetra ) const
    {
        assert(test_tetra);
        return ((test_tetra->IsVertexBool(u[0])) && (test_tetra->IsVertexBool(u[1])) && (test_tetra->IsVertexBool(u[2])) && (test_tetra->IsVertexBool(u[3])));
    }

    //! Returns the tetrahedron hash code.
    //! - Not yet implemented!
    //! @todo Doplnit implementaci generovani hash kodu.
    unsigned int GetHashCode() const                       { return 0; };

    //! Returns text description of the tetrahedra.
    virtual std::string ToString() const
    {
        std::ostringstream text_buff;
        text_buff << "Tetra " << this << " : " << u[0] << " / " << u[1] << " / " << u[2] << " / " << u[3];
        return text_buff.str();
    }

    //! Returns a tetrahedron registered in a specified tetrahedron vertex (sharing the same vertex).
    //! - NULL on failure.
    MCTetra * GetVertexTetra( MCVertex * _uzel )           { int index = IsVertex(_uzel); return((index != -1) ? s[index] : NULL); }

    //! Sets pointer to a tetrahedron registered in a specified tetrahedron vertex (sharing the same vertex).
    //! - NULL on failure.
    void SetVertexTetra(MCVertex * _uzel, MCTetra * _tet)  { int index = IsVertex(_uzel); assert(index != -1); s[index] = _tet; }

protected:
    //! Tetrahedron neighbours registration.
    //! - Actualizes all neighbours and also neighbours of all newly find neighbours.
    void NeighboursRegistration();

    //! Tetrahedron neighbours deregistration.
    //! - Actualizes all neighbours of its actual neighbours.
    //! - The function expects, that the tetra is deregistered from his vertices
    //!   by using the function DeRegistration() first.
    void NeighboursDeRegistration();

    //! Tetrahedra neighbours actualization.
    //! - Only for this tetra.
    void NeighboursActualization() { GetNeighboursByVertices(n); }
};


} // namespace vctl

#endif // MCTETRA_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

