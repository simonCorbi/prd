////////////////////////////////////////////////////////////
// $Id: mclistnode.h 2065 2012-02-02 23:29:38Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCLISTNODE_H
#define MCLISTNODE_H

////////////////////////////////////////////////////////////
// include

#include <MDSTk/Base/mdsSetup.h>

#include <cassert>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * An element of a double-linked list.
 * - See the MCList class for details.
 */
template <typename NODE_TYPE>
class MCListNode
{
public:
    //! Value type.
    typedef NODE_TYPE MT_VALUE;

private:
    //! Pointer to the previous node in the list.
    NODE_TYPE * prev_node;

    //! Pointer to the next node in the list.
    NODE_TYPE * next_node;

public:
    //! Default constructor.
    MCListNode() : prev_node(NULL), next_node(NULL) {}

    //! An empty non-virtual destructor!
    ~MCListNode() {}

    //! Returns pointer to the previous node in the list,
    //! or NULL in case the list is empty.
    NODE_TYPE * GetPrev() { return prev_node; }

    //! Returns pointer to the next node in the list,
    //! or NULL in case the list is empty.
    NODE_TYPE * GetNext() { return next_node; }

    //! Sets pointer to previous list node.
    void SetPrev( NODE_TYPE * _node ) { prev_node = _node; }

    //! Sets pointer to the next node in the list.
    void SetNext( NODE_TYPE * _node ) { next_node = _node; }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
