////////////////////////////////////////////////////////////
// $Id: mcentity.h 2095 2012-02-16 02:05:45Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCENTITY_H
#define MCENTITY_H

////////////////////////////////////////////////////////////
// include

// MDSTk
#include <MDSTk/Base/mdsSmallObject.h>
#include <MDSTk/Module/mdsSerializer.h>
#include <MDSTk/Math/mdsBase.h>

#include "mclistnode.h"
#include "mcpoint3d.h"
#include "mcvector3d.h"

// STL
#include <cmath>
#include <cassert>
#include <string>


////////////////////////////////////////////////////////////
// macro definitions

//! Maximal required precision. The value affects comparison of vertex coordinates, etc.
#define         DOUBLE_MAX                 1000000000
//#define         DOUBLE_MAX                 1000

//! Minimal required precision. The value affects comparison of vertex coordinates, etc.
#define         DOUBLE_MIN                 0.000000001
//#define         DOUBLE_MIN                 0.001

//! A predefined constant.
#define         SQRT_3                    1.73205080756887729353


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

//! Basic flags used to mark an entity.
enum MEEntityFlags
{
    NO_FLAG             = 0,
    ENTITY_MARK_FLAG    = 1,
    VERTEX_CORNER_FLAG  = 2,
    VERTEX_EDGE_FLAG    = 4
};


////////////////////////////////////////////////////////////
// forward declarations

class MCVertex;
class MCEdge;
class MCTri;
class MCTetra;

////////////////////////////////////////////////////////////
/*!
 * Base class for all vector entities like edges, etc.
 * - Derived from the MCListNode class.
 */
template <typename ENTITY_TYPE>
class MCEntity : public MCListNode<ENTITY_TYPE>
{
protected:
    //! Internal entity flags.
    int flag;

    //! Entity number.
    int object;

    //! A user specific value attribute of the entity.
    double value;

    //! A user specific pointer attribute of the entity.
    //! - You should manage the underlying memory on your own.
    //! - Please, remember to clear the pointer before destructor is called.
    void * value_ptr;

public:
    //! Default constructor.
    MCEntity() : flag(NO_FLAG), object(-1), value(0), value_ptr(NULL) {}

    //! Virtual destructor.
    virtual ~MCEntity() { assert(value_ptr == NULL); }


    //! Serialization of entity attributes.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        // write entity flag
        Writer.write(flag);

        // write entity object index
        Writer.write(object);
    }

    //! Deserializes the entity attributes.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader)
    {
        // read entity object flag
        Reader.read(flag);

        // read entity object index
        Reader.read(object);
    }


    //! Returns the object number.
    int GetObject() const { return object; }

    //! Sets the object number.
    void SetObject(int _value) { object = _value; }

    //! Sets the value attribute.
    void SetValue( const double& _value ) { value = _value; }

    //! Returns the current value attribute.
    double& GetValue() { return value; }
    const double& GetValue() const { return value; }

    //! Sets the pointer attribute.
    void SetValuePtr( void *  _ptr ) { value_ptr = _ptr; }

    //! Returns the current value of the pointer attribute.
    void * GetValuePtr() const { return value_ptr; }

    //! Adds a given flag using the binary or operation.
    void SetFlag( int set_flag ) { flag |= set_flag; }

    //! Returns the current value of internal flags.
    int GetFlag() { return flag; }

    //! Changes the internal flags completely.
    void ReplaceFlag( int new_flag ) { flag = new_flag; }

    //! Clears all bits specified in the mask.
    void MaskFlag( int mask_flag ) { flag &= (0xffff - mask_flag); }

    //! Checks if a given flag is present.
    bool TestFlag( int test_flag ) const { return ((flag & test_flag) != 0); }

    //! Copies all entity attributes.
    void SetEntityAttributes(MCEntity<ENTITY_TYPE> *entity)
    {
        assert(entity);
        flag = entity->flag;
        object = entity->object;
        value = entity->value;
    }


    //! Compares two entities and returns true if they are equal.
    virtual bool TestIdentity( ENTITY_TYPE * test_entity ) const = 0;

    //! Calculates hash code identifieng the entity.
    virtual unsigned int GetHashCode() const = 0;

    //! Returns textual representation of all entity attributes.
    virtual std::string ToString() const = 0;

    //! Returns a simple hash code of a given block of bytes.
    static unsigned int MakeStringHashCode(const char * hash_string, int hash_string_size)
    {
        static const unsigned int       hash_shift = 6;                           // hodnota posunu pro vypocet hash kodu
        static const unsigned int       hash_mask = ~0U << (32U - hash_shift);    // maska pro vypocet hash kodu
        unsigned int                    hash_code = 0;                            // vysledny hash kod

        assert(hash_string != NULL);

        // cyklus byte daneho stringu pro vypocet hash kodu
        for (int i = 0; i < hash_string_size; ++i)
            hash_code = (hash_code & hash_mask) ^ (hash_code << hash_shift) ^ hash_string[i];

        return hash_code;
    }
};


} // namespace vctl

#endif // MCENTITY_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

