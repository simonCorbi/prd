////////////////////////////////////////////////////////////
// $Id: mcedges.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCEDGES_H
#define MCEDGES_H

////////////////////////////////////////////////////////////
// includes

// MDSTk
#include <MDSTk/Module/mdsSerializer.h>

#include "mcvertices.h"
#include "mcedge.h"

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * A container of edges in 3D space.
 * - Derived from the general container of entities.
 */
class VCTL_EXPORT MCEdgeS : public MCEntitieS<MCEdge>, public mds::mod::CSerializable
{
private:
    //! Container of vertices.
    MCVerticeS vertices;

public:
    //! Standard method getEntityName().
    MDS_ENTITY_NAME("MCEdgesS");

    //! Standard method getEntityCompression().
    MDS_ENTITY_COMPRESSION(mds::mod::CC_RAW);

public:
    //! Default constructor.
    MCEdgeS() {}

    //! Empty destructor.
    ~MCEdgeS() {}


    //! Serializes the container of edges.
    template <class S>
    void serialize(mds::mod::CChannelSerializer<S>& Writer)
    {
        // serialize all vertices of the mesh
        vertices.serialize(Writer);

        // Begin of data serialization block
        Writer.beginWrite(*this);

        MCEdge * aktual = GetFirst();

        // write entity number
        Writer.write(MCList<MCEdge>::list_node_number);

        // entity cycle
        while( aktual )
        {
            // save actual entity
            aktual->SerializeEntity(Writer);

            // get next actual entity pointer
            aktual = aktual->GetNext();
        }

        // End of the block
        Writer.endWrite(*this);
    }

    //! Deserializes the container of edges.
    template <class S>
    void deserialize(mds::mod::CChannelSerializer<S>& Reader)
    {
        // clear container
        ClearAll();

        // deserialize vertices of the mesh
        vertices.deserialize(Reader);

        std::vector<MCVertex *> index_array;

        // prepare an ordered array of vertices
        vertices.MakeIndexVector(index_array);

        // Begin of data deserialization block
        Reader.beginRead(*this);

        // read entity number from channel
        int pocet = 0;
        Reader.read(pocet);

        // entity cycle
        MCEdge work_edge;
        for( int i = 0; i < pocet; ++i )
        {
            // read new entity
            work_edge.DeserializeEntity(Reader, index_array);

            // add the edge into container structures
            MCEdge *new_edge = New(work_edge.GetVertex(0), work_edge.GetVertex(1));
            assert(new_edge);
            new_edge->SetEntityAttributes(&work_edge);
        }

        // End of the block
        Reader.endRead(*this);
    }


    //! Returns pointer to a newly created edge.
    //! - Checks existence of the edge.
    //! - Returns pointer to an existing edge, or the newly created one.
    MCEdge * New( MCVertex * _u0, MCVertex * _u1 );

    //! Returns pointer to a newly created edge.
    //! - This method doesn't test existence of the edge.
    //! - Returns pointer to the newly created edge.
    MCEdge * NewNoTestExistence( MCVertex * _u0, MCVertex * _u1 );

    //! Removes an edge from the container.
    //! - Deregisters the edge and clears the memory.
    void Erase( MCEdge * del_ent);

    //! Deregisters all edges in the container from vertices.
    void DeRegistrationAll();

    //! Returns pointer to the internal container of vertices.
    MCVerticeS * GetVerticeS() { return &vertices; }

    //! Checks existence of an edge.
    //! - Returns pointer to the found edge, or NULL.
    MCEdge * TestExistence(MCVertex * _u0, MCVertex * _u1);

    //! Clears all edges currently stored in the container.
    virtual void ClearAll();

};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
