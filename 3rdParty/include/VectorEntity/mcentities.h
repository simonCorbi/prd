////////////////////////////////////////////////////////////
// $Id: mcentities.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCENTITIES_H
#define MCENTITIES_H

////////////////////////////////////////////////////////////
// includes

#include "mclist.h"
#include "mcentity.h"
#include "mcqueue.h"

// STL
#include <stdexcept>


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Container of entities derived from MCEntity.
 */
template <typename ENTITY_TYPE>
class MCEntitieS : public MCList<ENTITY_TYPE>
{
public:
    //! Queue for sorting entities in the container.
    typedef MCQueue<double, ENTITY_TYPE> MT_QUEUE;

    //! List of entities (= container).
    typedef MCList<ENTITY_TYPE> MT_LIST;

protected:
    //! Queue for sorting entities.
    MT_QUEUE fronta;

public:
    //! Defautl constructor.
    MCEntitieS() {}

    //! Destructor
    virtual ~MCEntitieS() { ClearAll(); }

    //! Returns pointer to the sorting queue.
    MT_QUEUE * GetQueue() { return &fronta; }

    //! Adds a given entity to the sorting queue.
    //! - The value attribute must be set!
    void Sort(ENTITY_TYPE * _entita)
    {
        assert(_entita != NULL);
        fronta.Sort(_entita->GetValue(), _entita);
    }

    //! Removes a given entity form the sorting queue.
    //! - The value attribute must be set!
    void UnSort(ENTITY_TYPE * _entita)
    {
        assert(_entita != NULL);
        fronta.UnSort(_entita->GetValue(), _entita);
    }

    //! Clears all entities currently stored in the container.
    virtual void ClearAll()
    {
        ENTITY_TYPE * aktual = MT_LIST::GetFirst(), * dalsi;

        // cyklus entit kontejneru
        while( aktual )
        {
            // ziskani nasledujici entity
            dalsi = aktual->GetNext();
            // vymazani objektu aktualni entity
            delete(aktual);
            // nastaveni nasledujici entity jako aktualni
            aktual = dalsi;
        }

        // uvolneni fronty
        fronta.Clear();

        // nulovani retezu uzlu
        MT_LIST::ClearAllNode();
    }

    //! Sorts all entities in the container against the value attribute.
    //! - It uses the internal sorting queue.
    void SortAll()
    {
        ENTITY_TYPE * aktual = MT_LIST::GetFirst();

        // nulovani fronty
        fronta.Clear();

        // cyklus vsech existujicich entit
        while( aktual )
        {
            // zatrideni aktualni entity
            fronta.Sort(aktual->GetValue(), aktual);
            // ziskani nasledujici entity
            aktual = aktual->GetNext();
        }
    }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
