////////////////////////////////////////////////////////////
// $Id: mccoordinate3d.h 1724 2010-05-27 11:05:25Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCCOORDINATE3D_H
#define MCCOORDINATE3D_H

////////////////////////////////////////////////////////////
// include files

#include <MDSTk/Image/mdsPoint3.h>

#include <cmath>

////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
//! Class encapsulating coordinates in 3D space.
//! - Just for backward compatibility...

template <typename T>
class MCCoordinate3D : public mds::img::CCoordinates3<T>
{
public:
    //! Base class.
    typedef mds::img::CCoordinates3<T> tBase;

public:
    //! Default constructor.
    //! - Initializes all coordinates to zero.
    MCCoordinate3D() : tBase(0, 0, 0) {}

    //! Copy constructor.
    MCCoordinate3D(const MCCoordinate3D& _c) : tBase(_c) {}

    //! Constructor which initializes coordinates to given values.
    MCCoordinate3D(const T& _x, const T& _y, const T& _z) : tBase(_x, _y, _z) {}

    //! Destructor
    ~MCCoordinate3D() {}

    //! Assignment operator.
    MCCoordinate3D& operator =(const MCCoordinate3D& p)
    {
        this->x() = p.x();
        this->y() = p.y();
        this->z() = p.z();
        return *this;
    }


    //! Returns current value of the x-coordinate.
    T& GetX()                                            { return this->x(); }
    const T& GetX() const                                { return this->x(); }

    //! Returns current value of the y-coordinate.
    T& GetY()                                            { return this->y(); }
    const T& GetY() const                                { return this->y(); }

    //! Returns current value of the z-coordinate.
    T& GetZ()                                            { return this->z(); }
    const T& GetZ() const                                { return this->z(); }

    //! Returns all x, y and z-coordinates.
    void GetXYZ(T & _x, T & _y, T & _z) const            { _x = this->x(); _y = this->y(); _z = this->z(); }

    //! Saves current coordinates (X,Y,Z) into a given array.
    void GetXYZ(T _xyz[3]) const                         { _xyz[0] = this->x(); _xyz[1] = this->y(); _xyz[2] = this->z(); }


    //! Sets value of the x-coordinate.
    //! @param _x - new value. */
    void SetX(const T& _x)                               { this->x() = _x; }

    //! Sets value of the y-coordinate.
    //! @param _x - new value. */
    void SetY(const T& _y)                               { this->y() = _y; }

    //! Sets value of the z-coordinate.
    //! @param _x - new value. */
    void SetZ(const T& _z)                               { this->z() = _z; }

    //! Sets all coordinates at once.
    void SetXYZ(const T& _x, const T& _y, const T& _z)   { this->x() = _x; this->y() = _y; this->z() = _z; }
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
