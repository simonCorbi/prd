////////////////////////////////////////////////////////////
// $Id: mcvertices.h 2065 2012-02-02 23:29:38Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCVERTICES_H
#define MCVERTICES_H

////////////////////////////////////////////////////////////
// includes

#include <MDSTk/Module/mdsSerializer.h>

#include "mctransformmatrix.h"
#include "mcentities.h"
#include "mcvertex.h"

#include <cstring>


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Container of vertices in 3D space.
 * - Checking of vertex existence may be enabled. In this case,
 *   simple hash codes are used identify similar vertices.
 * - This hash code cannot be changed for any existing vertex!
 * - Call SetTestExistence() to enable this mechanism.
 */
class MCVerticeS : public MCEntitieS<MCVertex>, public mds::mod::CSerializable
{
private:
    //! Enables vertex existence test.
    bool test_existence;

    //! Helper vertex...
    MCVertex work_node;

    //! Tree used to check vertex existence.
    MCQueue<unsigned int, MCVertex> exist_fronta;

public:
    //! Standard method getEntityName().
    MDS_ENTITY_NAME("MCVerticeS");

    //! Standard method getEntityCompression().
    MDS_ENTITY_COMPRESSION(mds::mod::CC_RAW);

public:
    //! Default constructor.
    MCVerticeS(bool bTestExistence = false) : test_existence(bTestExistence) {}

    //! Empty destructor.
    ~MCVerticeS() {}


    //! Serializes the container of vertices.
    template <class S>
    void serialize(mds::mod::CChannelSerializer<S>& Writer)
    {
        // Begin of data serialization block
        Writer.beginWrite(*this);

        int index = 0;  // vertex index
        MCVertex * aktual = GetFirst();

        // write the number of vertices
        Writer.write(MCList<MCVertex>::list_node_number);

        // entity cycle
        while( aktual )
        {
            // save actual entity
            aktual->SerializeEntity(Writer);
            // set vertex index
            aktual->SetIndex(index);
            // vertex index incrementation
            ++index;
            // get next actual entity pointer
            aktual = aktual->GetNext();
        }

        // End of the block
        Writer.endWrite(*this);
    }

    //! Deserializes the container of vertices
    template <class S>
    void deserialize(mds::mod::CChannelSerializer<S>& Reader)
    {
        // clear the container
        ClearAll();

        // Begin of data deserialization block
        Reader.beginRead(*this);

        // read the number of entities
        int pocet = 0;
        Reader.read(pocet);

        // entity cycle
        MCVertex work_vertex;
        for( int i = 0; i < pocet; ++i )
        {
            // read new entity
            work_vertex.DeserializeEntity(Reader);

            // including read edge into container structures
            MCVertex *new_vertex = New(work_vertex);
            assert(new_vertex);
            new_vertex->SetEntityAttributes(&work_vertex);
        }

        // End of the block
        Reader.endRead(*this);
    }


    //! Returns true if vertex existence test is enabled.
    bool GetTestExistence() const { return test_existence; }

    //! Enables/disables vertex test existence.
    void SetTestExistence(bool _value) { test_existence = _value; }


    //! Creates a new vertex.
    //! - Tests existence of the vertex if enabled.
    //! - Returns pointer to the newly created vertex or an existing one.
    MCVertex * New( MCPoint3D & new_bod );

    //! Creates a new vertex.
    //! - Tests existence of the vertex if enabled.
    //! - Returns pointer to the newly created vertex or an existing one.
    MCVertex * New( double new_x, double new_y, double new_z );

    //! Creates a new vertex using the internal work node.
    //! - Tests existence of the vertex if enabled.
    //! - Returns pointer to the newly created vertex or an existing one.
    MCVertex * NewFromWorkNode();

    //! Removes a given vertex from the container.
    //! - Removes the vertex from the internal sorting queue.
    //! - The vertex is destroyed and the memory is cleared.
    void Erase( MCVertex * del_ent);


    //! Creates an array of pointers to vertices ordered
    //! by previously assigned indexes.
    void MakeIndexVector( std::vector<MCVertex *> & pole_node );

    //! Assignes unique labels/indexes to all vertices in the container.
    void Indexing();

    //! Checks if a given vertex already exists in the container.
    //! - Returns pointer to a found vertex, NULL otherwise.
    MCVertex * TestExistence(MCVertex * _test)
    {
        assert(_test);
        return ((MCVertex *)exist_fronta.Exist(_test->GetHashCode(), _test));
    }

    //! Checks if a given point already exists in the container.
    //! - Returns pointer to a found vertex, NULL otherwise.
    MCVertex * TestExistence2(MCPoint3D * _test)
    {
        MCVertex aux;
        aux.SetPoint3D(_test);
        aux.MakeHashCode();
        return TestExistence(&aux);
    }

    //! Regenerates the internal tree used for testing vertex existence.
    void RegenExistence();

    //! Returns bounding box (minimal and maximal coordinates) of all vertices.
    void GetRange( MCPoint3D & bod_min, MCPoint3D & bod_max );

    //! Removes all vertices from the container.
    //! - Clears the memory.
    void ClearAll()
    {
        MCEntitieS<MCVertex>::ClearAll();
        exist_fronta.Clear();
    }

    //! Transform vertices by given transformation matrix
    void Transformation(MCTransformMatrix & matrix);

    //! Erase vertices free from registered entities
    void EraseFreeVertices();

};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
