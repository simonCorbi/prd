////////////////////////////////////////////////////////////
// $Id: mcsphere.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCSPHERE_H
#define MCSPHERE_H

////////////////////////////////////////////////////////////
// include

// MDSTk
#include <MDSTk/Base/mdsSmallObject.h>
#include <MDSTk/Module/mdsSerializer.h>

#include "mcentity.h"
#include "mcpoint3d.h"

#include <cstdio>
#include <memory.h>


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
/*!
 * Analytical description of a sphere (circumsphere
 * of a tetrahedron, triangle, or edge).
 */
class VCTL_EXPORT MCSphere : public mds::base::CSmallObject<>
{
private:
    //! Sphere radius.
    double radius;

    //! Sphere center.
    MCPoint3D stred;

public:
    //! Default constructor.
    MCSphere() : radius(0.0) {}

    //! Destructor.
    ~MCSphere() {}


    //! Writes the sphere data.
    template <class S>
    void SerializeEntity(mds::mod::CChannelSerializer<S>& Writer)
    {
        Writer.write(stred.GetX());
        Writer.write(stred.GetY());
        Writer.write(stred.GetZ());
        Writer.write(radius);
    }

    //! Reads the sphere data.
    template <class S>
    void DeserializeEntity(mds::mod::CChannelSerializer<S>& Reader)
    {
        double x, y, z;
        Reader.read(x);
        Reader.read(y);
        Reader.read(z);
        Reader.read(radius);
        stred.SetX(x);
        stred.SetY(y);
        stred.SetZ(z);
    }


    //! Returns sphere parameters.
    void GetSphere(double & _cx,  double & _cy,  double & _cz,  double & _r) const
    {
        stred.GetXYZ(_cx, _cy, _cz);
        _r = radius;
    }

    //! Returns radius of the sphere.
    double GetRadius() const { return radius; }

    //! Returns center of the sphere.
    void GetCenter(MCPoint3D & _point) const { stred.GetPoint3D(_point); }

    //! Returns center of the sphere.
    MCPoint3D * GetCenter() { return &stred; }
    const MCPoint3D * GetCenter() const { return &stred; }


    //! Estimates tetrahedron circumsphere.
    void MakeSphereTetra(MCVertex * n1, MCVertex * n2, MCVertex * n3, MCVertex * n4);

    //! Estimates triangle circumsphere.
    void MakeSphereTri(MCVertex * n1, MCVertex * n2, MCVertex * n3);

    //! Estimates edge circumsphere.
    void MakeSphereEdge(MCVertex * n1, MCVertex * n2);


    //! Checks position of a given point with respect to the sphere.
    //! - 1 ... the point lies inside the sphere.
    //! - 0 ... the point lies directly on the sphere surface.
    //! - -1 ... outside the sphere.
    int TestPoint(MCPoint3D & _point);

private:
    /** Vyreseni korenu dane matice 4x4 soustavy linearnich rovnic Gausovou eliminaci.
     *  Koreny rovnic jsou chapany jako stred resene koule.
     *  @param matice - pole jako dana soustava rovnic, matice 4x4 prvku typu double.
     *  @param vysledek - reference na vysledny stred koule.
     *  @return stav uspesnosti reseni soustavy.
     *  @return 0 = reseni probehlo OK.
     *  @return -1 = reseni neprobehlo OK, soustava nema reseni, deleni nulou. */
    static int GausElimin(double * matice, MCPoint3D & vysledek);
};

} // namespace vctl

#endif

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
