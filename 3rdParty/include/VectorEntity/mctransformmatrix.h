////////////////////////////////////////////////////////////
// $Id: mctransformmatrix.h 1863 2010-08-31 20:40:15Z spanel $
////////////////////////////////////////////////////////////

#ifndef MCTRANSFORMMATRIX_H
#define MCTRANSFORMMATRIX_H

////////////////////////////////////////////////////////////
// include files

// MDSTk
#include <MDSTk/Base/mdsSmallObject.h>
#include <MDSTk/Math/mdsStaticMatrix.h>
#include <MDSTk/Image/mdsImageTypes.h>
#include <MDSTk/Image/mdsPoint3.h>

#include "vctlExport.h"

#include <cmath>


////////////////////////////////////////////////////////////
// namespace vctl

namespace vctl
{

////////////////////////////////////////////////////////////
//! Class encapsulates basic linear affine transformations in 3D space.
//! The transformation is aplied for all clases derived from CCoordinates3, such are MCVector3D and MCpoint3d. 
//! Homogenous coordinate weight is set separately.

class VCTL_EXPORT MCTransformMatrix : public mds::math::CStaticMatrix<mds::img::tCoordinate, 4, 4> 
{
public:
    //! Base matrix class type.
    typedef mds::math::CStaticMatrix<mds::img::tCoordinate, 4, 4>  tMatrix44;

    //! Base vector class type
    typedef mds::img::CCoordinates3<mds::img::tCoordinate> tCoords3;

    //! Default constructor.
    //! - Initializes all coordinates to zero.
    MCTransformMatrix() {}

    //! Constructor that initializes matrix values.
    MCTransformMatrix(const tMatrix44 & v) : tMatrix44(v) {}

    //! Constructor that initializes matrix values.
    MCTransformMatrix(tMatrix44 * v) : tMatrix44(*v) {}

    //! Destructor.
    ~MCTransformMatrix() {}


    //! Set rotation around X axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & makeRotateX(double rotation_angle);

    //! Set rotation around Y axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & makeRotateY(double rotation_angle);

    //! Set rotation around Z axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & makeRotateZ(double rotation_angle);

    //! Set a translation to transformation matrix
    //! @param x: shift distance in X axis
    //! @param y: shift distance in Y axis
    //! @param z: shift distance in Z axis
    MCTransformMatrix & makeTranslation(double x, double y, double z);

    //! Set a scale to transformation matrix
    //! @param sx: scale factor in X axis
    //! @param sy: scale factor in Y axis
    //! @param sz: scale factor in Z axis
    MCTransformMatrix & makeScale(double sx, double sy, double sz);


    //! Add rotation arount X axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & addRotateX(double rotation_angle);

    //! Add rotation arount Y axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & addRotateY(double rotation_angle);

    //! Add rotation arount Z axis to transformation matrix
    //! @param rotation_angle: radians Angle
    MCTransformMatrix & addRotateZ(double rotation_angle);

    //! Add a translation to transformation matrix
    //! @param x: shift distance in X axis
    //! @param y: shift distance in Y axis
    //! @param z: shift distance in Z axis
    MCTransformMatrix & addTranslation(double x, double y, double z);

    //! Add a scale to transformation matrix
    //! @param sx: scale factor in X axis
    //! @param sy: scale factor in Y axis
    //! @param sz: scale factor in Z axis
    MCTransformMatrix & addScale(double sx, double sy, double sz);


    //! Multiplication of the matrix by given matrix (from right side)
    //! @param mm: multiplied matrix, from right side
    void postMult(const MCTransformMatrix & mm);

    //! Multiplication of given vector with the matrix
    //! Result vector is returned
    //! @param v: multiplied vector
    //! @param coord_weight: homogenout coordinate weight, for point = 1, for vector = 0 
    MCTransformMatrix::tCoords3 preMult(const tCoords3 & v, mds::img::tCoordinate coord_weight) const;

    //! Multiplication of given vector with the matrix
    //! Result is saved into the given vector
    //! @param v: multiplied and result vector
    //! @param coord_weight: homogenout coordinate weight, for point = 1, for vector = 0 
    void preMult2(tCoords3 & v, mds::img::tCoordinate coord_weight) const;

    //! Multiplication of given vector with the matrix
    //! Result is saved into second given vector
    //! @param v: multiplied vector
    //! @param coord_weight: homogenout coordinate weight, for point = 1, for vector = 0 
    //! @param r: result vector
    void preMult(const tCoords3 & v, mds::img::tCoordinate coord_weight, tCoords3 & r) const;

    //! Make identity matrix
    void makeIdentity();
};


} // namespace vctl

#endif // MCTRANSFORMMATRIX_H

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
