<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ" sourcelanguage="en">
<context>
    <name>CCustomDockWidgetTitle</name>
    <message>
        <location filename="../src/CCustomUI.cpp" line="271"/>
        <source>Save screenshot</source>
        <translation>Uložit snímek obrazovky</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="275"/>
        <source>Save slice</source>
        <translation>Uložit řez</translation>
    </message>
</context>
<context>
    <name>CDataInfoDialog</name>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="20"/>
        <source>Data Information</source>
        <translation>Informace o datové sadě</translation>
    </message>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="51"/>
        <source>Parameter</source>
        <translation>Parametr</translation>
    </message>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="56"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="181"/>
        <source>Patient Data</source>
        <translation>Pacient</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="182"/>
        <source>Dimensions</source>
        <translation>Rozměry</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="182"/>
        <source>%1 x %2 x %3 voxels</source>
        <translation>%1 x %2 x %3 voxelů</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="183"/>
        <source>Resolution</source>
        <translation>Rozlišení</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="183"/>
        <location filename="../src/CDataInfoDialog.cpp" line="184"/>
        <source>%1 x %2 x %3 mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="184"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="185"/>
        <source>Patient Name</source>
        <translation>Jméno pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="186"/>
        <source>Patient ID</source>
        <translation>ID pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="187"/>
        <source>Patient Position</source>
        <translation>Pozice pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="188"/>
        <source>Patient Birthday</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="189"/>
        <source>Patient Sex</source>
        <translation>Pohlaví</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="190"/>
        <source>Patient Description</source>
        <translation>Popis pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="191"/>
        <source>Study ID</source>
        <translation>ID studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="192"/>
        <source>Study Date</source>
        <translation>Datum studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="193"/>
        <source>Study Description</source>
        <translation>Popis studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="194"/>
        <source>Series ID</source>
        <translation>ID série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="195"/>
        <source>Series Date</source>
        <translation>Datum série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="196"/>
        <source>Series Time</source>
        <translation>Čas série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="197"/>
        <source>Series Description</source>
        <translation>Popis série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="198"/>
        <source>Scan Options</source>
        <translation>Nastavení skenu</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="199"/>
        <source>Manufacturer</source>
        <translation>Výrobce</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="200"/>
        <source>Model Name</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="212"/>
        <source>Volume Transformation</source>
        <translation>Transformace dat</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="216"/>
        <source>Matrix</source>
        <translation>Matice</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="218"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="231"/>
        <source>Model %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="238"/>
        <source>Closed</source>
        <translation>Uzavřený</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="238"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="255"/>
        <source>DICOM Info</source>
        <translation>Informace DICOM</translation>
    </message>
    <message>
        <source>Bone Model</source>
        <translation type="obsolete">Model kosti</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="233"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="234"/>
        <source>Triangles</source>
        <translation>Trojúhelníků</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="235"/>
        <source>Nodes</source>
        <translation>Vrcholů</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="236"/>
        <source>Components</source>
        <translation>Komponent</translation>
    </message>
</context>
<context>
    <name>CModelsWidget</name>
    <message>
        <location filename="../src/modelswidget.cpp" line="243"/>
        <source>Select color for model</source>
        <translation>Zvolte barvu modelu</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="498"/>
        <source>Adjust Position...</source>
        <translation>Upravit pozici...</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="499"/>
        <source>Center Position</source>
        <translation>Umístit na střed</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="514"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="517"/>
        <location filename="../src/modelswidget.cpp" line="518"/>
        <location filename="../src/modelswidget.cpp" line="519"/>
        <source> mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="533"/>
        <source>X:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="535"/>
        <source>Y:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="537"/>
        <source>Z:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CPluginInfoDialog</name>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="43"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="69"/>
        <source>Plugin Information</source>
        <translation>Informace o pluginech</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="90"/>
        <source>%1 found the following plugins in
%2</source>
        <translation>%1 našlo tyto pluginy na cestě
%2</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="95"/>
        <source>%1 (Static Plugin)</source>
        <translation>%1 (Statický plugin)</translation>
    </message>
</context>
<context>
    <name>CPluginManager</name>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Pluginy</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="376"/>
        <source>&amp;Plugins</source>
        <translation>&amp;Pluginy</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="463"/>
        <source>Show Help</source>
        <translation>Zobrazit nápovědu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="492"/>
        <source>Show/Hide Plugin Toolbar</source>
        <translation>Zobrazit/Skrýt lištu pluginu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="501"/>
        <source>Show/Hide Plugin Panel</source>
        <translation>Zobrazit/Skrýt panel pluginu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="513"/>
        <source>Plugin Registration...</source>
        <translation>Registrace pluginu...</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="662"/>
        <source>A PDF viewer is required to view help!</source>
        <translation>K zobrazení nápovědy je potřeba PDF prohlížeč!</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="665"/>
        <source>Help file is missing!</source>
        <translation>Soubor s nápovědou nebyl nalezen!</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="671"/>
        <source>Couldn&apos;t open online help! Please verify your internet connection.</source>
        <translation>Nepodařilo se získat online nápovědu, zkontrolujte prosím své internetové připojení.</translation>
    </message>
</context>
<context>
    <name>CPreferencesDialog</name>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="26"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="58"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="65"/>
        <location filename="../ui/cpreferencesdialog.ui" line="68"/>
        <source>Selects language used in application.</source>
        <translation>Zvolí jazyk použitý v aplikaci.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="75"/>
        <source>Rendering Mode</source>
        <translation>Renderovací jádro</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="82"/>
        <location filename="../ui/cpreferencesdialog.ui" line="85"/>
        <source>Selects mode of rendering.</source>
        <translation>Zvolí režim vykreslování.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="89"/>
        <source>Single Threaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="94"/>
        <source>Multi Threaded</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="102"/>
        <location filename="../ui/cpreferencesdialog.ui" line="105"/>
        <source>Allows logging.</source>
        <translation>Povolí vedení záznamu aplikace.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="108"/>
        <source>Logging</source>
        <translation>Vedení záznamu aplikace</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="115"/>
        <source>Show log...</source>
        <translation>Zobrazit záznam aplikace...</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="122"/>
        <source>Save path for segmentation and model data</source>
        <translation>Nabízená cesta pro uložení segmentačních dat a modelu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="125"/>
        <source>Save Path</source>
        <translation>Nabízená cesta pro uložení</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="131"/>
        <source>Last used path</source>
        <translation>poslední použitá</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="138"/>
        <source>Current project path</source>
        <translation>podle otevřeného projektu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="148"/>
        <source>Background Color</source>
        <translation>Barva pozadí</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="181"/>
        <source>Models mirror region data</source>
        <translation>Modely svázány s regiony</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="196"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="201"/>
        <location filename="../ui/cpreferencesdialog.ui" line="209"/>
        <source>Shortcut</source>
        <translation>Klávesová zkratka</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="218"/>
        <source>Type to set shortcut</source>
        <translation>Zadejte klávesovou zkratku</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="225"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="232"/>
        <source>Clear</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="279"/>
        <location filename="../ui/cpreferencesdialog.ui" line="308"/>
        <source>General</source>
        <translation>Základní</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="313"/>
        <source>Shortcuts</source>
        <translation>Klávesové zkratky</translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="44"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="259"/>
        <source>Alt+%1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CSeriesSelectionDialog</name>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="20"/>
        <source>Please, select one of the found DICOM series...</source>
        <translation>Prosím, zvolte některou z nalezených datových sad...</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="29"/>
        <source>Sampling Factors</source>
        <translation>Převzorkování</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="86"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="96"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="106"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="149"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="154"/>
        <source>Series</source>
        <translation>Série</translation>
    </message>
    <message>
        <source>Patient  Name</source>
        <translation type="obsolete">Jméno pacienta</translation>
    </message>
    <message>
        <source>Series Date</source>
        <translation type="obsolete">Datum série</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="159"/>
        <source>Image Data</source>
        <translation>Obrazová data</translation>
    </message>
    <message>
        <location filename="../src/cseriesselectiondialog.cpp" line="223"/>
        <source>%1 x %2 x %3 voxels
%4 x %5 x %6 mm
%7 - %8</source>
        <translation>%1 x %2 x %3 voxelů
%4 x %5 x %6 mm
%7 - %8</translation>
    </message>
    <message>
        <location filename="../src/cseriesselectiondialog.cpp" line="335"/>
        <source>Please select a series.</source>
        <translation>Zvolte prosím sérii.</translation>
    </message>
</context>
<context>
    <name>CVolumeLimiterDialog</name>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="20"/>
        <source>Please, select volume of your interest...</source>
        <translation>Prosím, omezte oblast zájmu pomocí žlutých značek...</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="37"/>
        <source>XY View</source>
        <translation>XY pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="56"/>
        <source>XZ View</source>
        <translation>XZ pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="75"/>
        <source>YZ View</source>
        <translation>YZ pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="84"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="90"/>
        <source>Imaging Mode</source>
        <translation>Mód zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="97"/>
        <source>Mode of imaging</source>
        <translation>Režim zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="100"/>
        <source>Selects mode of imaging.</source>
        <translation>Zvolí režim zobrazování.</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="104"/>
        <source>RTG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="109"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="120"/>
        <source>Volume of Interest</source>
        <translation>Oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="128"/>
        <source>Min X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="138"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="158"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="178"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="202"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="222"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="242"/>
        <source>Volume of interest</source>
        <translation>Oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="148"/>
        <source>Min Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="168"/>
        <source>Min Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="192"/>
        <source>Max X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="212"/>
        <source>Max Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="232"/>
        <source>Max Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="272"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="275"/>
        <source>Resets window zoom.</source>
        <translation>Obnoví původní přiblížení oken.</translation>
    </message>
</context>
<context>
    <name>CVolumeRenderingWidget</name>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="164"/>
        <source>Soft tissues</source>
        <translation>Měkké tkáně</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="165"/>
        <source>Bone tissues</source>
        <translation>Kostní tkáň</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="187"/>
        <source>Transparent</source>
        <translation>Průhledný</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="188"/>
        <source>Air pockets</source>
        <translation>Vzduchové kapsy</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="189"/>
        <source>Bones (skull)</source>
        <translation>Kosti (lebka)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="190"/>
        <source>Bones (spine)</source>
        <translation>Kosti (páteř)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="191"/>
        <source>Bones (pelvis)</source>
        <translation>Kosti (pánev)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="212"/>
        <location filename="../src/volumerenderingwidget.cpp" line="234"/>
        <source>Enhance soft</source>
        <translation>Zvýraznit měkké tkáně</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="213"/>
        <location filename="../src/volumerenderingwidget.cpp" line="235"/>
        <source>Enhance hard</source>
        <translation>Zvýraznit kosti</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="256"/>
        <source>Bone surface</source>
        <translation>Povrch kosti</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="257"/>
        <source>Skin surface</source>
        <translation>Měkké tkáně</translation>
    </message>
</context>
<context>
    <name>DensityWindowWidget</name>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="20"/>
        <source>Density Window</source>
        <translation>Densitní okénko</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="47"/>
        <source>Current Settings</source>
        <translation>Aktuální nastavení</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="88"/>
        <source>Center</source>
        <translation>Střed</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="69"/>
        <location filename="../ui/densitywindowwidget.ui" line="72"/>
        <source>Adjusts center of density window.</source>
        <translation>Upraví střed densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="95"/>
        <source>Width</source>
        <translation>Šířka</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="53"/>
        <location filename="../ui/densitywindowwidget.ui" line="56"/>
        <source>Adjusts width of density window.</source>
        <translation>Upraví šířku densitního okénka</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="140"/>
        <source>Predefined Windows</source>
        <translation>Připravená okénka</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="146"/>
        <location filename="../ui/densitywindowwidget.ui" line="149"/>
        <source>Selects default setting of density window.</source>
        <translation>Zvolí původní nastavení densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="152"/>
        <source>Default</source>
        <translation>Standardní</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="159"/>
        <location filename="../ui/densitywindowwidget.ui" line="162"/>
        <source>Analyzes data and selects optimal setting of density window.</source>
        <translation>Analyzuje data a zvolí optimální nastavení densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="165"/>
        <source>Find Optimal</source>
        <translation>Nalézt optimální</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="172"/>
        <location filename="../ui/densitywindowwidget.ui" line="175"/>
        <source>Selects setting of density window optimal for showing bones.</source>
        <translation>Zvolí nastavení optimální pro zobrazování kostí.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="178"/>
        <source>Show Bones</source>
        <translation>Zvýraznit kosti</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Úpravy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="61"/>
        <source>Switch Mouse Mode</source>
        <translation>Přepnout mód myši</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="159"/>
        <source>View</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="79"/>
        <source>Views</source>
        <translation>Pohledy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="89"/>
        <location filename="../src/mainwindow.cpp" line="4205"/>
        <source>Toolbars</source>
        <translation>Nástrojové lišty</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="99"/>
        <source>Control Panels</source>
        <translation>Kontrolní panely</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="109"/>
        <source>3D Scene</source>
        <translation>3D scéna</translation>
    </message>
    <message>
        <source>Model</source>
        <translation type="obsolete">Model</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="188"/>
        <location filename="../ui/mainwindow.ui" line="507"/>
        <source>Main Toolbar</source>
        <translation>Hlavní lišta</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="211"/>
        <location filename="../ui/mainwindow.ui" line="521"/>
        <source>Views Toolbar</source>
        <translation>Lišta pohledů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="222"/>
        <location filename="../ui/mainwindow.ui" line="754"/>
        <source>Mouse Toolbar</source>
        <translation>Mód myši</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="233"/>
        <location filename="../ui/mainwindow.ui" line="768"/>
        <source>Visibility Toolbar</source>
        <translation>Viditelnost objektů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="244"/>
        <source>Application Toolbar</source>
        <translation>Aplikační lišta</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="268"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="286"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Soubor</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>3DimViewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="118"/>
        <source>Layout</source>
        <translation>Uspořádání</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="938"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation type="obsolete">Filtrování</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="obsolete">Nástroje</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation type="obsolete">Měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="259"/>
        <source>Import Patient DICOM Data...</source>
        <translation>Importovat DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="262"/>
        <location filename="../ui/mainwindow.ui" line="265"/>
        <source>Loads DICOM dataset from a given directory.</source>
        <translation>Načte DICOM data ze zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="277"/>
        <source>Load Patient VLM Data...</source>
        <translation>Načíst objemová VLM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="280"/>
        <location filename="../ui/mainwindow.ui" line="283"/>
        <source>Loads density data from a specified VLM file.</source>
        <translation>Načte densitní data ze zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="295"/>
        <source>Save Volumetric VLM Data...</source>
        <translation>Uložit objemová VLM data...</translation>
    </message>
    <message>
        <source>Saves volumetric data to a specified VLM file.</source>
        <translation type="obsolete">Uloží objemová data do zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="304"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="313"/>
        <source>Exit</source>
        <translation>Ukončit program</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="316"/>
        <location filename="../ui/mainwindow.ui" line="319"/>
        <source>Terminates the program.</source>
        <translation>Ukončí program.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="322"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="331"/>
        <source>Print...</source>
        <translation>Tisk...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="334"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="346"/>
        <source>Undo</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="349"/>
        <location filename="../ui/mainwindow.ui" line="352"/>
        <source>Undoes the last command.</source>
        <translation>Vrátí zpět poslední příkaz.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="355"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="364"/>
        <source>Redo</source>
        <translation>Opakovat</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="367"/>
        <location filename="../ui/mainwindow.ui" line="370"/>
        <source>Redoes the last command.</source>
        <translation>Obnoví poslední odvolaný příkaz.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="373"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="385"/>
        <location filename="../ui/mainwindow.ui" line="737"/>
        <source>3D View</source>
        <translation>3D scéna</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="388"/>
        <source>Shows/Hides the main 3D scene.</source>
        <translation>Zobrazí/Skryje hlavní 3D scénu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="403"/>
        <source>Axial View</source>
        <translation>Axiální pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="406"/>
        <location filename="../ui/mainwindow.ui" line="409"/>
        <source>Shows/Hides the axial (XY) plane view.</source>
        <translation>Zobrazí/Skryje pohled na axiální (XY) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="421"/>
        <source>Coronal View</source>
        <translation>Koronární pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="424"/>
        <location filename="../ui/mainwindow.ui" line="427"/>
        <source>Shows/Hides the coronal (XZ) plane view.</source>
        <translation>Zobrazí/Skryje pohled na koronární (XZ) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="439"/>
        <source>Sagittal View</source>
        <translation>Sagitální pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="442"/>
        <location filename="../ui/mainwindow.ui" line="445"/>
        <source>Shows/Hides the sagittal (YZ) plane view.</source>
        <translation>Zobrazí/Skryje pohled na sagitální (YZ) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="460"/>
        <source>Panoramic View</source>
        <translation>Panoramatický pohled.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="475"/>
        <source>Normal Slice</source>
        <translation>Normálový řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="490"/>
        <source>Volume Rendering View</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="493"/>
        <location filename="../ui/mainwindow.ui" line="496"/>
        <source>Shows/Hides main volume rendering window.</source>
        <translation>Zobrazí/Skryje okno s volume renderingem.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="510"/>
        <source>Shows/Hides main toolbar.</source>
        <translation>Zobrazí/Skryje hlavní lištu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="524"/>
        <source>Shows/Hides views toolbar.</source>
        <translation>Zobrazí/Skryje lištu pohledů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="536"/>
        <source>Density Window Panel</source>
        <translation>Panel densitního okénka</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="539"/>
        <location filename="../ui/mainwindow.ui" line="542"/>
        <source>Shows/Hides the density window adjusting panel.</source>
        <translation>Zobrazí/Skryje panel densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="554"/>
        <source>Ortho Slices Panel</source>
        <translation>Panel kolmých řezů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="557"/>
        <location filename="../ui/mainwindow.ui" line="560"/>
        <source>Shows/Hides the ortho slices panel.</source>
        <translation>Zobrazí/Skryje panel kolmých řezů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="572"/>
        <source>Trackball Mode</source>
        <translation>Manipulovat se scénou</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="575"/>
        <location filename="../ui/mainwindow.ui" line="578"/>
        <source>Switches mouse to scene manipulation mode.</source>
        <translation>Přepne myš do módu manipulace se scénou.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="590"/>
        <source>Object Manipulation</source>
        <translation>Manipulovat s objekty</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="593"/>
        <location filename="../ui/mainwindow.ui" line="596"/>
        <source>Switches mouse to the object manipulation mode so that you can change position of cross-sectional slices, implants, etc.</source>
        <translation>Přepne myš do módu manipulace s objekty.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="608"/>
        <source>Density Window Adjusting</source>
        <translation>Měnit densitní okénko</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="611"/>
        <location filename="../ui/mainwindow.ui" line="614"/>
        <source>Switches mouse to the density window adjusting mode. Press the left mouse button and modify the density window moving the mouse cursor.</source>
        <translation>Přepne myš do módu nastavování densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="619"/>
        <source>Create Surface Model</source>
        <translation>Vytvořit model povrchu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="622"/>
        <location filename="../ui/mainwindow.ui" line="625"/>
        <source>Creates polygonal surface model regarding the current tissue segmentation. For options see the tissue segmentation panel...</source>
        <translation>Vytvoří polygonální model povrchu segmentované tkáně...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="637"/>
        <source>Show Surface Model</source>
        <translation>Zobrazit model povrchu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="640"/>
        <location filename="../ui/mainwindow.ui" line="643"/>
        <source>Shows/Hides the surface model in the 3D view.</source>
        <translation>Zobrazí/Skryje model povrchu v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="655"/>
        <source>Axial Slice</source>
        <translation>Axiální řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="658"/>
        <location filename="../ui/mainwindow.ui" line="661"/>
        <source>Shows/Hides the axial (XY) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje axiální (XY) řez v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="673"/>
        <source>Coronal Slice</source>
        <translation>Koronární řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="676"/>
        <location filename="../ui/mainwindow.ui" line="679"/>
        <source>Shows/Hides the coronal (XZ) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje koronoární (XZ) řez v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="691"/>
        <source>Sagittal Slice</source>
        <translation>Sagitální řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="694"/>
        <location filename="../ui/mainwindow.ui" line="697"/>
        <source>Shows/Hides the sagittal (YZ) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje sagitální (YZ) řez v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="709"/>
        <source>Segmentation Panel</source>
        <translation>Segmentační panel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="712"/>
        <location filename="../ui/mainwindow.ui" line="715"/>
        <source>Shows/Hides the tissue segmentation panel.</source>
        <translation>Zobrazí/Skryje panel segmentace tkání.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="723"/>
        <source>Tabs</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="726"/>
        <location filename="../ui/mainwindow.ui" line="729"/>
        <source>Switches workspace to tabbed layout.</source>
        <translation>Umístí hlavní pohledy do záložek.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="740"/>
        <location filename="../ui/mainwindow.ui" line="743"/>
        <source>Switches workspace to a layout with the 3D scene as the main window.</source>
        <translation>Nastaví 3D scénu jako hlavní okno.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="757"/>
        <source>Shows/Hides mouse mode toolbar.</source>
        <translation>Zobrazí/Skryje lištu manipulace.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="771"/>
        <source>Shows/Hides slices visibility toolbar.</source>
        <translation>Zobrazí/Skryje lištu viditelnosti.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="783"/>
        <source>Volume Rendering Panel</source>
        <translation>Panel volume renderingu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="786"/>
        <location filename="../ui/mainwindow.ui" line="789"/>
        <source>Shows/Hides the volume rendering configuration panel.</source>
        <translation>Zobrazí/Skryje panel konfigurace volume renderingu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="798"/>
        <source>Preferences...</source>
        <translation>Nastavení...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="801"/>
        <location filename="../ui/mainwindow.ui" line="804"/>
        <source>Shows application&apos;s options.</source>
        <translation>Zobrazí možnosti nastavení aplikace.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="813"/>
        <source>Show Data Properties...</source>
        <translation>Zobrazit informace o datové sadě...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="816"/>
        <location filename="../ui/mainwindow.ui" line="819"/>
        <source>Shows information about the loaded dataset.</source>
        <translation>Zobrazí informace o aktuální datové sadě.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="828"/>
        <source>Load STL Model...</source>
        <translation>Načíst STL model...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="831"/>
        <location filename="../ui/mainwindow.ui" line="834"/>
        <source>Loads surface model from a specified STL file.</source>
        <translation>Načte povrchový (polygonální) model ze zadaného souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="839"/>
        <source>About...</source>
        <translation>O aplikaci...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="842"/>
        <location filename="../ui/mainwindow.ui" line="845"/>
        <source>Shows basic information about the program.</source>
        <translation>Zobrazí základní informace o aplikaci.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="850"/>
        <source>About Plugins...</source>
        <translation>O pluginech...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="853"/>
        <location filename="../ui/mainwindow.ui" line="856"/>
        <source>Shows basic information about loaded plugins.</source>
        <translation>Zobrazí základní informace o načtených pluginech.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="861"/>
        <source>Gaussian Filter</source>
        <translation>Gaussův filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="864"/>
        <location filename="../ui/mainwindow.ui" line="867"/>
        <source>Applies 3D Gaussian filter on the volumetric data.</source>
        <translation>Aplikuje 3D Gaussův filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="872"/>
        <source>Median Filter</source>
        <translation>Mediánový filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="875"/>
        <location filename="../ui/mainwindow.ui" line="878"/>
        <source>Applies median filter on the volumetric data.</source>
        <translation>Aplikuje mediánový filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="883"/>
        <source>3D Anisotropic Filter</source>
        <translation>3D anizotropní filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="886"/>
        <location filename="../ui/mainwindow.ui" line="889"/>
        <source>Applies 3D anisotropic filter on the volumetric data.</source>
        <translation>Aplikuje 3D anizotropní filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="897"/>
        <source>Grid</source>
        <translation>Mříž</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="900"/>
        <location filename="../ui/mainwindow.ui" line="903"/>
        <source>Switches workspace to a grid layout with cross-sectional slices and 3D scene.</source>
        <translation>Nastaví do hlavního okna mříž hlavních pohledů na scénu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="914"/>
        <source>Show Information Widgets</source>
        <translation>Zobrazit základní informace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="917"/>
        <location filename="../ui/mainwindow.ui" line="920"/>
        <source>Shows/Hides special information widgets in all views.</source>
        <translation>Zobrazí/Skryje základní informace ve všech pohledech.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="939"/>
        <location filename="../ui/mainwindow.ui" line="942"/>
        <source>Shows Help.</source>
        <translation>Zobrazí Nápovědu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="960"/>
        <location filename="../ui/mainwindow.ui" line="963"/>
        <source>Switches mouse to scene scale so that you can scale scene to fit into window.</source>
        <translation>Přepne myš do módu škálování scény.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1004"/>
        <location filename="../ui/mainwindow.ui" line="1007"/>
        <source>Saves generated model to a specified STL file.</source>
        <translation>Uloží vytvořený model do zadaného STL souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1015"/>
        <location filename="../ui/mainwindow.ui" line="1018"/>
        <source>Sends volumetric data via DataExpress Service.</source>
        <translation>Odešle objemová data skrz DataExpress.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1030"/>
        <source>Measure Density Value [Hu]</source>
        <translation>Měřit hustotu [Hu]</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1033"/>
        <location filename="../ui/mainwindow.ui" line="1036"/>
        <source>To measure local density, specify a point using the mouse cursor and click the left button.</source>
        <translation>Pro změření hustoty zadejte bod pomocí kurzoru myši a kliknutím levého tlačitka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1048"/>
        <source>Measure Distance [mm]</source>
        <translation>Měřit vzdálenost [mm]</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1051"/>
        <location filename="../ui/mainwindow.ui" line="1054"/>
        <source>Measure distance by clicking the left mouse button and dragging.</source>
        <translation>Změřte vzdálenost kliknutím levého tlačítka myši a tažením.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1063"/>
        <source>Clear Measurements</source>
        <translation>Vymazat výsledky měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1066"/>
        <location filename="../ui/mainwindow.ui" line="1069"/>
        <source>Clears all visible measurement results.</source>
        <translation>Smaže všechny viditelné výsledky měření.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1084"/>
        <location filename="../ui/mainwindow.ui" line="1087"/>
        <source>Shows/hides volume rendering in the 3D scene.</source>
        <translation>Zobrazí/Skryje volume rendering v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1092"/>
        <source>Save DICOM Series...</source>
        <translation>Uložit DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1095"/>
        <location filename="../ui/mainwindow.ui" line="1098"/>
        <source>Saves DICOM Series to a given directory.</source>
        <translation>Uloží DICOM sérii do zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1107"/>
        <source>Import DICOM Data from ZIP...</source>
        <translation>Importovat DICOM data ze ZIP archivu...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1119"/>
        <source>Models List</source>
        <translation>Seznam modelů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1122"/>
        <location filename="../ui/mainwindow.ui" line="1125"/>
        <source>Shows/Hides the models list.</source>
        <translation>Zobrazí/Skryje seznam modelů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1133"/>
        <source>Equalize</source>
        <translation>Ekvalizace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1141"/>
        <source>Sharpen</source>
        <translation>Doostření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1146"/>
        <source>Save Original DICOM Series...</source>
        <translation>Uložit originální DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1149"/>
        <location filename="../ui/mainwindow.ui" line="1152"/>
        <source>Saves original DICOM Series to a given directory.</source>
        <translation>Uloží původní data série DICOM do zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="200"/>
        <location filename="../ui/mainwindow.ui" line="931"/>
        <source>Panels Toolbar</source>
        <translation>Lišta panelů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="31"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="35"/>
        <source>Recent</source>
        <translation>Naposledy otevřené</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="57"/>
        <source>&amp;Edit</source>
        <translation>Úp&amp;ravy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="75"/>
        <source>&amp;View</source>
        <translation>&amp;Zobrazit</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="139"/>
        <source>&amp;Model</source>
        <translation>M&amp;odel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="146"/>
        <source>&amp;Help</source>
        <translation>&amp;Nápověda</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="155"/>
        <source>Fil&amp;tering</source>
        <translation>&amp;Filtrování</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="171"/>
        <source>Me&amp;asurements</source>
        <translation>&amp;Měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="298"/>
        <location filename="../ui/mainwindow.ui" line="301"/>
        <source>Saves density data to a specified VLM file.</source>
        <translation>Uloží densitní data do zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="936"/>
        <source>Show Help</source>
        <translation>Zobrazit nápovědu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="945"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="957"/>
        <source>Scale Scene</source>
        <translation>Měřítko scény</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="968"/>
        <source>Load User Perspective</source>
        <translation>Načíst rozložení oken</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="971"/>
        <source>Loads the user defined windows layout.</source>
        <translation>Načte uživatelské rozložení oken (panelů, pohledů, lišt, apod.).</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="979"/>
        <source>Save User Perspective</source>
        <translation>Uložit rozložení oken</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="982"/>
        <source>Saves the current windows layout.</source>
        <translation>Uloží aktuální rozložení oken (panelů, pohledů, lišt, apod.).</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="990"/>
        <source>Load Default Perspective</source>
        <translation>Načíst standardní rozložení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="993"/>
        <location filename="../ui/mainwindow.ui" line="996"/>
        <source>Loads the default windows layout.</source>
        <translation>Načte výchozí rozložení oken.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1001"/>
        <source>Save STL Model...</source>
        <translation>Uložit STL model...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1012"/>
        <source>Send Data...  (DataExpress Service)</source>
        <translation>Odeslat data... (služba DataExpress)</translation>
    </message>
    <message>
        <source>Axial / XY Slice</source>
        <translation type="obsolete">Axiální / XY řez</translation>
    </message>
    <message>
        <source>Coronal / XZ Slice</source>
        <translation type="obsolete">Koronární / XZ řez</translation>
    </message>
    <message>
        <source>Sagittal / YZ Slice</source>
        <translation type="obsolete">Sagitální / YZ řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1081"/>
        <location filename="../src/mainwindow.cpp" line="917"/>
        <source>Volume Rendering</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="426"/>
        <source>Process model</source>
        <translation>Zpracovat model</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="429"/>
        <source>Visualization</source>
        <translation>Vizualizace</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="430"/>
        <source>Smooth</source>
        <translation>Hladké stínování</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="432"/>
        <source>Flat</source>
        <translation>Ploché stínování</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="434"/>
        <source>Wire</source>
        <translation>Drátový model</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="643"/>
        <source>Change panel visibility</source>
        <translation>Viditelnost panelů</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="650"/>
        <source>Fullscreen</source>
        <translation>Celá obrazovka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="889"/>
        <source>Density Window</source>
        <translation>Densitní okénko</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="898"/>
        <source>Ortho Slices</source>
        <translation>Kolmé řezy</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="907"/>
        <source>Tissue Segmentation</source>
        <translation>Segmentace tkání</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="928"/>
        <source>Models list</source>
        <translation>Seznam modelů</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <source>You have an unsaved segmentation data! All unsaved changes will be lost. Are your sure?</source>
        <translation>Segmentační data byla změněna, ale ne uložena. Při ukončení programu budou neuložené změny ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1188"/>
        <source>All unsaved changes will be lost. Are your sure?</source>
        <translation>Všechny neuložené změny budou při ukončení programu ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1285"/>
        <location filename="../src/mainwindow.cpp" line="1306"/>
        <source>Load DICOM Data</source>
        <translation>Prosím, zadejte adresář obsahující vstupní DICOM data...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1306"/>
        <source>ZIP archive (*.zip)</source>
        <translation>ZIP archiv (*.zip)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1363"/>
        <source>Scanning the directory for DICOM datasets, please wait...</source>
        <translation>Vyhledávání DICOM datových sad, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1380"/>
        <source>No valid DICOM datasets have been found in a given directory!</source>
        <translation>V zadaném adresáři nebyla nalaezena žádná DICOM data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1403"/>
        <source>Loading input DICOM dataset, please wait...</source>
        <translation>Načítání DICOM datové sady, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1412"/>
        <source>Failed to load the DICOM dataset!</source>
        <translation>Nepodařilo se načíst model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1468"/>
        <source>You have an unsaved data! All unsaved changes will be lost. Do you want to continue?</source>
        <translation>Všechny neuložené změny budou při ukončení programu ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1547"/>
        <source>Please, choose an input volume data to open...</source>
        <translation>Prosím, vyberte vstupní data...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1547"/>
        <location filename="../src/mainwindow.cpp" line="1690"/>
        <source>Volume Data (*.vlm)</source>
        <translation>Objemová data (*.vlm)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1587"/>
        <source>Loading input volumetric data, please wait...</source>
        <translation>Načítám objemová data, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1675"/>
        <source>Failed to read input volumetric data!</source>
        <translation>Nepodařilo se načíst vstupní objemová data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1690"/>
        <source>Choose an output file...</source>
        <translation>Zadejte výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1699"/>
        <source>Saving volumetric data, please wait...</source>
        <translation>Ukládám objemová data, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1744"/>
        <source>Failed to save volumetric data!</source>
        <translation>Nepodařilo se uložit objemová data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1764"/>
        <location filename="../src/mainwindow.cpp" line="1906"/>
        <source>Please, choose a directory where to save current DICOM dataset...</source>
        <translation>Zvolte cílový adresář pro uložení DICOM dat....</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1806"/>
        <source>Failed to copy file %1. Retry?</source>
        <translation>Při kopírování souboru %1 došlo k chybě. Opakovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1851"/>
        <source>Save DICOM</source>
        <translation>Uložení DICOM</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1856"/>
        <source>Save all data</source>
        <translation>Uložit aktuální data</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1859"/>
        <source>Save all segmented areas</source>
        <translation>Uložit všechny osegmentované oblasti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1861"/>
        <source>Save active region only</source>
        <translation>Uložit pouze aktivní region</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1868"/>
        <source>Compress</source>
        <translation>Komprimovat</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1893"/>
        <source>Segmented areas can&apos;t be exported with the current product license.</source>
        <translation>Aktuální licence neumožňuje export segmentovaných oblastí. </translation>
    </message>
    <message>
        <source>Save segmented areas only</source>
        <translation type="obsolete">Uložit pouze osegmentované oblasti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1919"/>
        <source>Specified directory already contains DICOM files! Do you really want to continue?</source>
        <translation>Zvolený adresář obsahuje DICOM soubory! Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1928"/>
        <source>Saving DICOM data, please wait...</source>
        <translation>Ukládájí se DICOM data, čekejte prosím...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2170"/>
        <source>Choose an input binary STL model to load...</source>
        <translation>Vyberte vstupní STL model pro načtení...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2170"/>
        <source>Stereo litography model (*.stl);;Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4415"/>
        <location filename="../src/mainwindow.cpp" line="4416"/>
        <location filename="../src/mainwindow.cpp" line="4417"/>
        <source>Process using %1</source>
        <translation>Zpracovat pomocí %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4421"/>
        <source>There are none known tools available!</source>
        <translation>Nejsou k dispozici žádné nástroje pro zpracování.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4600"/>
        <source>The maximum number of loadable models reached!</source>
        <translation>Byl dosažen maximální počet podporovaných modelů!</translation>
    </message>
    <message>
        <source>STL Data (*.stl)</source>
        <translation type="obsolete">STL data (*.stl)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2457"/>
        <source>Failed to load binary STL model!</source>
        <translation>Nepodařilo se načíst vstupní STL model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2520"/>
        <location filename="../src/mainwindow.cpp" line="4373"/>
        <source>No model selected!</source>
        <translation>Není zvolen žádný model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2532"/>
        <location filename="../src/mainwindow.cpp" line="4382"/>
        <source>No STL data!</source>
        <translation>Nejsou k dispozici žádná STL data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2552"/>
        <source>Data can&apos;t be exported with the current product license.</source>
        <translation>S aktuální licencí není možné exportovat zvolená data.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2564"/>
        <source>Please, specify an output file...</source>
        <translation>Zadejte výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2564"/>
        <source>Stereo litography model (*.stl);;Binary Polygon file format (*.ply);;ASCII Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2583"/>
        <source>ASCII Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <source>Loaded volume is cropped. It is recommended to include crop information in the exported STL unless you plan to load the model in VLM data.</source>
        <translation type="obsolete">Načtená data jsou oříznutá. Pokud neexportujete STL model pro využití s VLM daty, je vhodné exportovat model s informacemi o ořezu dat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2631"/>
        <source>Failed to save binary STL model!</source>
        <translation>Nepodařilo se uložit binární STL model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2905"/>
        <source>VR Error!</source>
        <translation>Chyba inicializace Volume Renderingu!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3004"/>
        <source>3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3012"/>
        <source>Axial / XY</source>
        <translation>Axiální / XY</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3020"/>
        <source>Coronal / XZ</source>
        <translation>Koronární / XZ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3028"/>
        <source>Sagittal / YZ</source>
        <translation>Sagitální / YZ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3181"/>
        <source>Creating surface model, please wait...</source>
        <translation>Vytvářím povrchový model, prosím čekejte...</translation>
    </message>
    <message>
        <source>Reducing flat areas, please wait...</source>
        <translation type="obsolete">Redukuji rovné plochy, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3220"/>
        <source>Reducing small areas, please wait...</source>
        <translation>Redukuji příliš malé oblaste, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3225"/>
        <source>Smoothing model, please wait...</source>
        <translation>Vyhlazuji model, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3238"/>
        <source>Decimating model, please wait...</source>
        <translation>Probíhá decimace modelu, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3318"/>
        <source>You must restart the application to apply the changes.</source>
        <translation>Nyní je nutné restartovat aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3826"/>
        <source>ONLY for internal testing and not for distribution!</source>
        <translation>Pouze pro interní testování, nešířit!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3828"/>
        <source>ONLY for testing!</source>
        <translation>Pouze pro testování!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3964"/>
        <location filename="../src/mainwindow.cpp" line="4035"/>
        <source>Please specify an output file...</source>
        <translation>Zadejte prosím výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3964"/>
        <location filename="../src/mainwindow.cpp" line="4035"/>
        <source>JPEG Image (*.jpg);;PNG Image (*.png);;BMP Image (*.bmp)</source>
        <translation>Obrázek JPEG (*.jpg);;Obrázek PNG (*.png);;Obrázek BMP (*.bmp)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4049"/>
        <source>Density Level: %1</source>
        <translation>Hustota: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="642"/>
        <location filename="../src/mainwindow.cpp" line="4195"/>
        <source>Panels</source>
        <translation>Panely</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4213"/>
        <source>Mouse Mode</source>
        <translation>Mód myši</translation>
    </message>
    <message>
        <source>Dimensions: 	%1 x %2 x %3 voxels
Resolution: 	%4 x %5 x %6 mm
Patient Name: 	%7
Patient ID: 	%8
Series Date: 	%9</source>
        <translation type="obsolete">Rozměry: 	%1 x %2 x %3 voxels
Rozlišení: 	%4 x %5 x %6 mm
Jméno pacienta:	%7
ID pacienta: 	%8
Datum série: 	%9</translation>
    </message>
    <message>
        <source>Information about the dataset...</source>
        <translation type="obsolete">Informace o aktuální datové sadě...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3342"/>
        <source>About </source>
        <translation>O aplikaci </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3344"/>
        <source>Lightweight 3D DICOM viewer.

</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3346"/>
        <source>http://www.3dim-laboratory.cz/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3348"/>
        <source>Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3413"/>
        <location filename="../src/mainwindow.cpp" line="3455"/>
        <location filename="../src/mainwindow.cpp" line="3496"/>
        <source>Filtering volumetric data, please wait...</source>
        <translation>Probíhá filtrování objemových dat, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3441"/>
        <location filename="../src/mainwindow.cpp" line="3479"/>
        <location filename="../src/mainwindow.cpp" line="3520"/>
        <source>Filtering aborted!</source>
        <translation>Filtrování selhalo!</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Pluginy</translation>
    </message>
    <message>
        <source>Show/Hide Plugin Toolbar</source>
        <translation type="obsolete">Zobrazit/Skrýt lištu pluginu</translation>
    </message>
    <message>
        <source>Show/Hide Plugin Panel</source>
        <translation type="obsolete">Zobrazit/Skrýt panel pluginu</translation>
    </message>
</context>
<context>
    <name>ModelsWidget</name>
    <message>
        <location filename="../ui/modelswidget.ui" line="14"/>
        <source>Models</source>
        <translation>Modely</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="21"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="26"/>
        <source>Visible</source>
        <translation>Viditelnost</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="31"/>
        <source>Cut</source>
        <translation>2D Řez</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="36"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="41"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="49"/>
        <source>Load/Save</source>
        <translation>Načtení/Uložení</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="55"/>
        <location filename="../ui/modelswidget.ui" line="58"/>
        <source>Transform according to original DICOM coordinates for later use in other software.</source>
        <translation>Transformovat na základě DICOM souřadnic pro pozdější načtení v jiném software.</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="61"/>
        <source>Use DICOM coordinates</source>
        <translation>Zohlednit DICOM souřadnice</translation>
    </message>
    <message>
        <source>Include information on volume transformation which was applied on data load.</source>
        <translation type="obsolete">Zohlednit transformaci objemových dat aplikovanou při jejich načtení.</translation>
    </message>
    <message>
        <source>Use Viewer transformation (VLM incompatible)</source>
        <translation type="obsolete">Zohlednit transformaci při načtení (nekompatibilní s VLM)</translation>
    </message>
</context>
<context>
    <name>OrthoSlicesWidget</name>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="26"/>
        <source>Ortho Slices</source>
        <translation>Kolmé řezy</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="53"/>
        <source>Axial / XY Slice</source>
        <translation>Axiální / XY řez</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="59"/>
        <location filename="../ui/orthosliceswidget.ui" line="62"/>
        <location filename="../ui/orthosliceswidget.ui" line="84"/>
        <location filename="../ui/orthosliceswidget.ui" line="87"/>
        <location filename="../ui/orthosliceswidget.ui" line="131"/>
        <location filename="../ui/orthosliceswidget.ui" line="134"/>
        <location filename="../ui/orthosliceswidget.ui" line="153"/>
        <location filename="../ui/orthosliceswidget.ui" line="156"/>
        <location filename="../ui/orthosliceswidget.ui" line="197"/>
        <location filename="../ui/orthosliceswidget.ui" line="200"/>
        <location filename="../ui/orthosliceswidget.ui" line="219"/>
        <location filename="../ui/orthosliceswidget.ui" line="222"/>
        <source>Adjusts slice position.</source>
        <translation>Upraví pozici řezu.</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="97"/>
        <location filename="../ui/orthosliceswidget.ui" line="100"/>
        <location filename="../ui/orthosliceswidget.ui" line="163"/>
        <location filename="../ui/orthosliceswidget.ui" line="166"/>
        <location filename="../ui/orthosliceswidget.ui" line="232"/>
        <location filename="../ui/orthosliceswidget.ui" line="235"/>
        <source>Selects displaying mode of slice.</source>
        <translation>Zvolí režim zobrazení řezu.</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="104"/>
        <location filename="../ui/orthosliceswidget.ui" line="170"/>
        <location filename="../ui/orthosliceswidget.ui" line="239"/>
        <source>Default</source>
        <translation>Standardní</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="109"/>
        <location filename="../ui/orthosliceswidget.ui" line="175"/>
        <location filename="../ui/orthosliceswidget.ui" line="244"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="114"/>
        <location filename="../ui/orthosliceswidget.ui" line="180"/>
        <location filename="../ui/orthosliceswidget.ui" line="249"/>
        <source>RTG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="125"/>
        <source>Coronal / XZ Slice</source>
        <translation>Koronární / XZ řez</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="191"/>
        <source>Sagittal / YZ Slice</source>
        <translation>Sagitální / YZ řez</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="96"/>
        <source>Invalid parameter detected in C runtime function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="418"/>
        <source>Your hardware doesn&apos;t meet the recommended configuration. You may experience reduced performance or the application won&apos;t run at all.</source>
        <translation>Váš hardware nesplňuje požadavky pro správný běh programu. Aplikace může běžet pomaleji nebo vůbec. </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="419"/>
        <source>Don&apos;t show again</source>
        <translation>Příště nezobrazovat</translation>
    </message>
    <message>
        <source>Copyright %1 2008-2014 by 3Dim Laboratory s.r.o.</source>
        <translation type="obsolete">Copyright %1 2008-2014 3Dim Laboratory s.r.o.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3345"/>
        <source>Copyright %1 2008-%2 by 3Dim Laboratory s.r.o.</source>
        <translation>Copyright %1 2008-%2 3Dim Laboratory s.r.o.</translation>
    </message>
</context>
<context>
    <name>SegmentationWidget</name>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="26"/>
        <source>Tissue Segmentation</source>
        <translation>Segmentace tkání</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="59"/>
        <source>Thresholding</source>
        <translation>Prahování</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="71"/>
        <location filename="../ui/segmentationwidget.ui" line="74"/>
        <location filename="../ui/segmentationwidget.ui" line="99"/>
        <location filename="../ui/segmentationwidget.ui" line="102"/>
        <source>Adjusts lower threshold.</source>
        <translation>Upraví dolní práh.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="115"/>
        <location filename="../ui/segmentationwidget.ui" line="118"/>
        <location filename="../ui/segmentationwidget.ui" line="143"/>
        <location filename="../ui/segmentationwidget.ui" line="146"/>
        <source>Adjusts higher threshold.</source>
        <translation>Upraví horní práh.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="170"/>
        <location filename="../ui/segmentationwidget.ui" line="173"/>
        <source>Applies coloring of data according to thresholds.</source>
        <translation>Podle prahů aplikuje barvení na data.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="176"/>
        <source>Apply Thresholds</source>
        <translation>Aplikovat prahování</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="183"/>
        <location filename="../ui/segmentationwidget.ui" line="186"/>
        <source>Sets color of thresholded data.</source>
        <translation>Nastaví barvu pro prahovaná data.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="207"/>
        <location filename="../ui/segmentationwidget.ui" line="210"/>
        <source>Picks lower threshold from data by clicking with left mose button.</source>
        <translation>Zvolte dolní práh z dat kliknutím levého tlačítka myši.</translation>
    </message>
    <message>
        <source>Pick T1</source>
        <translation type="obsolete">Získat T1</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="236"/>
        <location filename="../ui/segmentationwidget.ui" line="239"/>
        <source>Picks higher threshold from data by clicking with left mose button.</source>
        <translation>Zvolte horní práh z dat kliknutím levého tlačítka myši.</translation>
    </message>
    <message>
        <source>Pick T2</source>
        <translation type="obsolete">Získat T2</translation>
    </message>
</context>
<context>
    <name>VolumeRenderingWidget</name>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="20"/>
        <source>Volume Rendering</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="50"/>
        <source>Rendering Mode</source>
        <translation>Mód zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="58"/>
        <location filename="../ui/volumerenderingwidget.ui" line="61"/>
        <source>Selects MIP rendering mode.</source>
        <translation>Zvolí režim MIP.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="64"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="71"/>
        <location filename="../ui/volumerenderingwidget.ui" line="74"/>
        <source>Selects Shaded rendering mode.</source>
        <translation>Zvolí režim Stínovaný.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="77"/>
        <source>Shaded</source>
        <translation>Stínovaný</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="84"/>
        <location filename="../ui/volumerenderingwidget.ui" line="87"/>
        <source>Selects X-Ray rendering mode.</source>
        <translation>Zvolí režim X-Ray.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="90"/>
        <source>X-Ray</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="97"/>
        <location filename="../ui/volumerenderingwidget.ui" line="100"/>
        <source>Selects Surface rendering mode.</source>
        <translation>Zvolí režim Povrchy.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="103"/>
        <source>Surface</source>
        <translation>Povrchy</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="110"/>
        <location filename="../ui/volumerenderingwidget.ui" line="113"/>
        <source>Selects rendering mode controlled by plugins.</source>
        <translation>Zvolí režim Vlastní.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="116"/>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="128"/>
        <source>Coloring Lookup Tables</source>
        <translation>Mapování barev</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="134"/>
        <location filename="../ui/volumerenderingwidget.ui" line="137"/>
        <source>Selects look-up table.</source>
        <translation>Zvolí tabulku barev.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="141"/>
        <source>Bone surface</source>
        <translation>Povrch kosti</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="146"/>
        <source>Skin surface</source>
        <translation>Měkké tkáně</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="157"/>
        <source>Rendering Quality</source>
        <translation>Kvalita zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="163"/>
        <location filename="../ui/volumerenderingwidget.ui" line="166"/>
        <source>Adjusts quality of volume rendering.</source>
        <translation>Nastaví kvalitu zobrazení.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="191"/>
        <source>Output Adjustment</source>
        <translation>Nastavení výstupu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="199"/>
        <source>Wind. Shift</source>
        <translation>Posun okénka</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="206"/>
        <location filename="../ui/volumerenderingwidget.ui" line="209"/>
        <source>Adjusts shift of density window.</source>
        <translation>Upraví posun densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="225"/>
        <source>Wind. Width</source>
        <translation>Šířka okénka</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="232"/>
        <location filename="../ui/volumerenderingwidget.ui" line="235"/>
        <source>Adjusts width of density window.</source>
        <translation>Upraví šířku densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="251"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="258"/>
        <location filename="../ui/volumerenderingwidget.ui" line="261"/>
        <source>Adjusts brightness of volume rendering.</source>
        <translation>Upraví jas.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="277"/>
        <source>Contrast</source>
        <translation>Kontrast</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="284"/>
        <location filename="../ui/volumerenderingwidget.ui" line="287"/>
        <source>Adjusts contrast of volume rendering.</source>
        <translation>Upraví kontrast.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="303"/>
        <source>Surf. Tolerance</source>
        <translation>Tolerance povrchu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="310"/>
        <location filename="../ui/volumerenderingwidget.ui" line="313"/>
        <source>Adjusts surface tolerance.</source>
        <translation>Upraví toleranci povrchu.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="329"/>
        <source>Surf. Sharpness</source>
        <translation>Strmost povrchu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="336"/>
        <location filename="../ui/volumerenderingwidget.ui" line="339"/>
        <source>Adjusts surface sharpness.</source>
        <translation>Upraví strmost povrchu.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="360"/>
        <source>Cutting Plane</source>
        <translation>Rovina řezu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="366"/>
        <location filename="../ui/volumerenderingwidget.ui" line="369"/>
        <source>Adjust cutting plane position.</source>
        <translation>Nastaví rovinu řezu.</translation>
    </message>
    <message>
        <source>Soft tissues</source>
        <translation type="obsolete">Měkké tkáně</translation>
    </message>
    <message>
        <source>Bone tissues</source>
        <translation type="obsolete">Kostní tkáň</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="obsolete">Průhledný</translation>
    </message>
    <message>
        <source>Air pockets</source>
        <translation type="obsolete">Vzduchové kapsy</translation>
    </message>
    <message>
        <source>Enhance soft</source>
        <translation type="obsolete">Zvýraznit měkké tkáně</translation>
    </message>
    <message>
        <source>Enhance hard</source>
        <translation type="obsolete">Zvýraznit kosti</translation>
    </message>
</context>
</TS>
